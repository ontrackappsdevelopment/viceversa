//
//  ShareViewController.swift
//  SmartLockShare
//
//  Created by Bogachev on 6/11/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import Social
import MobileCoreServices

class ShareViewController: UIViewController {
	
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var statusLabel: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if let extensionContext = extensionContext {
			
			for inputItem in extensionContext.inputItems {
				
				let extensionItem = inputItem as! NSExtensionItem
				
				if let attachments = extensionItem.attachments as? [NSItemProvider] {
					
					let width: CGFloat = 200
					let height = width
					var x = containerView.bounds.width / 2 - width / 2
					var y = containerView.bounds.height / 2 - width / 2
					var previousImageView: UIImageView?
					var previewCounter = 0
					
					if attachments.count > 1 {
						statusLabel.text = "\(attachments.count) photos"
					} else {
						statusLabel.text = "\(attachments.count) photo"
					}
					
					for itemProvider in attachments {
						
						previewCounter += 1
						
						if previewCounter > 5 {
							break
						}

						itemProvider.loadPreviewImage(options: [NSItemProviderPreferredImageSizeKey:CGSize(width: 100, height: 100)], completionHandler: { (secureCoding, error) in
							
							if let image = secureCoding as? UIImage {
								
								let imageView = UIImageView(frame: CGRect(x: x, y: y, width: width, height: height))
								
								print("\(imageView.frame)")
								
								imageView.image = image
								imageView.contentMode = .scaleAspectFill
								imageView.clipsToBounds = true
						
								x += 5
								y -= 5
								
								if let previousImageView = previousImageView {
									
									DispatchQueue.main.async {
										self.containerView.insertSubview(imageView, belowSubview: previousImageView)
									}

								} else {
									
									DispatchQueue.main.async {
										self.containerView.addSubview(imageView)
									}

								}
								
								previousImageView = imageView
								
							}
							
						})
						
					}
					
				}
				
			}
			
		}
		
	}
		
	@IBAction func onCancel(_ sender: UIButton) {
		let cancelError = NSError(domain: NSCocoaErrorDomain, code: -1, userInfo: nil)
		self.extensionContext!.cancelRequest(withError: cancelError)
	}
	
	@IBAction func onAdd(_ sender: UIButton) {
		
		guard let folderURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.com.smlock.free")?.appendingPathComponent("Extension") else { return }
		
		if !FileManager.default.fileExists(atPath: folderURL.path) {
			try? FileManager.default.createDirectory(at: folderURL, withIntermediateDirectories: true, attributes: nil)
		}

		if let extensionContext = extensionContext {
			
			for inputItem in extensionContext.inputItems {
				
				let extensionItem = inputItem as! NSExtensionItem
				
				if let attachments = extensionItem.attachments as? [NSItemProvider] {
					
					if attachments.count > 1 {
						statusLabel.text = "\(attachments.count) photos"
					} else {
						statusLabel.text = "\(attachments.count) photo"
					}
					
					let index = 0
					var expected = attachments.count
					
					for itemProvider in attachments {
						
						var contentType = kUTTypeImage as String
						if itemProvider.hasItemConformingToTypeIdentifier(contentType) {
							itemProvider.loadItem(forTypeIdentifier: contentType, options: nil) { url, error in
								
								if let url = url as? URL {
									let fileName = url.lastPathComponent
									let newFileURL = folderURL.appendingPathComponent(fileName)
									try? FileManager.default.copyItem(at: url, to: newFileURL)
								}

								if let image = url as? UIImage {
									
									if let data = UIImageJPEGRepresentation(image, 0) {
										let filename = folderURL.appendingPathComponent("\(index).jpg")
										try? data.write(to: filename)
									}
									
								}
								
								expected -= 1;
								
								if expected <= 0 {
									
									self.extensionContext!.completeRequest(returningItems: nil, completionHandler: nil)
									
								}
							}
							
						}

						contentType = kUTTypeQuickTimeMovie as String
						if itemProvider.hasItemConformingToTypeIdentifier(contentType) {
							
							itemProvider.loadItem(forTypeIdentifier: contentType, options: nil) { url, error in
								
								print("\(error)")
								
								if let url = url as? URL {
									let fileName = url.lastPathComponent
									let newFileURL = folderURL.appendingPathComponent(fileName)
									try? FileManager.default.copyItem(at: url, to: newFileURL)
								}
								
								expected -= 1;
						
								if expected <= 0 {
									
									self.extensionContext!.completeRequest(returningItems: nil, completionHandler: nil)
									
								}
							}
							
						}
						
						contentType = kUTTypeURL as String
						if itemProvider.hasItemConformingToTypeIdentifier(contentType) {
							
							itemProvider.loadItem(forTypeIdentifier: contentType, options: nil) { url, error in
								
								print("\(error)")
								
								if let url = url as? URL {
									let fileName = url.lastPathComponent
									let newFileURL = folderURL.appendingPathComponent(fileName)
									try? FileManager.default.copyItem(at: url, to: newFileURL)
								}
								
								expected -= 1;
								
								if expected <= 0 {
									
									self.extensionContext!.completeRequest(returningItems: nil, completionHandler: nil)
									
								}
							}
							
						}
						
						
					}
					
				}
				
			}
			
		}
		
	}
	
}
