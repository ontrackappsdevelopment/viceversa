//
//  OneDriveService.swift
//  SmartLock
//
//  Created by Bogachev on 2/15/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import OneDriveSDK

class OneDriveServiceDownloadObserver: NSObject {
	
	var progressAction: ((_ progress: Float, _ writtenItems: Int, _ totalItems: Int) -> ())!
	var finishAction: (() -> ())!
	var cancelAction: (() -> ())!
	
	
	init(progress: @escaping (Float, Int, Int) -> (), finish: @escaping () -> (), cancel: @escaping () -> ()) {
		super.init()
		
		progressAction = progress
		finishAction = finish
		cancelAction = cancel
		
		OneDriveService.sharedInstance.addObserver(self, forKeyPath: "writtenItems", options: .new, context: nil)
		OneDriveService.sharedInstance.addObserver(self, forKeyPath: "progress", options: .new, context: nil)
		OneDriveService.sharedInstance.addObserver(self, forKeyPath: "downloadStatus", options: .new, context: nil)
	}
	
	deinit {
		OneDriveService.sharedInstance.removeObserver(self, forKeyPath: "writtenItems", context: nil)
		OneDriveService.sharedInstance.removeObserver(self, forKeyPath: "progress", context: nil)
		OneDriveService.sharedInstance.removeObserver(self, forKeyPath: "downloadStatus", context: nil)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		
		guard keyPath != nil else { return }
		
		switch keyPath! {
		case "writtenItems":
			
			if let action = progressAction {
				
				let total = OneDriveService.sharedInstance.totalItems
				let progress = OneDriveService.sharedInstance.progress.floatValue
				
				if let newValue = change?[.newKey] {
					action(progress, newValue as! Int, total)
				}
				
			}
			
		case "progress":
			
			if let action = progressAction {
				
				let total = OneDriveService.sharedInstance.totalItems
				let written = OneDriveService.sharedInstance.writtenItems.intValue
				
				if let newValue = change?[.newKey] {
					action((newValue as! NSNumber).floatValue, written, total)
				}
				
			}
			
		case "downloadStatus":
			
			if let newValue = change?[.newKey] {
				
				let status = newValue as! String
				
				switch status {
				case DownloadStatus.finished.rawValue:
					
					if let action = finishAction {
						
						action()
						
					}
					
				case DownloadStatus.canceled.rawValue:
					
					if let action = cancelAction {
						
						action()
						
					}
					
				default:
					break
				}
				
			}
			
		default: break
		}
		
	}
}

class OneDriveService: NSObject {
	
	static let sharedInstance = OneDriveService()
	
	public var client: ODClient?
	
	private let appID = "255f17d1-9c7d-4877-81a2-34ea4bb65165"
	private let scope = ["onedrive.readwrite"]
	private var task: ODURLSessionDownloadTask?
	private var taskProgress: Progress?
	
	dynamic var progress: NSNumber = 0
	
	dynamic var writtenItems: NSNumber = 0
	
	fileprivate var totalItems: Int = 0
	
	var savedFilesURL = [URL]()
	
	dynamic var downloadStatus: NSString = DownloadStatus.undefined.rawValue as NSString
	
	var itemsToDownload = [ODItem]()
	var itemsToDelete = [ODItem]()
	
	var oauthIsFailed: Bool = false
	
	var downloadCompleation: (() -> ())?
	
	override init() {
		super.init()
		ODClient.setMicrosoftAccountAppId(appID, scopes: scope)
	}
	
	func isAuthorized() -> Bool {
		
		if let _ = ODClient.loadCurrent() {
			return true
		}
		
		return false
		
	}
	
	func logout() {
		
		if let client = ODClient.loadCurrent() {
			
			client.signOut(completion: { (error) in
				print("\(error)")
			})
			
		}
		
	}
	
	func fetchAuthorization(compleation: @escaping () -> () ) {
		
		if oauthIsFailed == false {
			
			if let currentClient = ODClient.loadCurrent() {
				
				client = currentClient
				compleation()
				
				return
			}
			
		}
		
		ODClient.authenticatedClient {[weak self] (client, error) in
			guard let strongSelft = self else { return }
			guard error == nil else {
				//show error message
				return
			}
			
			strongSelft.oauthIsFailed = false
			
			ODClient.setCurrent(client)
			
			strongSelft.client = client
			compleation()
			
		}
		
	}
	
	func fetchOneDriveContent(itemId: String, compleation: @escaping ([ODItem]) -> () ) {
		
		let childrenRequest = client?.drive().items().item(itemId).children().request()
		
		if let flags = client?.serviceFlags(), flags["NoThumbnails"] == nil {
			
			let _ = childrenRequest?.expand("thumbnails")
			
		}
		
		let _ = childrenRequest?.getWithCompletion({ (response, nextRequest, error) in
			
			if let error = error, (error as NSError).code == 401 {
				
				self.oauthIsFailed = true
				
				self.fetchAuthorization(compleation: {
					
					self.fetchOneDriveContent(itemId: itemId, compleation: compleation)
					
				})
				
			}
			
			if let object = response?.value {
				
				let items = object as! [ODItem]
				
				compleation( items.filter({ (item) -> Bool in
					
					if item.file != nil {
						
						switch item.file.mimeType {
						case "video/mp4", "video/mpeg", "image/png", "image/jpeg", "video/quicktime": return true
						default: return false
						}
						
						
					} else if item.folder != nil {
						return true
					}
					
					return false
					
				}))
				
			}
			
			if nextRequest != nil {
				
				
				
			}
			
			if error != nil {
				
				compleation([ODItem]())
				
			}
			
		})
		
	}
	
	//MARK: - Download
	
	func download(items: [ODItem], compleation: @escaping () -> ()) {
		
		itemsToDownload = items
		
		itemsToDelete = itemsToDownload
		
		downloadCompleation = compleation
		
		writtenItems = 0
		
		progress = 0
		
		totalItems = itemsToDownload.count
		
		savedFilesURL.removeAll()
		
		downloadStatus = DownloadStatus.started.rawValue as NSString
		
		startDownloadNextFile()
	}
	
	func startDownloadNextFile() {
		
		guard itemsToDownload.count > 0 else { return }
		
		let item = itemsToDownload.removeFirst()
		
		task = client?.drive().items(item.id).contentRequest().download(completion: { [weak self] (url, response, error) in
			
			guard let strongSelf = self else { return }
			
			strongSelf.taskProgress?.removeObserver(strongSelf, forKeyPath: "fractionCompleted", context: nil)
			
			if error == nil {
				
				let uniqueName = FileService.sharedInstance.verifyFileName(name: item.name)
				let newFileURL = FileService.sharedInstance.currentDirectory.url.appendingPathComponent(uniqueName)
				
				if newFileURL.isImage || newFileURL.isVideo {
					
					do {
						
						try FileManager.default.moveItem(at: url!, to: newFileURL)
						
					} catch {
						
					}
					
					strongSelf.savedFilesURL.append(newFileURL)
					
				}
				
			}
			
			strongSelf.writtenItems = (strongSelf.writtenItems.intValue + 1) as NSNumber
			
			strongSelf.progress = 0
			
			if strongSelf.itemsToDownload.count == 0 {
				
				self?.savedFilesURL.forEach({ (url) in
					let file = File(url: url)
					
					let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						_ = MFile.create(with: file.relativePath, context: localContext)
					})
					
					file.saveFileThumbnail()

				})
				
				strongSelf.downloadStatus = DownloadStatus.finished.rawValue as NSString
			} else {
				strongSelf.startDownloadNextFile()
			}
			
		})
		
		taskProgress = task!.progress
		
		taskProgress!.totalUnitCount = item.size
		
		taskProgress!.addObserver(self, forKeyPath: "fractionCompleted", options: .new, context: nil)
		
	}
	
	func cancelDownload() {
		
		itemsToDownload.removeAll()
		
		if task != nil {
			task!.cancel()
		}
		
		FileManager.default.delete(urls: savedFilesURL)
		
		downloadStatus = DownloadStatus.canceled.rawValue as NSString
		
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		
		guard keyPath != nil else { return }
		
		switch keyPath! {
		case "fractionCompleted":
			
			if let newValue = change?[.newKey] {
				progress = newValue as! NSNumber
				getUploadProgressData(progress: progress.floatValue)
			}
			
		default: break
			
		}
		
	}
	
	func deleteFileWithService() {
		
		for item in itemsToDelete {
			
			let _ = client?.drive().items(item.id).request().delete(completion: { (error) in
				
				print("\(error)")
				
			})
			
		}
		
	}
	
	//MARK: - Upload
	
	var uploadArray = [(File, String)]()
	
	let uploadGroup = DispatchGroup()
	
	var uploadCompleate: (() -> ())?
	var uploadProgress: ((Float, Int, Int) -> ())?
	
	var uploadTotal: Int?
	var uploadWritten: Int?
	var uploadCurrentProgress: Float?
	
	var uploadTask: ODURLSessionUploadTask?
	
	
	func upload(files: [File], itemId: String, progress: @escaping (Float, Int, Int) -> (), compleate: @escaping () -> ()) {
		
		uploadProgress = progress
		uploadCompleate = compleate
		
		DispatchQueue.global().async {
			
			for file in files {
				
				switch file.fileType {
				case .image, .video:
					self.uploadArray.append((file, itemId))
					
				case .directory:
					self.prepareFolderToUpload(folder: file, itemId: itemId)
					
				default:
					break
				}
				
			}
			
			self.uploadGroup.wait()
			
			DispatchQueue.main.async {
				
				self.uploadTotal = self.uploadArray.count
				self.uploadWritten = 0
				self.uploadCurrentProgress = 0
				
				self.uploadNext()
				
			}
			
		}
		
	}
	
	func uploadNext() {
		
		let fileInfo = uploadArray.removeFirst()
		
		inserFile(file: fileInfo.0, itemID: fileInfo.1)
		
	}
	
	func prepareFolderToUpload(folder: File, itemId: String) {
		
		uploadGroup.enter()
		
		createFolder(itemId: itemId, name: folder.name) { (item, error) in
			
			if error == nil {
				
				folder.fetchContent()
				
				for file in folder.contentOfDirectory {
					
					switch file.fileType {
					case .image, .video:
						self.uploadArray.append((file, item!.id))
						
					case .directory:
						self.prepareFolderToUpload(folder: file, itemId: item!.id)
						
					default:
						break
						
					}
					
				}
				
			}
			
			self.uploadGroup.leave()
			
		}
		
	}
	
	func inserFile(file: File, itemID: String) {
		
		uploadTask = client?.drive().items(itemID).item(byPath: file.name).contentRequest().upload(fromFile: file.url, completion: { [weak self] (item, error) in
			
			guard let strongSelf = self else { return }
			
			strongSelf.taskProgress?.removeObserver(strongSelf, forKeyPath: "fractionCompleted", context: nil)
			
			strongSelf.uploadWritten! += 1
			strongSelf.uploadCurrentProgress = 0
			
			if strongSelf.uploadArray.count == 0 {
				strongSelf.uploadCompleate!()
			} else {
				strongSelf.uploadNext()
			}
			
		})
		
		taskProgress = uploadTask!.progress
		
		taskProgress!.totalUnitCount = file.size!
		
		taskProgress!.addObserver(self, forKeyPath: "fractionCompleted", options: .new, context: nil)
		
	}
	
	func getUploadProgressData(progress: Float) {
		
		if let progressAction = uploadProgress {
			
			self.uploadCurrentProgress = progress
			
			progressAction(progress, uploadWritten!, uploadTotal!)
			
		}
	}
	
	func cancelUpload() {
		
		if uploadTask != nil {
			uploadTask!.cancel()
		}
		
		uploadArray.removeAll()
		
	}
	
	//MARK: - Create Folder
	
	func createFolder(itemId: String, name: String, compleation: @escaping (ODItem?, Error?) -> ()) {
		
		let key = ODNameConflict.rename().key!
		let value = ODNameConflict.rename().value!
		let newFolder = ODItem(dictionary: [key:value])
		newFolder!.name = name
		newFolder!.folder = ODFolder()
		
		let _ = client?.drive().items().item(itemId).children().request().add(newFolder, withCompletion: { (item, error) in
			
			if error == nil {
				compleation(item, nil)
			} else {
				compleation(nil, error)
			}
			
		})
		
	}
	
}
