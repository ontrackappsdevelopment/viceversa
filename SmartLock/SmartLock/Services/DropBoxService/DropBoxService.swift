//
//  DropBoxService.swift
//  SmartLock
//
//  Created by Bogachev on 2/10/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import SwiftyDropbox

class DropBoxServiceDownloadObserver: NSObject {
  
  var progressAction: ((_ progress: Float, _ writtenItems: Int, _ totalItems: Int) -> ())!
  var finishAction: (() -> ())!
  var cancelAction: (() -> ())!
  
  
  init(progress: @escaping (Float, Int, Int) -> (), finish: @escaping () -> (), cancel: @escaping () -> ()) {
    super.init()
    
    progressAction = progress
    finishAction = finish
    cancelAction = cancel
    
    DropBoxService.sharedInstance.addObserver(self, forKeyPath: "writtenItems", options: .new, context: nil)
    DropBoxService.sharedInstance.addObserver(self, forKeyPath: "progress", options: .new, context: nil)
    DropBoxService.sharedInstance.addObserver(self, forKeyPath: "downloadStatus", options: .new, context: nil)
  }
  
  deinit {
    DropBoxService.sharedInstance.removeObserver(self, forKeyPath: "writtenItems", context: nil)
    DropBoxService.sharedInstance.removeObserver(self, forKeyPath: "progress", context: nil)
    DropBoxService.sharedInstance.removeObserver(self, forKeyPath: "downloadStatus", context: nil)
  }
  
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    
    guard keyPath != nil else { return }
    
    switch keyPath! {
    case "writtenItems":
      
      if let action = progressAction {
        
        let total = DropBoxService.sharedInstance.totalItems
        let progress = DropBoxService.sharedInstance.progress.floatValue
        
        if let newValue = change?[.newKey] {
          action(progress, newValue as! Int, total)
        }
        
      }
      
    case "progress":
      
      if let action = progressAction {
        
        let total = DropBoxService.sharedInstance.totalItems
        let written = DropBoxService.sharedInstance.writtenItems.intValue
        
        if let newValue = change?[.newKey] {
          action((newValue as! NSNumber).floatValue, written, total)
        }
        
      }
      
    case "downloadStatus":
      
      if let newValue = change?[.newKey] {
        
        let status = newValue as! String
        
        switch status {
        case DownloadStatus.finished.rawValue:
          
          if let action = finishAction {
            
            action()
            
          }
          
        case DownloadStatus.canceled.rawValue:
          
          if let action = cancelAction {
            
            action()
            
          }
          
        default:
          break
        }
        
      }
      
    default: break
    }
    
  }
}

class DropBoxService: NSObject {
  
  static let sharedInstance = DropBoxService()
  
  var filesToDownload = Array<Files.Metadata>()
  
  fileprivate var filesToDelete = Array<Files.Metadata>()

  var uploadCompleation: (() -> ())?
  
  fileprivate var autorizationCompleation: (() -> ())?
  
  dynamic var progress: NSNumber = 0
  
  dynamic var writtenItems: NSNumber = 0
  
  fileprivate var totalItems: Int = 0
  
  var savedFilesURL = [URL]()
  
  var request: DownloadRequestFile<Files.FileMetadataSerializer, Files.DownloadErrorSerializer>?
  
  dynamic var downloadStatus: NSString = DownloadStatus.undefined.rawValue as NSString
  
  override init() {
    super.init()
    
    DropboxClientsManager.setupWithAppKey("l9ao07i0t8k3x50")
    
  }
  
  func handleURL(url: URL) -> Bool {
    
    if let authResult = DropboxClientsManager.handleRedirectURL(url) {
      switch authResult {
      case .success:
        
        if let action = autorizationCompleation {
          action()
        }
        
        print("Success! User is logged into Dropbox.")
      case .cancel:
        print("Authorization flow was manually canceled by user!")
      case .error(_, let description):
        print("Error: \(description)")
      }
    }
    
    return true
    
  }
  
  func fetchAuthorization(presenter: UIViewController, compleation: @escaping () -> () ) {
    
    autorizationCompleation = compleation
    
    if DropboxClientsManager.authorizedClient == nil {
      
      DropboxClientsManager.authorizeFromController(UIApplication.shared, controller: presenter, openURL: { (url: URL) -> Void in
        
        UIApplication.shared.openURL(url)})
      
    } else {
      
      compleation()
      
    }
    
  }
  
  func fetchThambnail(path: String, compleation: @escaping (UIImage) -> ()) -> DownloadRequestMemory<Files.FileMetadataSerializer, Files.ThumbnailErrorSerializer>? {
    
    guard let client = DropboxClientsManager.authorizedClient else { return nil }
    
    let request = client.files.getThumbnail(path: path, format: .png, size: .w128h128)
    
    request.response { (fileMetaData, error) in
      
      if let fileMetaData = fileMetaData {
        
        compleation(UIImage(data: fileMetaData.1)!)
        
      }
      
    }
    
    return request
    
  }
  
  func fetchFolderContent(pathFolder: String, compleation: @escaping ([Files.Metadata]) -> () ) {
    
    guard let client = DropboxClientsManager.authorizedClient else { return }
    
    client.files.listFolder(path: pathFolder).response { (list, error) in
      
      var entries = Array<Files.Metadata>()
      
      if let list = list {
        
        entries = list.entries
        
      }
      
      let results = entries.filter({ (file) -> Bool in
        
        if let pathLower = file.pathLower {
          
          if pathLower.isImage || pathLower.isVideo {
            
            return true
            
          }
          
          if pathLower.contains(".") == false {
            return true
          }
          
        }
        
        return false
        
      })
      
      compleation(results)
      
      
    }
    
  }
  
  //MARK: - Download
  
  func download(files: [Files.Metadata], compleation: @escaping () -> ()) {
    
    filesToDownload = files
    
    filesToDelete = filesToDownload
    
    uploadCompleation = compleation
    
    writtenItems = 0
    
    progress = 0
    
    totalItems = filesToDownload.count
    
    downloadStatus = DownloadStatus.started.rawValue as NSString
    
    savedFilesURL.removeAll()
    
    startDownloadNextFile()
    
  }
  
  func cancelDownload() {
    
    filesToDownload.removeAll()
    
    if request != nil {
      request?.cancel()
    }
    
    FileManager.default.delete(urls: savedFilesURL)
    
    downloadStatus = DownloadStatus.canceled.rawValue as NSString
    
  }
  
  func startDownloadNextFile() {
    
    guard filesToDownload.count > 0 else { return }
    
    let file = filesToDownload.removeLast()
    
    guard let client = DropboxClientsManager.authorizedClient else { return }
	
	let uniqueName = FileService.sharedInstance.verifyFileName(name: file.name)
	let newFileURL = FileService.sharedInstance.currentDirectory.url.appendingPathComponent(uniqueName)
	if !newFileURL.isImage && !newFileURL.isVideo {
		startDownloadNextFile()
		return
	}
		
    request = client.files.download(path: file.pathLower ?? "", rev: nil, overwrite: false, destination: {[weak self] (url, response) -> URL in
		
      self?.savedFilesURL.append(newFileURL)
      
      self!.writtenItems = (self!.writtenItems.intValue + 1) as NSNumber
      
      return newFileURL
      
    })
    
    request?.response(completionHandler: {[weak self] (metadata, error) in
      
      if let strongSelf = self {
        
        if strongSelf.filesToDownload.count == 0 {
          
          strongSelf.downloadStatus = DownloadStatus.finished.rawValue as NSString
          
          self?.savedFilesURL.forEach({ (url) in
            let file = File(url: url)
			
			let context = NSManagedObjectContext.mr_default()
			context.mr_save(blockAndWait: { (localContext) in
				_ = MFile.create(with: file.relativePath, context: localContext)
			})
			
			file.saveFileThumbnail()
			
          })
          
          strongSelf.uploadCompleation!()
          
        } else {
          
          strongSelf.startDownloadNextFile()
          
        }
		
      }
      
    })
    
    request!.progress { [weak self] (progress) in
      
      guard let strongSelf = self else { return }

      strongSelf.progress = (Float(progress.completedUnitCount) / Float(progress.totalUnitCount)) as NSNumber
      
    }
    
  }
  
  func deleteFileWithService() {
    
    guard let client = DropboxClientsManager.authorizedClient else { return }
    
    for file in filesToDelete {
		
      client.files.deleteV2(path: file.pathLower!).response(completionHandler: { (metadata, error) in
        
        print("\(String(describing: error))")
        
      })
      
    }
    
  }
  
  //MARK: - Create
  
  func createFolder(pathFolder: String, name: String, compleation: @escaping (_ file: Files.CreateFolderResult?, _ error: Error?) -> ()) {
    
    guard let client = DropboxClientsManager.authorizedClient else { return }
    
    let newFolderPath = "\(pathFolder)/\(name)"
    
    client.files.createFolderV2(path: newFolderPath).response { (folderMetaDate, error) in
      
      compleation(folderMetaDate, error as! Error?)
      
    }

  }
  
  //MARK: - Upload
  
  var uploadArray = [(File, String)]()
  
  let uploadGroup = DispatchGroup()
  
  var uploadCompleate: (() -> ())?
  var uploadProgress: ((Float, Float) -> ())?
  
  var uploadTotal: Int64?
  var uploadWritten: Int64?
  var uploadCurrentWritten: Int64?
  
  var uloadRequest: UploadRequest<Files.FileMetadataSerializer, Files.UploadErrorSerializer>?
  
  func upload(files: [File], folderIdentifier: String, progress: @escaping (Float, Float) -> (), compleate: @escaping () -> ()) {
    
    uploadProgress = progress
    uploadCompleate = compleate
    
    DispatchQueue.global().async {
      
      for file in files {
        
        switch file.fileType {
        case .image, .video:
          self.uploadArray.append((file, folderIdentifier))
          
        case .directory:
          self.prepareFolderToUpload(folder: file, folderIdentifier: folderIdentifier)
          
        default:
          break
        }
        
      }
      
      self.uploadGroup.wait()
      
      DispatchQueue.main.async {
        
        self.uploadTotal = self.uploadArray.flatMap({ $0.0.size }).reduce(0, +)
        self.uploadWritten = 0
        self.uploadCurrentWritten = 0
        
        self.uploadNext()
        
      }
      
    }
    
  }
  
  func uploadNext() {
    
    let fileInfo = uploadArray.removeFirst()
    
    inserFile(url: fileInfo.0.url, folderIdentifier: fileInfo.1)
    
  }
  
  func prepareFolderToUpload(folder: File, folderIdentifier: String) {
    
    uploadGroup.enter()
    
    createFolder(pathFolder: folderIdentifier, name: folder.name) { (folderMetadata, error) in
      
      if error == nil {
        
        folder.fetchContent()
        
        for file in folder.contentOfDirectory {
          
          switch file.fileType {
          case .image, .video:
			self.uploadArray.append((file, (folderMetadata?.metadata.pathDisplay)!))
            
          case .directory:
            self.prepareFolderToUpload(folder: file, folderIdentifier: folderMetadata!.metadata.pathDisplay!)
            
          default:
            break
            
          }
          
        }
        
      }
      
      self.uploadGroup.leave()
      
    }
    
  }
  
  func inserFile(url: URL, folderIdentifier: String) {
    
    guard let client = DropboxClientsManager.authorizedClient else { return }
    
    let filePath = "\(folderIdentifier)/\(url.lastPathComponent)"
    
    uloadRequest = client.files.upload(path: filePath, input: url)
    
    uloadRequest?.response(completionHandler: { [weak self] (object, error) in
      
      guard let strongSelf = self else { return }
      
      strongSelf.uploadWritten! += strongSelf.uploadCurrentWritten!
      strongSelf.uploadCurrentWritten = 0
      
      if strongSelf.uploadArray.count == 0 {
        strongSelf.uploadCompleate!()
      } else {
        strongSelf.uploadNext()
      }
      
    })
    
    uloadRequest?.progress({ (progress) in
      
      self.uploadCurrentWritten = Int64(progress.completedUnitCount)
      
      self.uploadProgress!((Float(self.uploadWritten!) + Float(self.uploadCurrentWritten!))/1024/1024, Float(self.uploadTotal!)/1024/1024)
      
    })
    
  }
  
  func cancelUpload() {
    
    if uloadRequest != nil {
      uloadRequest?.cancel()
    }
    
    uploadArray.removeAll()
    
  }
  
}
