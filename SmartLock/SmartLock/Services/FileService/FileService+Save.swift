//
//  FileService+Save.swift
//  SmartLock
//
//  Created by Bogachev on 8/17/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension FileService {
	
	func saveImagePNG(image: UIImage) -> URL {
		
		let newURL = getNewFileURL(directory: currentDirectory, extention: ".PNG")
		
		do {
			
			try UIImagePNGRepresentation(image)?.write(to: newURL)
			
		} catch let error as NSError {
			
			print(error.localizedDescription);
			
		}
		
		return newURL
		
	}
	
	func saveImage(image: UIImage) -> URL {
		
		let newURL = newImageURL
		
		let uniqueName = FileService.sharedInstance.verifyFileName(name: newURL.lastPathComponent)
		let uniqueFileURL = FileService.sharedInstance.currentDirectory.url.appendingPathComponent(uniqueName)
		
		print("saveImage: \(uniqueFileURL)")
		
		do {
			
			try UIImageJPEGRepresentation(image, 0.95)?.write(to: uniqueFileURL)
			
			let file = File(url: uniqueFileURL)
			
			let context = NSManagedObjectContext.mr_default()
			context.mr_save(blockAndWait: { (localContext) in
				_ = MFile.create(with: file.relativePath, context: localContext)
			})
			
			file.saveFileThumbnail()
			
		} catch let error as NSError {
			
			print(error.localizedDescription);
			
		}
		
		return newURL
		
	}
	
	func saveImageToAlbums(image: UIImage) -> URL {
		
		let newURL = newImageURLAlbums
		
		do {
			
			try UIImageJPEGRepresentation(image, 0.95)?.write(to: newURL)
			let file = File(url: newURL)
			
			let context = NSManagedObjectContext.mr_default()
			context.mr_save(blockAndWait: { (localContext) in
				_ = MFile.create(with: file.relativePath, context: localContext)
			})
			
			file.saveFileThumbnail()
			
		} catch let error as NSError {
			
			print(error.localizedDescription);
			
		}
		
		return newURL
		
	}
	
	func saveVideo(URL: URL) -> URL {
		
		let newURL = newVideoURL
		
		do {
			
			try fileManager.moveItem(at: URL, to: newURL)
			
			let file = File(url: newURL)
			
			let context = NSManagedObjectContext.mr_default()
			context.mr_save(blockAndWait: { (localContext) in
				_ = MFile.create(with: file.relativePath, context: localContext)
			})
			
			file.saveFileThumbnail()
			
		} catch let error as NSError {
			
			print(error.localizedDescription);
			
		}
		
		return newURL
		
	}
	
	func saveVideoAlbums(URL: URL) -> URL {
		
		let newURL = newVideoURLAlbums
		
		do {
			
			try fileManager.moveItem(at: URL, to: newURL)
			
			let file = File(url: newURL)
			
			let context = NSManagedObjectContext.mr_default()
			context.mr_save(blockAndWait: { (localContext) in
				_ = MFile.create(with: file.relativePath, context: localContext)
			})			
			
			file.saveFileThumbnail()
			
		} catch let error as NSError {
			
			print(error.localizedDescription);
			
		}
		
		return newURL
		
	}
	
	func saveVideo(data: NSData) -> URL {
		
		let newURL = newVideoURL
		data.write(to: newURL, atomically: true)
		
		let file = File(url: newURL)
		
		let context = NSManagedObjectContext.mr_default()
		context.mr_save(blockAndWait: { (localContext) in
			_ = MFile.create(with: file.relativePath, context: localContext)
		})
		
		file.saveFileThumbnail()
		
		return newURL
		
	}
	
	func saveBreakInPhoto(image: UIImage) -> String {
		
		let newImageName = "\(newFileName).jpg"
		
		let newURL = breakInDirectoryURL.appendingPathComponent(newImageName)
		
		do {
			
			if let data = UIImageJPEGRepresentation(image, 0) {
				
				try data.write(to: newURL)
				
			}
			
		} catch let error as NSError {
			
			print(error.localizedDescription);
			
		}
		
		return newURL.lastPathComponent
		
	}
	
}
