//
//  FileService+UploadService.swift
//  SmartLock
//
//  Created by Bogachev on 8/17/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension FileService {

	func startUploadFromService(files:[File]) {
		
		createUploadServiceDirectoryIfNeeded()
		
		for file in files {
			
			let url = file.url
			var newURL = uploadServiceDirectoryURL.appendingPathComponent(file.name)
			
			do {
				
				if fileManager.fileExists(atPath: newURL.path) {
					newURL = uploadServiceDirectoryURL.appendingPathComponent("\(file.name) (\(Date()))")
				}
				
				try fileManager.copyItem(at: url, to: newURL)
			} catch let error {
				print("\(error)")
			}
		}
		
	}
	
	func finishUploadFromService() {
		
		uploadServiceDirectory!.fetchContent()
		
		for file in uploadServiceDirectory!.contentOfDirectory {
			
			do {
				
				try fileManager.removeItem(at: file.url)
				
			} catch let error {
				
				print("\(error)")
				
			}
			
		}
		
	}
	
	func starthDownloadFromService() {
		
		createUploadServiceDirectoryIfNeeded()
		
	}
	
	func finishDownloadFromService() {
		
		uploadServiceDirectory!.fetchContent()
		
		for file in uploadServiceDirectory!.contentOfDirectory {
			
			switch file.fileType {
			case .directory:
				downloadFolderFromService(folder: FileService.sharedInstance.currentDirectory, file: file)
			case .image, .video:
				downloadFileFromService(folder: FileService.sharedInstance.currentDirectory, file: file)
			default:
				break
			}
			
		}
		
		for file in uploadServiceDirectory!.contentOfDirectory {
			
			do {
				
				try fileManager.removeItem(at: file.url)
				
			} catch let error {
				
				print("\(error)")
				
			}
			
		}
		
	}
	
	func downloadFileFromService(folder: File, file: File) {
		
		let url = file.url
		let uniqueName = FileService.sharedInstance.verifyFileName(in: folder, name: file.url.lastPathComponent)
		
		let newFileURL = folder.url.appendingPathComponent(uniqueName)
		if !newFileURL.isImage && !newFileURL.isVideo {
			return
		}
		
		do {
			
			try fileManager.moveItem(at: url, to: newFileURL)
			
			let file = File(url: newFileURL)
			
			let context = NSManagedObjectContext.mr_default()
			context.mr_save(blockAndWait: { (localContext) in
				_ = MFile.create(with: file.relativePath, context: localContext)			
			})
			
			file.saveFileThumbnail()
						
		} catch let error {
			
			print("\(error)")
			
		}
		
	}
	
	func downloadFolderFromService(folder: File, file: File) {
		
		let newFolderURL = folder.url.appendingPathComponent(file.name)
		
		File.createDirectory(atURL: newFolderURL) { (newFolder, error) in
			
			let contect = NSManagedObjectContext.mr_default()
			contect.mr_save(blockAndWait: { (localContext) in
				if let album = MAlbum.mr_createEntity(in: localContext) {
					album.mobName = newFolder?.name
					album.mobPath = newFolder?.relativePath
					album.mobAlbumPath = newFolder?.relativePath.deletingLastPathComponent()
					album.mobDate = Date().timeIntervalSince1970
				}
			})
			
			file.fetchContent()
			
			//create new
			
			for file in file.contentOfDirectory {
				
				switch file.fileType {
				case .directory:
					downloadFolderFromService(folder: newFolder!, file: file)
				case .image, .video:
					downloadFileFromService(folder: newFolder!, file: file)
				default:
					break
				}
				
			}
			
		}
		
	}

	
}
