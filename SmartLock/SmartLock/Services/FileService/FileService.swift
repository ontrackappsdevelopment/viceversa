//
//  FileService.swift
//  SmartLock
//
//  Created by Bogachev on 2/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import Photos
import YandexMobileMetrica

var rootFolderLevel: Int {
	return FileService.sharedInstance.rootMediaDirectory!.level
}

class FileService {
	
	//MARK: -
	
	let mainDirectoryName = "albums"
	let uploadServiceDirectoryName = "uploadservice"
	let breakInDirectoryName = "BreakIn"
	let decoyDirectoryName = "Decoy"
	
	//MARK: -
	
	static let sharedInstance = FileService()
		
	//MARK: -
	
	var isDecoyEnabled = false {
		didSet {
			createMainDirectoryIfNeeded()
		}
	}
	
	var fileManager: FileManager {
		return FileManager.default
	}
	
	var rootMediaDirectory: File? {
		didSet {
			currentDirectory = rootMediaDirectory
		}
	}
	
	var uploadServiceDirectory: File?
	
	var currentDirectory: File!
	
	private init() {
		createDecoyDirectoryIfNeeded() // must be first
		createMainDirectoryIfNeeded()
		createTmpDirectoryIfNeeded()
		createUploadServiceDirectoryIfNeeded()
		createBreakInDirectoryIfNeeded()
	}	
	
	//MARK: -
	
	static func fetchAlbumContent(relativePath: String) -> [NSManagedObject] {
		
		var objects = [NSManagedObject]()
				
		objects.append(contentsOf: fetchSubAlbums(relativePath: relativePath) as [NSManagedObject])
		objects.append(contentsOf: fetchAlbumFiles(relativePath: relativePath) as [NSManagedObject])
		
//		if var albums = MAlbum.mr_findAll(with: NSPredicate(format: "mobAlbumPath = %@", "\(relativePath)")) as? [MAlbum] {
//			albums.sort(by: { $0.0.mobDate < $0.1.mobDate })
//			objects.append(contentsOf: albums as [NSManagedObject])
//		}
//		
//		if var files = MFile.mr_findAll(with: NSPredicate(format: "mobFilePath = %@", "\(relativePath)")) as? [MFile] {
//			files.sort(by: { $0.0.mobDate < $0.1.mobDate })
//			objects.append(contentsOf: files as [NSManagedObject])
//		}
		
		return objects
	}
	
	static func fetchAlbumFiles(relativePath: String) -> [MFile] {
		
		var objects = [MFile]()

		if var files = MFile.mr_findAll(with: NSPredicate(format: "mobFilePath = %@", "\(relativePath)")) as? [MFile] {
			files.sort(by: { $0.0.mobDate < $0.1.mobDate })
			objects.append(contentsOf: files)
		}
		
		return objects
	}
	
	static func fetchSubAlbums(relativePath: String) -> [MAlbum] {
		
		var objects = [MAlbum]()

		if var albums = MAlbum.mr_findAll(with: NSPredicate(format: "mobAlbumPath = %@", "\(relativePath)")) as? [MAlbum] {
			albums.sort(by: { $0.0.mobDate < $0.1.mobDate })
			objects.append(contentsOf: albums)
		}
		
		return objects
	}

	
	//MARK: - App Share Extention
	
	func checkAndUploadExtentionFiles() -> Int {
		
		guard let folderURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.com.smlock.free")?.appendingPathComponent("Extension") else { return 0 }

		let extensionDirectory = File(url: folderURL)
		
		extensionDirectory.fetchContent()
		
		let mainDirectory = File(url: mainDirectoryURL)
		
		let count = extensionDirectory.contentOfDirectory.count
		
		if count > 0 {
			YMMYandexMetrica.reportEvent("Import", parameters: ["Share Extension": "Photoroll"], onFailure: nil)
		}
		
		extensionDirectory.contentOfDirectory.forEach { (file) in
			
			let uniqueName = FileService.sharedInstance.verifyFileName(in: mainDirectory, name: file.url.lastPathComponent)
			let newFileURL = mainDirectory.url.appendingPathComponent(uniqueName)
			
			if newFileURL.isImage || newFileURL.isVideo {
				let newFile = File(url: newFileURL)
				
				do {
					try FileManager.default.moveItem(at: file.url, to: newFileURL)
					
					let context = NSManagedObjectContext.mr_default()
					context.mr_save(blockAndWait: { (localContext) in
						_ = MFile.create(with: newFile.relativePath, context: localContext)
					})
					
				} catch let error as NSError {
					print("\(error.description)")
				}
			}
			
		}
		
		mainDirectory.fetchContent()
		
		return count
	}
	
	//MARK: - PhotoRoll
	
	var requestIDs = [PHImageRequestID]()
	
	let checkCancelFlagQueue = DispatchQueue(label: "checkCancelFlagQueue")
	
	var downloadingIsCanceled = false
	
	var exportFiles = [File]()
	var exportTotal: Int = 0
	var exportProgress: Int = 0
	var progressAction: ((_ exported: Int, _ total: Int) -> ())!
	var compleateAction: (() -> ())!
	
	func importFromPhotoRoll(presenter: UIViewController, assets: [PHAsset], compleation: ((_ errorMessage: String) -> ())? ) {
		
		var errorMessage = ""
		
		downloadingIsCanceled = false
		
		let startedInDecoy = LockService.sharedService.isDecoyEnabled
		
		let group = DispatchGroup()
		
//		var savedURL = [URL]()
		
		let totalItems = Float(assets.count)
		
		var downloadedLocalItems: Float = 0
		
		var progressCloudInfo = [Int:Float]()
		
		var assetsToDownload = assets
		
		assetsToDownload.sort(by: { $0.0.creationDate! > $0.1.creationDate! })
		
		let progressView = ProgressView.showProgressView(inView: presenter.view, labelType: .items, cancel: {
			FileService.sharedInstance.cancelDownloading()
		})
		
		func saveProgressInfo(progress: Double, info: [AnyHashable : Any]) {
			
			DispatchQueue.main.async {
				
				if let object = info[PHImageResultRequestIDKey] {
					let key = object as! Int
					
					if progress >= 1 {
						
						progressCloudInfo[key] = 0
						
					} else {
						
						progressCloudInfo[key] = Float(progress)
						progressView.setProgressLabel(downloaded: getTotalProgress(), total: totalItems)
						
					}
					
				}
				
			}
			
		}
		
		func increaseProgress() {
			DispatchQueue.main.async {
				downloadedLocalItems += 1
				progressView.setProgressLabel(downloaded: getTotalProgress(), total: totalItems)
			}
		}
		
		func getTotalProgress() -> Float {
			return downloadedLocalItems + progressCloudInfo.flatMap({ $0.value }).reduce(0, +)
		}
		
		func donwloadNext() {
			
			guard LockService.sharedService.isDecoyEnabled == startedInDecoy else {
				cancelDownloading()
				return
			}
			
			if assetsToDownload.count == 0 {
				
				guard AppDelegate.appIsLocked == false else {
					
					DispatchQueue.main.async {
						progressView.compleateMode {
							finishDownload()
						}
					}
					
					return
				}
				
				finishDownload()
				return
			}
			
			let currentAsset = assetsToDownload.removeLast()
			
			if self.checkDownloadingForCancel() {
				finishDownload()
				return
			}
			
			switch currentAsset.mediaType {
			case .image:
				
				let options = PHImageRequestOptions()
				options.isSynchronous = false
				options.isNetworkAccessAllowed = true
				options.progressHandler =  {(progress, error, pointer, info) -> () in
					saveProgressInfo(progress: progress, info: info!)
				}
				
				self.requestIDs.append(PHImageManager.default().requestImageData(for: currentAsset, options: options, resultHandler: { (data, fileName, orientation, info) in
					
					guard data != nil else {
						increaseProgress()
						donwloadNext()
						
						errorMessage = "Some of your iCloud files \n haven't been imported.\n Check your Internet connection and try again."
						
						return
					}
					
					let extensionTokenKey = info!["PHImageFileSandboxExtensionTokenKey"]! as! String
					
					let path = extensionTokenKey.components(separatedBy: ";").last
					let fileName = path?.components(separatedBy: "/").last
					
					let uniqueName = FileService.sharedInstance.verifyFileName(name: fileName!)
					let newFileURL = FileService.sharedInstance.currentDirectory.url.appendingPathComponent(uniqueName)
					
					if let data = data, newFileURL.isImage == true {
						
						do {
							
							try data.write(to: newFileURL, options: Data.WritingOptions.atomicWrite)
//							savedURL.append(newFileURL)
							let file = File(url: newFileURL)
							
							let context = NSManagedObjectContext.mr_default()
							context.mr_save(blockAndWait: { (localContext) in
								_ = MFile.create(with: file.relativePath, context: localContext)
							})
							
							file.saveFileThumbnail()
							
						} catch {
							
						}
						
					}
					
					increaseProgress()
					donwloadNext()
					
				}))
				
			case .video:
				
				let options = PHVideoRequestOptions()
				options.isNetworkAccessAllowed = true
				options.progressHandler = {(progress, error, pointer, info) -> () in
					saveProgressInfo(progress: progress, info: info!)
				}
				
				self.requestIDs.append(PHImageManager.default().requestAVAsset(forVideo: currentAsset, options: options, resultHandler: { (asset, audioMix, info) -> Void in
					
					if let asset = asset {
						
						if let composition = asset as? AVComposition, composition.tracks.count == 2 {
							
							//              let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
							//              let documentsDirectory = paths.first
							//              let myPathDocs = documentsDirectory?.appending("filename.mov")
							//              let url = URL(string: myPathDocs!)
							
							let extensionTokenKey = info!["PHImageFileSandboxExtensionTokenKey"]! as! String
							
							print("\(extensionTokenKey)")
							
							let path = extensionTokenKey.components(separatedBy: ";").last
							let fileName = path?.components(separatedBy: "/").last
							
							let uniqueName = FileService.sharedInstance.verifyFileName(name: fileName!)
							let newFileURL = FileService.sharedInstance.currentDirectory.url.appendingPathComponent(uniqueName)
							
							if newFileURL.isVideo {
							
								if let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) {
								
									exporter.outputURL = newFileURL
									exporter.outputFileType = AVFileTypeQuickTimeMovie
									exporter.shouldOptimizeForNetworkUse = true
									exporter.exportAsynchronously {
										
										DispatchQueue.main.async {
											
											if exporter.status == .completed {
												
												let file = File(url: newFileURL)
												
												let context = NSManagedObjectContext.mr_default()
												context.mr_save(blockAndWait: { (localContext) in
													_ = MFile.create(with: file.relativePath, context: localContext)
												})
												
												file.saveFileThumbnail()
												
//												savedURL.append((exporter.outputURL)!)
												
												increaseProgress()
												
												donwloadNext()
												
											}
											
										}
										
									}
									
								}
								
							}
							
						} else {
							
							let extensionTokenKey = info!["PHImageFileSandboxExtensionTokenKey"]! as! String
							if let path = extensionTokenKey.components(separatedBy: ";").last,
								let fileName = path.components(separatedBy: "/").last {
								
								let uniqueName = FileService.sharedInstance.verifyFileName(name: fileName)
								let newFileURL = FileService.sharedInstance.currentDirectory.url.appendingPathComponent(uniqueName)
								if newFileURL.isVideo {
									if let urlAsset = asset as? AVURLAsset, let videoData = NSData(contentsOf: urlAsset.url) {
										let _ = videoData.write(to: newFileURL, atomically: true)
										
										let file = File(url: newFileURL)
										
										let context = NSManagedObjectContext.mr_default()
										context.mr_save(blockAndWait: { (localContext) in
											_ = MFile.create(with: file.relativePath, context: localContext)
										})
										
										file.saveFileThumbnail()
										
//										savedURL.append(newFileURL)
									}
								}
								
							}
							
							increaseProgress()
							
							donwloadNext()
							
						}
						
					} else {
						increaseProgress()
						donwloadNext()
					}
					
				}))
				
			default : donwloadNext()
			}
			
		}
		
		func finishDownload() {
			
			if !self.checkDownloadingForCancel() {
				
//				savedURL.forEach({ (url) in
//					let file = File(url: url)
//					
//					let context = NSManagedObjectContext.mr_default()
//					context.mr_save(blockAndWait: { (localContext) in
//						_ = MFile.create(with: file.relativePath, context: localContext)
//					})
//					
//				})
				
			} else {
				
//				for ulr in savedURL {
//					
//					do {
//						
//						try FileManager.default.removeItem(at: ulr)
//						
//					} catch let error {
//						
//						print("\(error.localizedDescription)")
//						
//					}
//					
//				}
				
			}
			
			DispatchQueue.main.async {
				progressView.close()
				
				if compleation != nil {
					compleation!(errorMessage)
				}
			}
			
		}
		
		donwloadNext()
		
	}
	
	func cancelDownloading() {
		
		checkCancelFlagQueue.async {
			
			self.downloadingIsCanceled = true
			
			print("Set downloadingIsCancel \(self.downloadingIsCanceled)")
			
			for requestID in self.requestIDs {
				
				PHImageManager.default().cancelImageRequest(requestID)
				
			}
			
		}
		
	}
	
	func checkDownloadingForCancel() -> Bool {
		
		var isCanceledFlag = false
		
		checkCancelFlagQueue.sync {
			
			isCanceledFlag = self.downloadingIsCanceled
			
			print("GET downloadingIsCancel \(isCanceledFlag)")
			
			
		}
		
		return isCanceledFlag
		
	}
	
	func exportToPhotoRoll(files: [File], progress: ((Int, Int) -> ())?, compleation: (() -> ())? ) {
		
		exportFiles = files
		
		exportTotal = exportFiles.count
		
		progressAction = progress
		
		compleateAction = compleation
		
		exportNextToPhotoRoll()
		
	}
	
	func exportNextToPhotoRoll() {
		
		if exportFiles.count == 0 {
			
			compleateAction()
			
			return
			
		}
		
		let file = exportFiles.removeFirst()
		
		switch file.fileType {
		case .video:
			
			createAssetFromVideo(file: file, compleation: {
				
				self.exportNextToPhotoRoll()
				
			})
			
		case .image:
			
			createAssetFromImage(file: file, compleation: {
				
				self.exportNextToPhotoRoll()
				
			})
			
		default:
			break
		}
		
	}
	
	func createAssetFromImage(file: File, compleation: @escaping () -> ()) {
		
		PHPhotoLibrary.shared().performChanges({
			
			PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: file.url)
			
		}) { (success, error) in
			
			self.exportProgress += 1
			
			self.progressAction(self.exportProgress, self.exportTotal)
			
			compleation()
			
		}
		
	}
	
	func createAssetFromVideo(file: File, compleation: @escaping () -> ()) {
		
		PHPhotoLibrary.shared().performChanges({
			
			PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: file.url)
			
		}) { (success, error) in
			
			self.exportProgress += 1
			
			self.progressAction(self.exportProgress, self.exportTotal)
			
			compleation()
			
		}
		
	}

}

enum FileErrors : Error {
	case BadEnumeration
	case BadResource
}

func findSize(path: String) throws -> UInt64 {
	
	let fullPath = (path as NSString).expandingTildeInPath
	let fileAttributes: NSDictionary = try FileManager.default.attributesOfItem(atPath: fullPath) as NSDictionary
	
	if let fileType = fileAttributes.fileType(), fileType == FileAttributeType.typeRegular.rawValue {
		return fileAttributes.fileSize()
	}
	
	let url = NSURL(fileURLWithPath: fullPath)
	guard let directoryEnumerator = FileManager.default.enumerator(at: url as URL, includingPropertiesForKeys: [URLResourceKey.fileSizeKey], options: [.skipsHiddenFiles], errorHandler: nil) else { throw FileErrors.BadEnumeration }
	
	var total: UInt64 = 0
	
	for (index, object) in directoryEnumerator.enumerated() {
		guard let fileURL = object as? NSURL else { throw FileErrors.BadResource }
		var fileSizeResource: AnyObject?
		try fileURL.getResourceValue(&fileSizeResource, forKey: URLResourceKey.fileSizeKey)
		guard let fileSize = fileSizeResource as? NSNumber else { continue }
		total += fileSize.uint64Value
		if index % 1000 == 0 {
			print(".", terminator: "")
		}
	}
	return total
}

