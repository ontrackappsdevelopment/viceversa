//
//  GoogleService.swift
//  SmartLock
//
//  Created by Bogachev on 2/7/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import GoogleAPIClient
import AppAuth
import GTMAppAuth

enum DownloadStatus:String {
	case undefined = "undefined"
	case started = "started"
	case finished = "finished"
	case canceled = "canceled"
}

class GoogleServiceDownloadObserver: NSObject {
	
	var progressAction: ((_ bytesWritten: Int64, _ totalBytes: Int64) -> ())!
	var finishAction: (() -> ())!
	var cancelAction: (() -> ())!
	
	init(progress: @escaping (Int64, Int64) -> (), finish: @escaping () -> (), cancel: @escaping () -> ()) {
		super.init()
		
		progressAction = progress
		finishAction = finish
		cancelAction = cancel
		
		GoogleService.sharedInstance.addObserver(self, forKeyPath: "currentSize", options: .new, context: nil)
		GoogleService.sharedInstance.addObserver(self, forKeyPath: "downloadStatus", options: .new, context: nil)
	}
	
	deinit {
		GoogleService.sharedInstance.removeObserver(self, forKeyPath: "currentSize", context: nil)
		GoogleService.sharedInstance.removeObserver(self, forKeyPath: "downloadStatus", context: nil)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		
		guard keyPath != nil else { return }
		
		switch keyPath! {
		case "currentSize":
			
			if let action = progressAction {
				
				let total = GoogleService.sharedInstance.totalSize
				if let newValue = change?[.newKey] {
					action(newValue as! Int64, total)
				}
				
			}
			
		case "downloadStatus":
			
			if let newValue = change?[.newKey] {
				
				let status = newValue as! String
				
				switch status {
				case DownloadStatus.finished.rawValue:
					
					if let action = finishAction {
						
						action()
						
					}
					
				case DownloadStatus.canceled.rawValue:
					
					if let action = cancelAction {
						
						action()
						
					}
					
				default:
					break
				}
				
			}
			
		default: break
		}
		
	}
}

class GoogleService: NSObject, OIDAuthStateChangeDelegate, OIDAuthStateErrorDelegate {
	
	static let sharedInstance = GoogleService()
	
	let kIssuer = "https://accounts.google.com"
	let kClientID = "748264322596-p2uhqfntdcd130jgkteha5ensi1dqlkg.apps.googleusercontent.com"
	let kRedirectURI = "com.googleusercontent.apps.748264322596-p2uhqfntdcd130jgkteha5ensi1dqlkg:/oauthredirect"
	let kAuthorizerKey = "googleAuthorization"
	
	var currentAuthorizationFlow: OIDAuthorizationFlowSession?
	
	lazy var serviceDrive = GTLServiceDrive()
	
	fileprivate var driveFilesToUpload = [GTLDriveFile]()
	fileprivate var driveFilesToDelete = [GTLDriveFile]()
	
	fileprivate var fetcher: GTMSessionFetcher?
	
	fileprivate var metadataQuery: NSMetadataQuery?
	
	dynamic var currentSize: NSNumber = 0
	
	fileprivate var totalSize: Int64 = 0
	
	fileprivate var progressInfo = [String:Int64]()
	
	dynamic var downloadStatus: NSString = DownloadStatus.undefined.rawValue as NSString
	
	fileprivate var savedFilesURL = [URL]()
	
	fileprivate var uploadCompleation: (() -> ())?
	
	fileprivate var autorizationCompleation: (() -> ())?
	
	fileprivate var authorization: GTMAppAuthFetcherAuthorization? {
		didSet {
			serviceDrive.authorizer = authorization
			saveAuthorization()
		}
	}
	
	func resumeAuthorizationFlowWithURL(url: URL) -> Bool {
		
		if let currentFlow = currentAuthorizationFlow {
			
			if currentFlow.resumeAuthorizationFlow(with: url) {
				currentAuthorizationFlow = nil
				return true
			}
			
		}
		
		return false
	}
	
	func isAuthorized() -> Bool {
		
		loadAuthorization()
		
		if let auth = authorization, auth.canAuthorize() == true {
			return true
		}
		
		return false
	}
	
	func fetchAuthorization(presenter: UIViewController, compleation: @escaping () -> () ) {
		
		loadAuthorization()
		
		if let auth = authorization, auth.canAuthorize() == true {
			compleation()
			return
		}
		
		self.autorizationCompleation = compleation
		
		let issuerURL = URL(string: kIssuer)
		let redirectURL = URL(string: kRedirectURI)
		
		OIDAuthorizationService.discoverConfiguration(forIssuer: issuerURL!) {[weak self] (configuration, error) in
			
			guard let conf = configuration else { return }
			guard let strongSelf = self else { return }
			
			let request = OIDAuthorizationRequest(configuration: conf, clientId: strongSelf.kClientID, scopes: [OIDScopeOpenID, OIDScopeProfile, kGTLAuthScopeDriveMetadataReadonly, kGTLAuthScopeDrive], redirectURL: redirectURL!, responseType: OIDResponseTypeCode, additionalParameters: nil)
			
			strongSelf.currentAuthorizationFlow = OIDAuthState.authState(byPresenting: request, presenting: presenter, callback: { (authState, error) in
				
				if let state = authState {
					
					strongSelf.authorization = GTMAppAuthFetcherAuthorization(authState: state)
					
					if strongSelf.autorizationCompleation != nil {
						strongSelf.autorizationCompleation!()
					}
					
				} else {
					
					strongSelf.authorization = nil
					
				}
				
			})
			
		}
		
	}
	
	func fetchGoogleDriveContent(folderIdentifier: String, compleation: @escaping ([GTLDriveFile]) -> () ) {
		
		let query = GTLQueryDrive.queryForFilesList()
		query?.fields = "kind,nextPageToken,files(mimeType,id,kind,name,webViewLink,thumbnailLink,size)";
		query?.q = "'\(folderIdentifier)' in parents AND (mimeType = 'video/mpeg' OR mimeType = 'image/png' OR mimeType = 'image/jpeg' OR mimeType = 'video/quicktime' OR mimeType = 'application/vnd.google-apps.folder')" //
		
		serviceDrive.shouldFetchNextPages = true
		
		serviceDrive.executeQuery(query!) { (ticket, fileList, error) in
			
			var fetchedDriveFiles = [GTLDriveFile]()
			
			if error == nil {
				let fetchedDriveFileList = fileList as! GTLDriveFileList
				
				if fetchedDriveFileList.files != nil {
					fetchedDriveFiles = fetchedDriveFileList.files as! [GTLDriveFile]
				}
			}
			
			compleation(fetchedDriveFiles)
		}
		
	}
	
	//MARK: - Download
	
	func download(driveFiles: [GTLDriveFile], compleation: @escaping () -> ()) {
		
		progressInfo.removeAll()
		
		driveFilesToUpload = driveFiles
		
		driveFilesToDelete = driveFilesToUpload
		
		uploadCompleation = compleation
		
		totalSize = driveFilesToUpload.flatMap({ $0.size.intValue }).reduce(0, +)
		
		downloadStatus = DownloadStatus.started.rawValue as NSString
		
		savedFilesURL.removeAll()
		
		startDownloadNextFile()
		
	}
	
	var driveFile: GTLDriveFile?
	
	func startDownloadNextFile() {
		
		guard LockService.sharedService.isDecoyEnabled == false else {
			cancelDownload()
			return
		}
		
		guard driveFilesToUpload.count > 0 else { return }
		
		driveFile = driveFilesToUpload.removeFirst()
		
		fetcher = serviceDrive.fetcherService.fetcher(withURLString: "https://www.googleapis.com/drive/v3/files/\(driveFile!.identifier ?? "")?alt=media")
		
		fetcher!.receivedProgressBlock = { (bytesWritten: Int64, totalBytesWritten: Int64) in
			
			let identifier = self.driveFile!.identifier
			
			self.progressInfo[identifier!] = totalBytesWritten
			
			self.currentSize = self.progressInfo.flatMap({ $0.value }).reduce(0, +) as NSNumber
			
		}
		
		fetcher!.beginFetch(withDelegate: self, didFinish: #selector(finishedWithData(fetcher:data:error:)))
	}
	
	func cancelDownload() {
		
		driveFilesToUpload.removeAll()
		
		if fetcher != nil {
			fetcher?.stopFetching()
		}
		
		FileManager.default.delete(urls: savedFilesURL)
		
		downloadStatus = DownloadStatus.canceled.rawValue as NSString
		
	}
	
	func finishedWithData(fetcher: GTMSessionFetcher, data: NSData, error: NSError?) {
		
		guard LockService.sharedService.isDecoyEnabled == false else {
			cancelDownload()
			return
		}
		
		if error == nil {
			
			let uniqueName = FileService.sharedInstance.verifyFileName(name: driveFile!.name)
			let newFileURL = FileService.sharedInstance.currentDirectory.url.appendingPathComponent(uniqueName)
			
			if newFileURL.isImage || newFileURL.isVideo {
				savedFilesURL.append(newFileURL)
				data.write(to: newFileURL, atomically: true)
			}
			
		}
		
		if driveFilesToUpload.count == 0 {
			
			currentSize = totalSize as NSNumber
			
			downloadStatus = DownloadStatus.finished.rawValue as NSString
			
			savedFilesURL.forEach({ (url) in
				let file = File(url: url)
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					_ = MFile.create(with: file.relativePath, context: localContext)
				})
				
				file.saveFileThumbnail()

			})
			
			if let action = uploadCompleation {
				action()
			}
			
		} else {
			
			startDownloadNextFile()
			
		}
		
	}
	
	//MARK: - Upload
	
	var uploadArray = [(File, String)]()
	
	let uploadGroup = DispatchGroup()
	
	var uploadCompleate: (() -> ())?
	var uploadProgress: ((Float, Float) -> ())?
	
	var uploadTotal: Int64?
	var uploadWritten: Int64?
	var uploadCurrentWritten: Int64?
	
	var serviceTicket:GTLServiceTicket?
	
	func upload(files: [File], folderIdentifier: String, progress: @escaping (Float, Float) -> (), compleate: @escaping () -> ()) {
		
		uploadProgress = progress
		uploadCompleate = compleate
		
		DispatchQueue.global().async {
			
			for file in files {
				
				switch file.fileType {
				case .image, .video:
					self.uploadArray.append((file, folderIdentifier))
					
				case .directory:
					self.prepareFolderToUpload(folder: file, folderIdentifier: folderIdentifier)
					
				default:
					break
				}
				
			}
			
			self.uploadGroup.wait()
			
			DispatchQueue.main.async {
				
				self.uploadTotal = self.uploadArray.flatMap({ $0.0.size }).reduce(0, +)
				self.uploadWritten = 0
				self.uploadCurrentWritten = 0
				
				self.uploadNext()
				
			}
			
		}
		
	}
	
	func uploadNext() {
		
		guard LockService.sharedService.isDecoyEnabled == false else {
			cancelUpload()
			return
		}
		
		let fileInfo = uploadArray.removeFirst()
		
		inserFile(url: fileInfo.0.url, folderIdentifier: fileInfo.1)
		
	}
	
	func prepareFolderToUpload(folder: File, folderIdentifier: String) {
		
		uploadGroup.enter()
		
		createFolder(identifier: folderIdentifier, name: folder.name) { (driveFile, error) in
			
			if error == nil {
				
				folder.fetchContent()
				
				for file in folder.contentOfDirectory {
					
					switch file.fileType {
					case .image, .video:
						self.uploadArray.append((file, driveFile!.identifier))
						
					case .directory:
						self.prepareFolderToUpload(folder: file, folderIdentifier: driveFile!.identifier)
						
					default:
						break
						
					}
					
				}
				
			}
			
			self.uploadGroup.leave()
			
		}
		
	}
	
	func createFolder(identifier: String, name: String, compleation: @escaping (_ file: GTLDriveFile?, _ error: Error?) -> ()) {
		
		let metadata = GTLDriveFile()
		metadata.parents = [identifier]
		metadata.name = name
		metadata.mimeType = "application/vnd.google-apps.folder"
		
		let query = GTLQueryDrive.queryForFilesCreate(withObject: metadata, uploadParameters: nil)
		query?.fields = "mimeType,id,kind,name,webViewLink,thumbnailLink,size";
		serviceDrive.executeQuery(query!) { (ticket, file, error) in
			
			if error == nil {
				compleation(file as? GTLDriveFile, nil)
			} else {
				compleation(nil, error)
			}
		}
	}
	
	func inserFile(url: URL, folderIdentifier: String) {
		
		let fileData = FileManager.default.contents(atPath: url.path)
		let metadata = GTLDriveFile()
		metadata.name = url.lastPathComponent
		metadata.parents = [folderIdentifier]
		
		let uploadParameters = GTLUploadParameters(data: fileData!, mimeType: url.mimeType())
		let query = GTLQueryDrive.queryForFilesCreate(withObject: metadata, uploadParameters: uploadParameters)
		query!.fields = "id"
		
		serviceTicket = serviceDrive.executeQuery(query!) { (ticket, file, error) in
			
			self.uploadWritten! += self.uploadCurrentWritten!
			self.uploadCurrentWritten = 0
			
			if self.uploadArray.count == 0 {
				self.uploadCompleate!()
			} else {
				self.uploadNext()
			}
			
		}
		
		serviceTicket?.uploadProgressBlock = { (ticket, totalBytesUploaded, totalBytesExpectedToUpload) in
			
			self.uploadCurrentWritten = Int64(totalBytesUploaded)
			
			self.uploadProgress!((Float(self.uploadWritten!) + Float(self.uploadCurrentWritten!))/1024/1024, Float(self.uploadTotal!)/1024/1024)
			
		}
		
	}
	
	func cancelUpload() {
		
		if serviceTicket != nil {
			serviceTicket?.cancel()
		}
		
		uploadArray.removeAll()
		
	}
	
	//MARK: -
	
	func loadAuthorization() {
		
		authorization = GTMAppAuthFetcherAuthorization(fromKeychainForName: kAuthorizerKey)
		
	}
	
	func saveAuthorization() {
		
		if let auth = authorization, auth.canAuthorize() == true {
			
			GTMAppAuthFetcherAuthorization.save(auth, toKeychainForName: kAuthorizerKey)
			
		} else {
			
			GTMAppAuthFetcherAuthorization.removeFromKeychain(forName: kAuthorizerKey)
			
		}
		
	}
	
	func logout() {
		GTMAppAuthFetcherAuthorization.removeFromKeychain(forName: kAuthorizerKey)
	}
	
	func authState(_ state: OIDAuthState, didEncounterTransientError error: Error) {
		
		
		
	}
	
	func authState(_ state: OIDAuthState, didEncounterAuthorizationError error: Error) {
		
		
		
	}
	
	func didChange(_ state: OIDAuthState) {
		
		authorization = GTMAppAuthFetcherAuthorization(authState: state)
		
	}
	
	func deleteFileWithService() {
		
		for file in driveFilesToDelete {
			
			let query = GTLQueryDrive.queryForFilesDelete(withFileId: file.identifier)
			serviceDrive.executeQuery(query!, completionHandler: { (ticket, onject, error) in
				
				print("\(error)")
				
			})
			
		}
	}
}
