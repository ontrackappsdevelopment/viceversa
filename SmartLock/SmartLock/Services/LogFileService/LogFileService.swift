//
//  LogFileService.swift
//  SmartLock
//
//  Created by Bogachev on 4/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

enum LogActionType: String {
	case create = "create"
	case delete = "delete"
	case move = "move"
	case rename = "rename"
	case addpasscode = "addpasscode"
	case deletepasscode = "deletepasscode"
	case changeEmail = "change_email"
	
}

enum LogFileType: Int16 {
	case file
	case directory
}

class LogFileService {
	
	static func move(from fileFrom: File, to fileTo: File) {
		
		let context = NSManagedObjectContext.mr_default()
		
		let predicateCreated = NSPredicate(format: "mobPath == %@ AND mobAction == %@", fileFrom.relativePath, LogActionType.create.rawValue)
		let predicateMoved = NSPredicate(format: "mobPath == %@ AND mobAction == %@", fileFrom.relativePath, LogActionType.move.rawValue)
		
		if let log = MLog.mr_findFirst(with: predicateCreated, in: context) {
			
			context.mr_save(blockAndWait: { (localContext) in
				if let localLog = log.mr_(in: localContext) {
					localLog.mr_deleteEntity(in: localContext)
				}
			})
			
		} else if let log = MLog.mr_findFirst(with: predicateMoved, in: context) {
			
			context.mr_save(blockAndWait: { (localContext) in
				if let localLog = log.mr_(in: localContext) {
					localLog.mr_deleteEntity(in: localContext)
				}
			})
			
			if let oldPath = log.mobOldPath, let url = URL(string: oldPath) {
				move(from: File(url: url), to: fileTo)
			}
			
		} else {
			
			context.mr_save(blockAndWait: { (localContext) in
				if let log = MLog.mr_createEntity(in: localContext) {
					if fileTo.relativePath != fileFrom.relativePath {
						log.mobType = fileTo.fileType == .directory ? LogFileType.directory.rawValue : LogFileType.file.rawValue
						log.mobPath = fileTo.relativePath
						log.mobOldPath = fileFrom.relativePath
						log.mobAction = LogActionType.move.rawValue
						log.mobDate = NSDate().timeIntervalSince1970
					}
				}
			})
			
		}
		
	}
	
}
