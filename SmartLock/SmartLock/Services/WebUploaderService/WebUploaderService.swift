//
//  WebUploaderService.swift
//  SmartLock
//
//  Created by Bogachev on 2/27/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

enum TypeService {
	case download
	case upload
}

class WebUploaderService: NSObject, GCDWebUploaderDelegate {
	
	static let sharedInstance = WebUploaderService()
	
	var webServer: GCDWebUploader!
	
	var typeService: TypeService = .download
	
	func start(typeService: TypeService) -> String? {
		
		self.typeService = typeService
		
		switch typeService {
		case .download:
			FileService.sharedInstance.starthDownloadFromService()
		case .upload:
			
			DispatchQueue.global().async {
				FileService.sharedInstance.startUploadFromService(files: PhotosViewController.filesToUpload)
			}
			
		}
		
		webServer = GCDWebUploader(uploadDirectory: FileService.sharedInstance.uploadServiceDirectoryURL.path)
		webServer.delegate = self
		webServer.allowHiddenItems = true
		
		if webServer.start() {
			
			print("GCDWebServer running locally on port \(webServer.serverURL)")
			
			if let serverURL = webServer.serverURL {
				return "\(serverURL.absoluteString)"
			} else {
				return nil
			}
						
		} else {
			
			print("GCDWebServer not running!")
			return nil
			
		}
		
	}
	
	func stop() {
		
		switch typeService {
		case .download: FileService.sharedInstance.finishDownloadFromService()
		case .upload: FileService.sharedInstance.finishUploadFromService()
		}
		
		if webServer != nil {
			webServer.stop()
			webServer = nil
		}
		
	}
	
	func webUploader(_ uploader: GCDWebUploader!, didUploadFileAtPath path: String!) {
		
	}
	
	func webUploader(_ uploader: GCDWebUploader!, didMoveItemFromPath fromPath: String!, toPath: String!) {
		
	}
	
	func webUploader(_ uploader: GCDWebUploader!, didCreateDirectoryAtPath path: String!) {
		
	}
	
	func webUploader(_ uploader: GCDWebUploader!, didDownloadFileAtPath path: String!) {
		
	}
	
	func webUploader(_ uploader: GCDWebUploader!, didDeleteItemAtPath path: String!) {
		
	}
}
