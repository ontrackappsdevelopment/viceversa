//
//  YandexService.swift
//  SmartLock
//
//  Created by Bogachev on 2/12/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

class YandexServiceDownloadObserver: NSObject {
	
	var progressAction: ((_ bytesWritten: Int64, _ totalBytes: Int64) -> ())!
	var finishAction: (() -> ())!
	var cancelAction: (() -> ())!
	
	
	init(progress: @escaping (Int64, Int64) -> (), finish: @escaping () -> (), cancel: @escaping () -> ()) {
		super.init()
		
		progressAction = progress
		finishAction = finish
		cancelAction = cancel
		
		YandexService.sharedInstance.addObserver(self, forKeyPath: "writtenBytes", options: .new, context: nil)
		YandexService.sharedInstance.addObserver(self, forKeyPath: "currentWritten", options: .new, context: nil)
		YandexService.sharedInstance.addObserver(self, forKeyPath: "downloadStatus", options: .new, context: nil)
	}
	
	deinit {
		YandexService.sharedInstance.removeObserver(self, forKeyPath: "writtenBytes", context: nil)
		YandexService.sharedInstance.removeObserver(self, forKeyPath: "currentWritten", context: nil)
		YandexService.sharedInstance.removeObserver(self, forKeyPath: "downloadStatus", context: nil)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		
		guard keyPath != nil else { return }
		
		switch keyPath! {
		case "writtenBytes":
			
			if let action = progressAction {
				
				let total = YandexService.sharedInstance.totalBytes
				var written = YandexService.sharedInstance.currentWritten.int64Value
				
				if let newValue = change?[.newKey] {
					written += newValue as! Int64
					action(written, total)
				}
				
			}
			
			
		case "currentWritten":
			
			if let action = progressAction {
				
				let total = YandexService.sharedInstance.totalBytes
				var written = YandexService.sharedInstance.writtenBytes.int64Value
				
				if let newValue = change?[.newKey] {
					written += newValue as! Int64
					action(written, total)
				}
				
			}
			
		case "downloadStatus":
			
			if let newValue = change?[.newKey] {
				
				let status = newValue as! String
				
				switch status {
				case DownloadStatus.finished.rawValue:
					
					if let action = finishAction {
						
						action()
						
					}
					
				case DownloadStatus.canceled.rawValue:
					
					if let action = cancelAction {
						
						action()
						
					}
					
				default:
					break
				}
				
			}
			
		default: break
		}
		
	}
}


class YandexService: NSObject, YDSessionDelegate, YOAuth2Delegate, URLSessionDataDelegate, URLSessionDownloadDelegate, URLSessionTaskDelegate {
	
	static let sharedInstance = YandexService()
	
	var authSession: YDSession!
	var downloadSession: URLSession!
	var itemsToDownload = [YDItem]()
	var itemsToDelete = [YDItem]()
	
	var downloadCompleation: (() -> ())?
	
	var downloadTask: URLSessionDownloadTask?
	var uploadTask: URLSessionUploadTask?
	var dataTask: URLSessionDataTask?
	var newURL: URL?
	
	var totalBytes: Int64 = 0
	dynamic var writtenBytes: NSNumber = 0
	dynamic var currentWritten: NSNumber = 0
	fileprivate var savedFilesURL = [URL]()
	
	dynamic var downloadStatus: NSString = DownloadStatus.undefined.rawValue as NSString
	
	private var auth2ViewController: YOAuth2ViewController?
	
	var autorizationCompleation: (() -> ())?
	
	override init() {
		super.init()
		authSession = YDSession(delegate: self)
		authSession.oAuthToken = loadToken()
	}
	
	func isAuthorized() -> Bool {
		return authSession.oAuthToken != nil
	}
	
	func logout() {
		
		deleteToken()
		
		authSession.oAuthToken = nil
		
	}
	
	func fetchAuthorization(presenter: UIViewController, compleation: @escaping () -> () ) {
		
		autorizationCompleation = compleation
		
		if authSession.oAuthToken == nil {
			
			auth2ViewController = YOAuth2ViewController(delegate: self)
			auth2ViewController?.pushFrom(viewController: presenter)
			
		} else {
			
			compleation()
			
		}
		
	}
	
	//MAKR: - YDSessionDelegate
	
	func userAgent() -> String! {
		return "disk-sdk-smarklock";
	}
	
	//MAKR: - YOAuth2Delegate
	
	func clientID() -> String! {
		return "2a552371613f4821b15ece541baec52c"
	}
	
	func redirectURL() -> String! {
		return ""
	}
	
	public let baseURL = "https://cloud-api.yandex.net:443"
	
	func oAuthLoginSucceeded(withToken token: String!) {
		authSession.oAuthToken = token
		let _ = auth2ViewController?.navigationController?.popViewController(animated: true)
	}
	
	func oAuthLoginFailedWithError(_ error: Error!) {
		let _ = auth2ViewController?.navigationController?.popViewController(animated: true)
	}
	
	func handleURL(_ url: URL) -> Bool {
		var querydict : [String: String] = [:]
		
		let fragment = url.absoluteString
		for tuple in fragment.components(separatedBy: "&") {
			let nv = tuple.components(separatedBy: "=") as [NSString]
			
			switch (nv[0], nv[1]) {
			case (var name, let value):
				name = name.contains("access_token") ? "access_token" : name
				querydict[name as String] = value as String
			}
		}
		
		if let token = querydict["access_token"] {
			authSession.oAuthToken = token
			
			saveToken(token)
			
			if autorizationCompleation != nil {
				autorizationCompleation!()
				autorizationCompleation = nil
			}
		}
		return true
	}
	
	fileprivate func saveToken(_ token: String) {
		KeychainService.savePassword(token: token as NSString)
	}
	
	fileprivate func loadToken() -> String? {
		return KeychainService.loadPassword() as String?
	}
	
	fileprivate func deleteToken() {
		KeychainService.deletePassword(token: authSession.oAuthToken as NSString)
	}
	
	//MARK: = Fetch Content
	
	func fetchYandexDriveContent(folderIdentifier: String, compleation: @escaping ([YDItem]) -> () ) {
		
		configurateDownloadSession()
		
		let folderIdentifierPath = folderIdentifier.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? ""
		
		downloadSession.dataTask(with: URL(string: "https://cloud-api.yandex.net:443/v1/disk/resources?path=\(folderIdentifierPath)")!) { (data, response, error) in
			
			do {
				
				let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
				
				if let embedded = (json as! NSDictionary).object(forKey: "_embedded"),
					let items = (embedded as! NSDictionary).object(forKey: "items") {
					
					var resultItems = [YDItem]()
					
					for itemJSON in items as! NSArray {
						
						let item = YDItem(props: itemJSON as! NSDictionary)
						
						resultItems.append(item)
						
					}
					
					compleation(resultItems)
					
				}
				
			} catch let error as NSError {
				print("\(error)")
			}
			
			}.resume()
		
	}
	
	func configurateDownloadSession() {
		
		guard downloadSession == nil else {
			return
		}
		
		let sessionConfig = URLSessionConfiguration.default
		sessionConfig.httpAdditionalHeaders = [
			"Accept"        :   "application/json",
			"Authorization" :   "OAuth \(self.authSession.oAuthToken!)",
			"User-Agent"    :   "Yandex Disk swift SDK"]
		
		sessionConfig.httpShouldUsePipelining = true
		
		downloadSession = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
		
	}
	
	func thambnailTask(item: YDItem, compleation: ((UIImage) -> ())? ) -> URLSessionDataTask? {
		
		if item.type == "file" {
			
			return downloadSession.dataTask(with: URL(string: item.preview!)!, completionHandler: { (data, response, error) in
				
				if error == nil {
					
					if let image = UIImage(data: data!) {
						
						compleation!(image)
						
						return
						
					}
					
				}
				
			})
			
		}
		
		return nil
		
	}
	
	//MARK: - Download
	
	func download(driveFiles: [YDItem], compleation: @escaping () -> ()) {
		
		itemsToDownload = driveFiles
		
		itemsToDelete = itemsToDownload
		
		downloadCompleation = compleation
		
		totalBytes = itemsToDownload.flatMap({ $0.size }).reduce(0, +)
		
		downloadStatus = DownloadStatus.started.rawValue as NSString
		
		savedFilesURL.removeAll()
		
		startDownloadNextFile()
		
	}
	
	func cancelDownload() {
		
		itemsToDownload.removeAll()
		
		if dataTask != nil {
			dataTask?.cancel()
		}
		
		if downloadTask != nil {
			downloadTask?.cancel()
		}
		
		FileManager.default.delete(urls: savedFilesURL)
		
		downloadStatus = DownloadStatus.canceled.rawValue as NSString
		
	}
	
	func startDownloadNextFile() {
		
		guard itemsToDownload.count > 0 else { return }
		
		let driveFile = itemsToDownload.removeFirst()
		
		let path = driveFile.path!.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? ""
		
		let uniqueName = FileService.sharedInstance.verifyFileName(name: driveFile.name!)
		newURL = FileService.sharedInstance.currentDirectory.url.appendingPathComponent(uniqueName)
		
		if let newURL = newURL {
			if newURL.isImage == false && newURL.isVideo == false {
				startDownloadNextFile()
				return
			}
			
		}
		
		dataTask = downloadSession.dataTask(with: URL(string: "https://cloud-api.yandex.net:443/v1/disk/resources/download?path=\(path)")!) { [weak self] (data, response, error) in
			
			guard let strongSelf = self else { return }
			
			if error == nil {
				
				do {
					
					let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
					
					if let href = (json as! NSDictionary).object(forKey: "href")  {
						
						strongSelf.downloadTask = strongSelf.downloadSession.downloadTask(with: URL(string: href as! String)!)
						strongSelf.downloadTask!.resume()
						
						
					}
					
				} catch let error as NSError {
					print("\(error)")
				}
				
				
				
			} else {
				
				if strongSelf.itemsToDownload.count == 0 {
					strongSelf.downloadStatus = DownloadStatus.finished.rawValue as NSString
					strongSelf.downloadCompleation!()
				} else {
					strongSelf.startDownloadNextFile()
				}
				
			}
			
		}
		
		dataTask!.resume()
		
	}
	
	//MARK: - Delete
	
	func deleteFileWithService() {
		
		for file in itemsToDelete {
			
			let path = file.path!.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? ""
			
			let request = try! URLRequest(url: URL(string: "https://cloud-api.yandex.net:443/v1/disk/resources?path=\(path)")!, method: .delete)
			
			downloadSession.dataTask(with: request, completionHandler: { (data, response, error) in
				
				print("\(error)")
				
			}).resume()
			
		}
		
	}
	
	//MARK: - Upload
	
	var uploadArray = [(File, String)]()
	
	let uploadGroup = DispatchGroup()
	
	var uploadCompleate: (() -> ())?
	var uploadProgress: ((Float, Float) -> ())?
	
	var uploadTotal: Int64?
	var uploadWritten: Int64?
	var uploadCurrentWritten: Int64?
	
	//  var uploadTask: ODURLSessionUploadTask?
	
	func upload(files: [File], folderIdentifier: String, progress: @escaping (Float, Float) -> (), compleate: @escaping () -> ()) {
		
		uploadProgress = progress
		uploadCompleate = compleate
		
		DispatchQueue.global().async {
			
			for file in files {
				
				switch file.fileType {
				case .image, .video:
					self.uploadArray.append((file, folderIdentifier))
					
				case .directory:
					self.prepareFolderToUpload(folder: file, itemId: folderIdentifier)
					
				default:
					break
				}
				
			}
			
			self.uploadGroup.wait()
			
			DispatchQueue.main.async {
				
				self.uploadTotal = self.uploadArray.flatMap({ $0.0.size }).reduce(0, +)
				self.uploadWritten = 0
				self.uploadCurrentWritten = 0
				
				self.uploadNext()
				
			}
			
		}
		
	}
	
	func uploadNext() {
		
		let fileInfo = uploadArray.removeFirst()
		
		inserFile(file: fileInfo.0, path: fileInfo.1)
		
	}
	
	func prepareFolderToUpload(folder: File, itemId: String) {
		
		uploadGroup.enter()
		
		createFolder(path: itemId, name: folder.name) { (item, error) in
			
			if error == nil {
				
				folder.fetchContent()
				
				for file in folder.contentOfDirectory {
					
					switch file.fileType {
					case .image, .video:
						self.uploadArray.append((file, item!.path! + "/"))
						
					case .directory:
						self.prepareFolderToUpload(folder: file, itemId: item!.path! + "/")
						
					default:
						break
						
					}
					
				}
				
			}
			
			self.uploadGroup.leave()
			
		}
		
	}
	
	func inserFile(file: File, path: String) {
		
		var uploadPath = "https://cloud-api.yandex.net:443/v1/disk/resources/upload?path=\(path)\(file.name)"
		uploadPath = uploadPath.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? ""
		let uploadURL = URL(string: uploadPath)
		
		dataTask = downloadSession.dataTask(with: uploadURL!) { [weak self] (data, response, error) in
			
			guard let strongSelf = self else { return }
			
			if error == nil {
				
				do {
					
					let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
					
					if let href = (json as! NSDictionary).object(forKey: "href")  {
						
						var request: URLRequest?
						
						do {
							request = try URLRequest(url: URL(string: href as! String)!, method: .put)
						} catch let error {
							print("\(error)")
						}
						
						strongSelf.uploadTask = strongSelf.downloadSession.uploadTask(with: request!, fromFile: file.url)
						strongSelf.uploadTask?.resume()
						
					} else {
						
						strongSelf.uploadCompleateHandler()
						
					}
					
				} catch let error as NSError {
					print("\(error)")
				}
				
			}
			
		}
		
		dataTask!.resume()
		
		
		
	}
	
	func uploadProgressHandler(totalBytesUploaded: Int64, totalBytesExpectedToUpload: Int64) {
		
		if let progressAction = uploadProgress {
			
			self.uploadCurrentWritten = Int64(totalBytesUploaded)
			
			progressAction((Float(self.uploadWritten!) + Float(self.uploadCurrentWritten!))/1024/1024, Float(self.uploadTotal!)/1024/1024)
			
		}
		
	}
	
	func uploadCompleateHandler() {
		
		if let compleatAction = uploadCompleate {
			
			self.uploadWritten! += self.uploadCurrentWritten!
			self.uploadCurrentWritten = 0
			
			if self.uploadArray.count == 0 {
				compleatAction()
			} else {
				self.uploadNext()
			}
			
		}
		
	}
	
	func cancelUpload() {
		
		if uploadTask != nil {
			uploadTask!.cancel()
		}
		
		uploadArray.removeAll()
		
	}
	
	//MARK: - Create Folder
	
	func createFolder(path: String, name: String, compleation: @escaping (_ folder: YDItem?, _ error: Error?) -> ()) {
		
		var newFolderPath = path.appending(name)
		
		newFolderPath = newFolderPath.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? ""
		
		let request = try! URLRequest(url: URL(string: "https://cloud-api.yandex.net:443/v1/disk/resources?path=\(newFolderPath)")!, method: .put)
		
		downloadSession.dataTask(with: request) { [weak self] (data, response, error) in
			
			guard let strongSelf = self else { return }
			
			if error == nil {
				
				do {
					
					let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
					
					if let href = (json as! NSDictionary).object(forKey: "href")  {
						
						strongSelf.dataTask = strongSelf.downloadSession.dataTask(with: URL(string: href as! String)!, completionHandler: { (data, response, error) in
							
							if error == nil {
								
								do {
									
									let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
									
									let item = YDItem(props: json as! NSDictionary)
									
									compleation(item, nil)
									
									
								} catch let error as NSError {
									print("\(error)")
								}
								
							} else {
								
								compleation(nil, error)
								
							}
							
						})
						
						strongSelf.dataTask!.resume()
						
					}
					
				} catch let error as NSError {
					print("\(error)")
				}
				
				
				
			}
			
			}.resume()
		
	}
	
	//MARK: - URLSession Delegate
	
	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
		
		if let newURL = newURL {
		
			writtenBytes = (writtenBytes.int64Value + currentWritten.int64Value) as NSNumber
			currentWritten = 0
			
			do {
				try FileManager.default.moveItem(at: location, to: newURL)
				let file = File(url: newURL)
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					_ = MFile.create(with: file.relativePath, context: localContext)
				})
				
				file.saveFileThumbnail()
				
			} catch {
				
				
			}
			
			savedFilesURL.append(newURL)
		}
		
		if itemsToDownload.count == 0 {
			downloadStatus = DownloadStatus.finished.rawValue as NSString
			downloadCompleation!()
		} else {
			startDownloadNextFile()
		}
		
	}
	
	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
		
		currentWritten = totalBytesWritten as NSNumber
		
		print("didWriteData")
		
	}
	
	func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
		
		uploadCompleateHandler()
		
	}
	
	func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
		
		uploadProgressHandler(totalBytesUploaded: totalBytesSent, totalBytesExpectedToUpload: totalBytesExpectedToSend)
		
	}
	
}

public class YDItem : NSObject {
	
	public var name : String?
	public var path : String?
	public var type : String?
	public var resource_id : String?
	public var size : Int?
	
	public var md5 : String?
	public var mime_type : String?
	public var media_type : String?
	public var preview : String?
	public var public_key : String?
	public var public_url : String?
	public var origin_path: String?
	public var custom_properties: NSDictionary?
	
	init(props: NSDictionary) {
		super.init()
		
		self.name = props["name"] as? String
		self.path = props["path"] as? String
		self.type = props["type"] as? String
		self.resource_id = props["resource_id"] as? String
		
		self.preview = props["preview"] as? String
		self.mime_type = props["mime_type"] as? String
		self.md5 = props["md5"] as? String
		self.public_key = props["public_key"] as? String
		self.public_url = props["public_url"] as? String
		self.origin_path = props["origin_path"] as? String
		self.media_type = props["media_type"] as? String
		self.custom_properties = props["custom_properties"] as? NSDictionary
		
		self.size = props["size"] as? Int
	}
}

