//
//  BoxService.swift
//  SmartLock
//
//  Created by Bogachev on 2/14/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import BoxContentSDK

class BoxServiceDownloadObserver: NSObject {
	
	var progressAction: ((_ progress: Float, _ writtenItems: Int, _ totalItems: Int) -> ())!
	var finishAction: (() -> ())!
	var cancelAction: (() -> ())!
	
	
	init(progress: @escaping (_ progress: Float, _ writtenItems: Int, _ totalItems: Int) -> (), finish: @escaping () -> (), cancel: @escaping () -> ()) {
		super.init()
		
		progressAction = progress
		finishAction = finish
		cancelAction = cancel
		
		BoxService.sharedInstance.addObserver(self, forKeyPath: "progress", options: .new, context: nil)
		BoxService.sharedInstance.addObserver(self, forKeyPath: "writtenItems", options: .new, context: nil)
		BoxService.sharedInstance.addObserver(self, forKeyPath: "downloadStatus", options: .new, context: nil)
	}
	
	deinit {
		BoxService.sharedInstance.removeObserver(self, forKeyPath: "progress", context: nil)
		BoxService.sharedInstance.removeObserver(self, forKeyPath: "writtenItems", context: nil)
		BoxService.sharedInstance.removeObserver(self, forKeyPath: "downloadStatus", context: nil)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		
		guard keyPath != nil else { return }
		
		switch keyPath! {
		case "writtenItems":
			
			if let action = progressAction {
				
				let total = BoxService.sharedInstance.totalItems
				let progress = BoxService.sharedInstance.progress.floatValue
				
				if let newValue = change?[.newKey] {
					action(progress, (newValue as! NSNumber).intValue, Int(total))
				}
				
			}
			
			
		case "progress":
			
			if let action = progressAction {
				
				let total = BoxService.sharedInstance.totalItems
				let written = BoxService.sharedInstance.writtenItems.int64Value
				
				if let newValue = change?[.newKey] {
					
					action((newValue as! NSNumber).floatValue , Int(written), Int(total))
				}
				
			}
			
		case "downloadStatus":
			
			if let newValue = change?[.newKey] {
				
				let status = newValue as! String
				
				switch status {
				case DownloadStatus.finished.rawValue:
					
					if let action = finishAction {
						
						action()
						
					}
					
				case DownloadStatus.canceled.rawValue:
					
					if let action = cancelAction {
						
						action()
						
					}
					
				default:
					break
				}
				
			}
			
		default: break
		}
		
	}
}

class BoxService: NSObject {
	
	private let clientID = "33e9ewpumokpywcj602yztm1p249c29a"
	private let clientSecret = "AZizw00lWWjfvEchfXg4a5h9C3EmAnrE"
	
	static let sharedInstance = BoxService()
	
	var itemsToDownload = [BOXItem]()
	var itemsToDelete = [BOXItem]()
	
	var totalItems: Int64 = 0
	dynamic var writtenItems: NSNumber = 0
	dynamic var progress: NSNumber = 0
	fileprivate var savedFilesURL = [URL]()
	
	private var request: BOXFileDownloadRequest?
	
	dynamic var downloadStatus: NSString = DownloadStatus.undefined.rawValue as NSString
	
	var downloadCompleation: (() -> ())?
	
	var client: BOXContentClient?
	
	override init() {
		super.init()
		
		BOXContentClient.setClientID(clientID, clientSecret: clientSecret)
		
	}
	
	func isAuthorized() -> Bool {
		return BOXContentClient.users().count > 0
	}
	
	func logOut() {
		BOXContentClient.logOutAll()
	}
	
	func fetchAuthorization(presenter: UIViewController, compleation: @escaping () -> () ) {
		
		BOXContentClient.default().authenticate { [weak self] (user, error) in
			
			guard let strongSelf = self else { return }
			
			if user != nil {
				
				strongSelf.client = BOXContentClient(forUser: user)
				
				compleation()
			}
			
		}
		
	}
	
	func logout() {
		
		BOXContentClient.default().logOut()
		
	}
	
	func fetchFolderContent(folderIdentifier: String, compleation: @escaping ([BOXItem]) -> () ) {
		
		guard client != nil  else {
			compleation([BOXItem]())
			return
		}
		
		client?.folderItemsRequest(withID: folderIdentifier).perform { (items, error) in
			
			if let items = items {
				compleation(items as! [BOXItem])
			} else {
				compleation([BOXItem]())
			}
			
		}
		
	}
	
	func download(items: [BOXItem], compleation: @escaping () -> ()) {
		
		itemsToDownload = items
		itemsToDelete = itemsToDownload
		
		writtenItems = 0
		progress = 0
		totalItems = Int64(itemsToDownload.count)
		
		downloadStatus = DownloadStatus.started.rawValue as NSString
		
		savedFilesURL.removeAll()
		
		downloadCompleation = compleation
		
		startDownloadNextFile()
		
	}
	
	func cancelDownload() {
		
		itemsToDownload.removeAll()
		
		if request != nil {
			request?.cancel()
		}
		
		FileManager.default.delete(paths: savedFilesURL.map( { $0.path} ))
		
		downloadStatus = DownloadStatus.canceled.rawValue as NSString
		
	}
	
	func startDownloadNextFile() {
		
		guard itemsToDownload.count > 0 else { return }
		
		let item = itemsToDownload.removeFirst() as! BOXFile
		
		let uniqueName = FileService.sharedInstance.verifyFileName(name: item.name)
		let newFileURL = FileService.sharedInstance.currentDirectory.url.appendingPathComponent(uniqueName)
		if !newFileURL.isImage && !newFileURL.isVideo {
			startDownloadNextFile()
			return
		}
		
		savedFilesURL.append(newFileURL)
		
		request = client?.fileDownloadRequest(withID: item.modelID, toLocalFilePath: newFileURL.path)
		
		request!.perform(progress: { (totalBytesTransferred, totalBytesExpectedToTransfer) in
			
			self.progress = (Float(totalBytesTransferred) / Float(totalBytesExpectedToTransfer)) as NSNumber
			
		}, completion: { [weak self] (error) in
			
			guard let strongSelf = self else { return }
			
			self?.writtenItems = ((self?.writtenItems.int64Value)! + 1) as NSNumber
			self?.progress = 0
			
			if strongSelf.itemsToDownload.count == 0 {
				
				if self?.downloadStatus != DownloadStatus.canceled.rawValue as NSString {
					
					self?.savedFilesURL.forEach({ (url) in
						
						let file = File(url: url)
						
						let context = NSManagedObjectContext.mr_default()
						context.mr_save(blockAndWait: { (localContext) in
							_ = MFile.create(with: file.relativePath, context: localContext)
						})
						
						file.saveFileThumbnail()
						
					})
					
					self?.downloadStatus = DownloadStatus.finished.rawValue as NSString
				}
				
			} else {
				strongSelf.startDownloadNextFile()
			}
			
		})
		
	}
	
	func deleteFileWithService() {
		
		for item in itemsToDelete {
			
			let request = client?.fileDeleteRequest(withID: item.modelID)
			request?.perform(completion: { (error) in
				print("\(error?.localizedDescription)")
			})
			
		}
		
	}
	
	//MARK: - Upload
	
	func createFolder(folderID: String, name: String, compleation: @escaping (_ folder: BOXFolder?, _ error: Error?) -> ()) {
		
		let request = client?.folderCreateRequest(withName: name, parentFolderID: folderID)
		
		request?.perform(completion: { (folder, error) in
			
			if error == nil {
				compleation(folder, nil)
			} else {
				compleation(nil, error)
			}
		})
	}
	
	var uploadArray = [(File, String)]()
	
	let uploadGroup = DispatchGroup()
	
	var uploadCompleate: (() -> ())?
	var uploadProgress: ((Float, Float) -> ())?
	
	var uploadTotal: Int64?
	var uploadWritten: Int64?
	var uploadCurrentWritten: Int64?
	
	var uploadRequest: BOXFileUploadRequest?
	
	func upload(files: [File], folderIdentifier: String, progress: @escaping (Float, Float) -> (), compleate: @escaping () -> ()) {
		
		uploadProgress = progress
		uploadCompleate = compleate
		
		DispatchQueue.global().async {
			
			for file in files {
				
				switch file.fileType {
				case .image, .video:
					self.uploadArray.append((file, folderIdentifier))
					
				case .directory:
					self.prepareFolderToUpload(folder: file, folderIdentifier: folderIdentifier)
					
				default:
					break
				}
				
			}
			
			self.uploadGroup.wait()
			
			DispatchQueue.main.async {
				
				self.uploadTotal = self.uploadArray.flatMap({ $0.0.size }).reduce(0, +)
				self.uploadWritten = 0
				self.uploadCurrentWritten = 0
				
				self.uploadNext()
				
			}
			
		}
		
	}
	
	func uploadNext() {
		
		let fileInfo = uploadArray.removeFirst()
		
		inserFile(url: fileInfo.0.url, folderIdentifier: fileInfo.1)
		
	}
	
	func prepareFolderToUpload(folder: File, folderIdentifier: String) {
		
		uploadGroup.enter()
		
		createFolder(folderID: folderIdentifier, name: folder.name) { (boxFolder, error) in
			
			if error == nil {
				
				folder.fetchContent()
				
				for file in folder.contentOfDirectory {
					
					switch file.fileType {
					case .image, .video:
						self.uploadArray.append((file, boxFolder!.modelID))
						
					case .directory:
						self.prepareFolderToUpload(folder: file, folderIdentifier: boxFolder!.modelID)
						
					default:
						break
						
					}
					
				}
				
			}
			
			self.uploadGroup.leave()
			
		}
		
	}
	
	func inserFile(url: URL, folderIdentifier: String) {
		
		uploadRequest = client?.fileUploadRequestToFolder(withID: folderIdentifier, fromLocalFilePath: url.path)
		uploadRequest?.perform(progress: { (written, total) in
			
			self.uploadCurrentWritten = written
			
			self.uploadProgress!((Float(self.uploadWritten!) + Float(self.uploadCurrentWritten!))/1024/1024, Float(self.uploadTotal!)/1024/1024)
			
		}, completion: { (file, error) in
			
			self.uploadWritten! += self.uploadCurrentWritten!
			self.uploadCurrentWritten = 0
			
			if self.uploadArray.count == 0 {
				self.uploadCompleate!()
			} else {
				self.uploadNext()
			}
			
		})
		
	}
	
	func cancelUpload() {
		
		if uploadRequest != nil {
			uploadRequest?.cancel()
		}
		
		uploadArray.removeAll()
		
	}
	
	
	
}
