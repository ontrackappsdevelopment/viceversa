//
//  BackupFile.swift
//  SmartLock
//
//  Created by Bogachev on 4/10/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import ObjectMapper

class BackupFile: Mappable {
	
	var id: String?
	var filePath: String?
	var dateCreated: String?
	var UUID: String?
	var dateDevice: String?
	var removed: Bool?
	
	required init?(map: Map){
		
	}
	
	func mapping(map: Map) {
		id <- map["_id"]
		filePath <- map["file_path"]
		dateCreated <- map["date_created"]
		dateDevice <- map["date_device"]
		UUID <- map["uuid"]
		removed <- map["removed"]
	}
}
