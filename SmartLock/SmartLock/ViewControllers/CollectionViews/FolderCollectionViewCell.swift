//
//  FolderCollectionViewCell.swift
//  CameraTest
//
//  Created by Bogachev on 1/29/17.
//  Copyright © 2017 SBogachev. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

enum FolderCollectionViewCellMode {
	case view
	case edit
}

class FolderCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var preview: UIImageView!
	@IBOutlet weak var selectedView: UIView!
	@IBOutlet weak var settingsButton: UIButton!
	@IBOutlet weak var selectedImageView: UIImageView!
	@IBOutlet weak var dot1ImageView: UIImageView!
	@IBOutlet weak var dot2ImageView: UIImageView!
	@IBOutlet weak var dot3ImageView: UIImageView!
	@IBOutlet weak var lockImageView: UIImageView!
	@IBOutlet weak var progressContainerView: UIView!
	@IBOutlet weak var draggedView: UIView!
	
	private var progressIndicatorView: NVActivityIndicatorView?
	
	var path: String?
	
	var secretAlbum: Bool = false {
		didSet {
			lockImageView.isHidden = !secretAlbum
		}
	}
	
	var isDragged: Bool = false {
		didSet {
			draggedView.isHidden = !isDragged
		}
	}
	
	var isOver: Bool = false {
		didSet {
			layer.borderWidth = isOver == true ? 2 : 0
			layer.borderColor = UIColor.white.withAlphaComponent(0.7).cgColor
		}
	}
	
	var isBlocked: Bool = false {
		didSet {
			if isBlocked == true {
				
				if progressIndicatorView == nil {
					progressIndicatorView = NVActivityIndicatorView(frame: progressContainerView.bounds, type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.white)
					progressContainerView.addSubview(progressIndicatorView!)
					progressIndicatorView?.startAnimating()
				}
				
			} else {
				
				if progressIndicatorView != nil {
					progressIndicatorView?.stopAnimating()
					progressIndicatorView?.removeFromSuperview()
					progressIndicatorView = nil
				}
				
			}
		}
	}
	
	var mode: FolderCollectionViewCellMode = .view {
		didSet {
			
			switch mode {
			case .view:
				nameLabel.isHidden = false
				settingsButton.isHidden = true
				
				dot1ImageView.isHidden = true
				dot2ImageView.isHidden = true
				dot3ImageView.isHidden = true
				
			case .edit:
				nameLabel.isHidden = true
				settingsButton.isHidden = false
				
				dot1ImageView.isHidden = false
				dot2ImageView.isHidden = false
				dot3ImageView.isHidden = false
				
			}
			
		}
	}
	
	var openSettingsAction: (() -> ())?
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	override var isSelected: Bool {
		didSet {
			selectedView.backgroundColor = isSelected ? UIColor(red: 92/255, green: 53/255, blue: 168/255, alpha: 0.3) : UIColor.clear
			selectedImageView.isHidden = !isSelected
		}
	}
	
	@IBAction func onSettings(_ sender: UIButton) {
		if let action = openSettingsAction {
			action()
		}
	}
}

