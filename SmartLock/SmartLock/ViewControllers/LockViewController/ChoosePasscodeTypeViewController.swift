//
//  ChoosePasscodeTypeViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import YandexMobileMetrica

enum ChoosePasscodeTypeViewControllerMode {
	case new
	case restore
	case restoreAlbum
}

class ChoosePasscodeTypeViewController: BaseViewController, UINavigationControllerDelegate, CAAnimationDelegate {
	
	var mode: ChoosePasscodeTypeViewControllerMode = .new
	
	@IBOutlet weak var gradientView: UIView!
	
	let gradient = CAGradientLayer()
	var gradientSet = [[CGColor]]()
	var currentGradient: Int = 0
	
	let black = UIColor.black.cgColor
	let blue = UIColor(red: 108/255, green: 0/255, blue: 255/255, alpha: 1.0).cgColor
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.delegate = self
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
//		gradientSet.append([black, black, black, black, black, black, black, black, blue])
//		
//		gradient.frame = self.view.bounds
//		gradient.colors = gradientSet[currentGradient]
//		gradient.locations = [0.01, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.99]
//		gradient.startPoint = CGPoint(x:0, y:1)
//		gradient.endPoint = CGPoint(x:1, y:1)
//		gradient.drawsAsynchronously = true
//		gradientView.layer.addSublayer(gradient)
		
//		animateGradient()		
	}
	
//	func animateGradient() {
//		if currentGradient < gradientSet.count - 1 {
//			currentGradient += 1
//		} else {
//			currentGradient = 0
//		}
//		
//		let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
//		gradientChangeAnimation.delegate = self
//		gradientChangeAnimation.duration = 5.0
//		gradientChangeAnimation.toValue = gradientSet[currentGradient]
//		gradientChangeAnimation.fillMode = kCAFillModeForwards
//		gradientChangeAnimation.isRemovedOnCompletion = false
//		gradient.add(gradientChangeAnimation, forKey: "colorChange")
//	}
	
//	func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
//		if flag {
//			gradient.colors = gradientSet[currentGradient]
//			animateGradient()
//		}
//	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		guard let leftTopBarButton = leftTopBarButton else { return }
		guard let rightTopBarButton = rightTopBarButton else { return }
		
		navigationBarView.topBarLabel.text = "Choose Passcode Type"
		navigationBarView.topBarLabel.font = UIFont.titleMedium
		navigationBarView.setupNoButton(button: leftTopBarButton)
		navigationBarView.setupNoButton(button: rightTopBarButton)
	}
	
	func saveLockAndClose(lockType: LockType, passcode: String?) {
		guard let navigationController = navigationController else { return }
		
		let controller = StoryboardScene.Main.instantiatePhotosScene()
		
		switch mode {
		case .new, .restore:
			
			AppDelegate.appIsLocked = false
			
			if LockService.sharedService.user == nil {
				
				let _ = LockService.sharedService.createUser(lockType: lockType, passcode: passcode)
				navigationController.setViewControllers([controller], animated: true)
				
			} else {
				
				YMMYandexMetrica.reportEvent("Apply_new_passcode", parameters: ["passcode": lockType.metricaName], onFailure: nil)
				
				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
				
					if let localUser = LockService.sharedService.user?.mr_(in: localContext) {
					
						localUser.mobLockType = Int16(lockType.hashValue)
						localUser.mobPasscode = passcode
						
						if lockType == .undefined {
							localUser.mobTouchID = false
							localUser.mobShakeToLock = false
							localUser.mobDecoyPasscodeType = Int16(DecoyType.decoyOff.rawValue)
						}
						
					}
					
				})
				
//				}
				
				//go to
				
				var viewControllers = navigationController.viewControllers
				viewControllers.removeLast()
				viewControllers.removeLast()
				
				navigationController.setViewControllers(viewControllers: viewControllers, animated: true, completion: {
					if let appDelegate = UIApplication.shared.delegate as? AppDelegate, let lockViewController = appDelegate.lockViewController {
						
//						if let success = lockViewController.success {
//							success("")
//						}
						
						appDelegate.removeLockAndPushPhotos()
					}
				})
				
//				} else {
					
//				}
				
			}
			
		default: break
		}
		
		if mode == .new && lockType == .undefined {
			DispatchQueue.main.async {
				_ = ErrorView.shows(inViewController: controller, text: "Your personal photos and videos will not be protected. We recommend to use passcode.")
			}
		}
		
	}
	
	func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		return FadeOutAnimatedTransitioning()
	}
	
	//MARK: - Action
	
	@IBAction func onNumPad4(_ sender: UIButton) {
		
		LockService.sharedService.setLockScreen(lockType: .numpad4, presenter: self, canCancel: true, canOpenDecoy: false, showAnimated: true) { (passcode) in
			self.saveLockAndClose(lockType: .numpad4, passcode: passcode)
		}
		
	}
	
	@IBAction func onNumPad6(_ sender: UIButton) {
		
		LockService.sharedService.setLockScreen(lockType: .numpad6, presenter: self, canCancel: true, canOpenDecoy: false, showAnimated: true) { passcode in
			self.saveLockAndClose(lockType: .numpad6, passcode: passcode)
		}
		
	}
	
	@IBAction func onDotLock(_ sender: UIButton) {
		
		LockService.sharedService.setLockScreen(lockType: .dotlock, presenter: self, canCancel: true, canOpenDecoy: false, showAnimated: true) { passcode in
			self.saveLockAndClose(lockType: .dotlock, passcode: passcode)
		}
		
	}
	
	@IBAction func onAlphanumeric(_ sender: UIButton) {
		
		LockService.sharedService.setLockScreen(lockType: .aplhanum, presenter: self, canCancel: true, canOpenDecoy: false, showAnimated: true) { passcode in
			self.saveLockAndClose(lockType: .aplhanum, passcode: passcode)
		}
		
	}
	
	@IBAction func onSkip(_ sender: UIButton) {
		self.saveLockAndClose(lockType: .undefined, passcode: nil)
		
//		switch mode {
//		case .new:
//			let _ = LockService.sharedService.createUserWithoutPasscode()
//			let controller = StoryboardScene.Main.instantiatePhotosScene()
//			navigationController.setViewControllers([controller], animated: true)
//			
//			DispatchQueue.main.async {
//				_ = ErrorView.shows(inViewController: controller, text: "Your personal photos and videos will not be protected. We recommend to use passcode.")
//			}
//			
//		case .restore:
//			
//			LockService.sharedService.saveUser {
//				LockService.sharedService.user?.mobLockType = Int16(LockType.undefined.hashValue)
//				LockService.sharedService.user?.mobPasscode = nil
//			}
//			
//			var viewControllers = navigationController.viewControllers
//			viewControllers.removeLast()
//			viewControllers.removeLast()
//			
//			navigationController.setViewControllers(viewControllers, animated: false)
//			
//			if let appDelegate = UIApplication.shared.delegate as? AppDelegate, let lockViewController = appDelegate.lockViewController {
//				lockViewController.remove()
//			}
//			
//		default:
//			break
//		}
		
	}

}
