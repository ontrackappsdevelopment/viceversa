//
//  CameraViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class CameraViewController: UIViewController {
	
	let cameraManager = CameraManager()
	
	var resumeAfterBecameActive: Bool = false
	
	@IBOutlet weak var cameraView: UIView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground(notification:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive(notification:)), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
		
		cameraManager.writeFilesToPhoneLibrary = false
		
		if cameraManager.currentCameraStatus() == .ready {
			addCameraToView()
		}
		
		if cameraManager.cameraOutputMode != CameraOutputMode.stillImage {
			cameraManager.cameraOutputMode = CameraOutputMode.stillImage
		}
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		navigationController?.navigationBar.isHidden = true
		cameraManager.resumeCaptureSession()
		
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		navigationController?.navigationBar.isHidden = false
		
		DispatchQueue.global().async {
			self.cameraManager.stopAndRemoveCaptureSession()
		}
	}
	
	func didEnterBackground(notification: NSNotification) {
		
		if navigationController == nil {
			self.cameraManager.stopCaptureSession()
		} else {
			
			DispatchQueue.global().async {
				self.cameraManager.stopCaptureSession()
			}
			
		}
		
	}
	
	func didBecomeActive(notification: NSNotification) {
		
		if resumeAfterBecameActive {
			
			DispatchQueue.global().async {
				self.cameraManager.resumeCaptureSession()
			}
			
		}
		
	}
		
	fileprivate func addCameraToView() {
		let _ = cameraManager.addPreviewLayerToView(cameraView)
		
		cameraManager.showErrorBlock = { [weak self] (erTitle: String, erMessage: String) -> Void in
			
			guard let strongSelf = self else { return }
			
			let alertController = UIAlertController(title: erTitle, message: erMessage, preferredStyle: .alert)
			alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alertAction) -> Void in  }))
			
			if UIDevice.current.isIpad() == true {
				
				if let popoverPresentationController = alertController.popoverPresentationController {
					popoverPresentationController.sourceView = strongSelf.view
					popoverPresentationController.sourceRect = CGRect(x: strongSelf.view.bounds.size.width / 2.0,
					                                                  y: strongSelf.view.bounds.size.height / 2.0,
					                                                  width: strongSelf.view.bounds.size.width / 2.0,
					                                                  height: strongSelf.view.bounds.size.height / 2.0)
				}
				
			}
			
			self?.present(alertController, animated: true, completion: nil)
		}
	}
	
	func takePhoto(compleation: @escaping (_ image: UIImage?, _ error: NSError?) -> () ) {
		
		cameraManager.capturePictureWithCompletion({ (image, error) -> Void in
			
			print("capturePictureWithCompletion")
			
			DispatchQueue.global().async {
				
				if error != nil {
					
				}
				else {
					
					if let image = image {
						
						if AppDelegate.appIsLocked == true {
							
							_ = FileService.sharedInstance.saveImageToAlbums(image: image)
							
						} else {
							
							_ = FileService.sharedInstance.saveImage(image: image)
							
						}
						
						DispatchQueue.main.async {
							compleation(image, nil)
						}
						
						
					}
				}
				
			}
			
		})
		
	}
	
	func startRecording(compleation: (() -> ())?) {
		cameraManager.startRecordingVideo {
			if compleation != nil {
				compleation!()
			}
		}
	}
	
	func stopRecording(compleation: @escaping (_ videoURL: URL?, _ error: NSError?) -> ()) {
		cameraManager.stopVideoRecording({ (videoURL, error) -> Void in
			
			var newURL = videoURL
			
			if let errorOccured = error {
				self.cameraManager.showErrorBlock("Error occurred", errorOccured.localizedDescription)
				compleation(nil, error)
				
			} else {
				if let videoURL = videoURL {
					
					if AppDelegate.appIsLocked == true {
						
						newURL = FileService.sharedInstance.saveVideoAlbums(URL: videoURL)
						
					} else {
						
						newURL = FileService.sharedInstance.saveVideo(URL: videoURL)
						
					}
					
					compleation(newURL, nil)
					
				}
			}
		})
		
		
	}
}
