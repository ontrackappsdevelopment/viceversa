//
//  PhotosViewController+ExportMenuViewDelegate.swift
//  SmartLock
//
//  Created by Bogachev on 8/17/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import MessageUI

extension PhotosViewController: ExportMenuViewDelegate {
	
	func prepareForExport(compleation: @escaping ((_ files: [File], _ folders:[File]) -> ())) {
		
		var filesToUpload = [File]()
		var foldersToUpload = [File]()
		
		if mode == .edit {
			PhotosViewController.filesToUpload = selectedFiles()
		}
		
		for file in selectedFiles() {
			
			if file.fileType == .directory {
				
				filesToUpload.append(contentsOf: file.getFiles())
				
				foldersToUpload.append(file)
				
				foldersToUpload.append(contentsOf: file.getFolders())
				
			} else {
				
				filesToUpload.append(file)
				
			}
			
		}
		
		if foldersToUpload.first(where: { $0.isSecret == true }) != nil {
			
			_ = AlertView.shows(inViewController: self, text: "Be aware of protected folder will be exported without any passcode", action: {
				
				compleation(filesToUpload, foldersToUpload)
				
			}, cancel: {
				
			})
			
		} else {
			
			compleation(filesToUpload, foldersToUpload)
			
		}
		
	}
	
	func openCameraRoll(exportView: ExportMenuView) {
		
		prepareForExport { (filesToUpload, foldersToUpload) in
			
			self.onFinishEdit()
			
			self.exportMenuSheets?.dismiss {  }
			
			self.progressView = ProgressView.showUploadProgressView(inView: self.view, labelType: .items, cancel: {
				
				DispatchQueue.main.async {
					
					self.progressView?.close()
					
				}
				
			})
			
			FileService.sharedInstance.exportToPhotoRoll(files: filesToUpload, progress: { (written, total) in
				
				DispatchQueue.main.async {
					
					self.progressView?.setProgressLabel(progress: Float(written) / Float(total), written: written, total: total)
					
				}
				
			}) {
				
				DispatchQueue.main.async {
					
					self.progressView?.close()
					
				}
				
			}
			
		}
  
	}
 
	func chooseCloud(exportView: ExportMenuView) {
		
		exportMenuSheets!.dismiss {
			
			self.hideAddMenuButton(hide: true)
			self.showCloudMenu()
			
		}
		
	}
	
	func wifiTransfer(exportView: ExportMenuView) {
		
		prepareForExport { (filesToUpload, foldersToUpload) in
			
			self.exportMenuSheets!.dismiss {
				self.hideAddMenuButton(hide: true)
				self.showWiFiTransferView()
			}
			
		}
		
	}
	
	func onAirDrop(exportView: ExportMenuView) {
		
		prepareForExport { (filesToUpload, foldersToUpload) in
			
			self.exportMenuSheets!.dismiss {
				
				let urls = filesToUpload.filter( { $0.fileType == .image || $0.fileType == .video } ).map( { $0.url } )
				
				let controller = UIActivityViewController(activityItems: urls, applicationActivities: nil)
				
				controller.presentBy(viewController: self)
				
			}
			
		}
		
	}
	
	func onMail(exportView: ExportMenuView) {
		
		prepareForExport { (filesToUpload, foldersToUpload) in
			
			self.exportMenuSheets!.dismiss {
				
				if( MFMailComposeViewController.canSendMail() ) {
					
					let mailComposer = MFMailComposeViewController()
					mailComposer.mailComposeDelegate = self
					
					
					mailComposer.setSubject("Smark Lock")
					mailComposer.setMessageBody("", isHTML: false)
					
					let urls = filesToUpload.filter( { $0.fileType == .image || $0.fileType == .video } ).map( { $0.url } )
					
					for url in urls {
						
						if let fileData = NSData(contentsOfFile: url.path) {
							
							let fileName = url.lastPathComponent
							let mimeType = url.mimeType()
							mailComposer.addAttachmentData(fileData as Data, mimeType: mimeType, fileName: fileName)
						}
						
					}
					
					mailComposer.presentBy(viewController: self)
					
				}
				
			}
			
		}
		
	}
	
}
