//
//  PhotosViewController+CloudMenuViewDelegate.swift
//  SmartLock
//
//  Created by Bogachev on 8/17/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import YandexMobileMetrica

extension PhotosViewController: CloudMenuViewDelegate {
	
	func prepareForCloud(compleation: @escaping (() -> ())) {
		
		if mode == .edit {
			
			prepareForExport { (filesToUpload, foldersToUpload) in
				
				self.cloudMenuSheets!.dismiss { }
				
				if PhotosViewController.filesToUpload.count > 0 {
					
					compleation()
				}
				
			}
			
		}
		
		if mode == .view {
			
			self.cloudMenuSheets!.dismiss { }
			
			compleation()
			
		}
		
	}
	
	func openGoogleDrive(view: CloudMenuView) {
		
		if mode == .edit {
			YMMYandexMetrica.reportEvent("Export", parameters: ["event": "GoogleDrive"], onFailure: nil)
		} else {
			YMMYandexMetrica.reportEvent("Import", parameters: ["event": "GoogleDrive"], onFailure: nil)
		}
		
		prepareForCloud {
			
			GoogleService.sharedInstance.fetchAuthorization(presenter: self) { [weak self] in
				
				guard let strongSelf = self else { return }
				
				let controller = GooglePickerViewController()
				controller.setParent(viewController: strongSelf)
				controller.type = strongSelf.mode == .edit ? .upload : .download
				controller.view.frame = strongSelf.view.frame
				controller.compleationAction = {
					DispatchQueue.main.async {
						strongSelf.onFinishEdit()
						strongSelf.scrollToEnd()
					}
				}
				
				controller.pushFrom(viewController: strongSelf)
				
			}
			
		}
		
	}
	
	func openDropBox(view: CloudMenuView) {
		
		if mode == .edit {
			YMMYandexMetrica.reportEvent("Export", parameters: ["event": "Dropbox"], onFailure: nil)
		} else {
			YMMYandexMetrica.reportEvent("Import", parameters: ["event": "Dropbox"], onFailure: nil)
		}
		
		prepareForCloud {
			
			DropBoxService.sharedInstance.fetchAuthorization(presenter: self) { [weak self] in
				
				guard let strongSelf = self else { return }
				
				let controller = DropBoxPickerViewController()
				controller.setParent(viewController: strongSelf)
				controller.type = strongSelf.mode == .edit ? .upload : .download
				controller.view.frame = strongSelf.view.frame
				controller.compleationAction = {
					self?.onFinishEdit()
					self?.scrollToEnd()
				}
				controller.pushFrom(viewController: strongSelf)
				
			}
			
		}
		
	}
	
	func openiCloud(view: CloudMenuView) {
		
		if mode == .edit {
			YMMYandexMetrica.reportEvent("Export", parameters: ["event": "iCloud"], onFailure: nil)
		} else {
			YMMYandexMetrica.reportEvent("Import", parameters: ["event": "iCloud"], onFailure: nil)
		}
		
		prepareForCloud {
			
			if self.mode == .edit {
				
				let filesToUpload = self.selectedFiles()
				if filesToUpload.count == 1 {
					let file = filesToUpload.first!
					let exportMenu = UIDocumentPickerViewController(url: file.url, in: .exportToService)
					exportMenu.delegate = self
					self.present(exportMenu, animated: true, completion: nil)
					
				} else {
					
					let alert = UIAlertController(title: "Upload Error", message: "Choose only one file to upload to iCloud Drive", preferredStyle: .alert)
					alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
						
					}))
					
					if UIDevice.current.isIpad() == true {
						
						if let popoverPresentationController = alert.popoverPresentationController {
							popoverPresentationController.sourceView = self.view
							popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0,
							                                                  y: self.view.bounds.size.height / 2.0,
							                                                  width: self.view.bounds.size.width / 2.0,
							                                                  height: self.view.bounds.size.height / 2.0)
						}
						
					}
					
					alert.presentBy(viewController: self)
				}
				
			} else {
				let importMenu = UIDocumentPickerViewController(documentTypes: ["public.data"], in: .import)
				importMenu.delegate = self
				self.present(importMenu, animated: true, completion: nil)
			}
			
		}
		
	}
	
	func openYandexDisk(view: CloudMenuView) {
		
		if mode == .edit {
			YMMYandexMetrica.reportEvent("Export", parameters: ["event": "Yandex.Disk"], onFailure: nil)
		} else {
			YMMYandexMetrica.reportEvent("Import", parameters: ["event": "Yandex.Disk"], onFailure: nil)
		}
		
		prepareForCloud {
			
			YandexService.sharedInstance.fetchAuthorization(presenter: self) {  [weak self] in
				
				guard let strongSelf = self else { return }
				
				let controller = YandexPickerViewController()
				controller.setParent(viewController: strongSelf)
				controller.type = strongSelf.mode == .edit ? .upload : .download
				controller.view.frame = strongSelf.view.frame
				controller.compleationAction = {
					self?.onFinishEdit()
					self?.scrollToEnd()
				}
				controller.pushFrom(viewController: strongSelf)
				
			}
			
		}
		
	}
	
	func openBox(view: CloudMenuView) {
		
		if mode == .edit {
			YMMYandexMetrica.reportEvent("Export", parameters: ["event": "Box"], onFailure: nil)
		} else {
			YMMYandexMetrica.reportEvent("Import", parameters: ["event": "Box"], onFailure: nil)
		}
		
		prepareForCloud {
			
			BoxService.sharedInstance.fetchAuthorization(presenter: self) { [weak self] in
				
				guard let strongSelf = self else { return }
				
				let controller = BoxPickerViewController()
				controller.setParent(viewController: strongSelf)
				controller.type = strongSelf.mode == .edit ? .upload : .download
				controller.view.frame = strongSelf.view.frame
				controller.compleationAction = {
					self?.onFinishEdit()
					self?.scrollToEnd()
				}
				controller.pushFrom(viewController: strongSelf)
				
			}
			
		}
		
	}
	
	func openOneDrive(view: CloudMenuView) {
		
		if mode == .edit {
			YMMYandexMetrica.reportEvent("Export", parameters: ["event": "OneDrive"], onFailure: nil)
		} else {
			YMMYandexMetrica.reportEvent("Import", parameters: ["event": "OneDrive"], onFailure: nil)
		}
		
		prepareForCloud {
			
			OneDriveService.sharedInstance.fetchAuthorization { [weak self] in
				
				guard let strongSelf = self else { return }
				
				let controller = OneDrivePickerViewController()
				controller.setParent(viewController: strongSelf)
				controller.type = strongSelf.mode == .edit ? .upload : .download
				controller.view.frame = strongSelf.view.frame
				controller.compleationAction = {
					self?.onFinishEdit()
					self?.scrollToEnd()
				}
				controller.pushFrom(viewController: strongSelf)
				
			}
			
		}
  
	}
	
}
