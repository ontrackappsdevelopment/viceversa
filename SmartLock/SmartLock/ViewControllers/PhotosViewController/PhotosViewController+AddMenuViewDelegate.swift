//
//  PhotosViewController+AddMenuViewDelegate.swift
//  SmartLock
//
//  Created by Bogachev on 8/17/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import AVFoundation

extension PhotosViewController: AddMenuViewDelegate {
	
	open func askUserForCameraPermission(_ completion: @escaping (Bool) -> Void) {
		AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (alowedAccess) -> Void in
			
			DispatchQueue.main.sync(execute: { () -> Void in
				completion(alowedAccess)
			})
			
		})
	}
	
	func openCamera(view: AddMenuView) {
		addMenuSheets!.dismiss {
			
		}
		
		askUserForCameraPermission { (alowed) in
			
			if alowed {
				
				self.needScrollToEnd = true
				self.performSegue(withIdentifier: "ShowCameraSegue", sender: nil)
				
			} else {
				
				_ = AlertView.shows(inViewController: self, text: "Camera access required for capturing photos!", actionTitle: "Allow Camera", action: {
					UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
				}, cancel: {})
				
			}
			
		}
		
	}
	
	func openCameraRoll(view: AddMenuView) {
		addMenuSheets!.dismiss {
			
		}
		
		checkPhotoPermission { (granted) in
			
			if granted {
				self.needScrollToEnd = true
				let controller = PhotoRollAlbumsPickerViewController()
				controller.view.frame = view.frame
				controller.pushFrom(viewController: self)
			}
		}
	}
	
	func createAlbum(view: AddMenuView) {
		addMenuSheets!.dismiss { }
		
//		self.needUpdate = true
		
		let controlller = AlbumViewController.createAlbum(delegate: CreateAlbumSettingsViewControllerDelegate())
		controlller.directoryWasCreated = {
			
			self.refreshDataAndReload()
			
//			FileService.sharedInstance.fetchCurrentDirectoryContent()
//			
//			self.reloadCollectionView()
			
			if self.showDragHint && self.wasShownDragHint == false {
				DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
					self.wasShownDragHint = true
					_ = HintView.shows(inViewController: self, image: #imageLiteral(resourceName: "ic_hint_draganddrop"), text: "Tap or select many to drag and drop items into the folders")
				}
			}
		}
		
		controlller.pushFrom(viewController: self, transitionFade: UIDevice.current.isIpad())
		
		showDragHint = true
	}
	
	func chooseCloud(view: AddMenuView) {
		addMenuSheets!.dismiss {
			self.hideAddMenuButton(hide: true)
			self.showCloudMenu()
		}
	}
	
	func wifiTransfer(view: AddMenuView) {
		addMenuSheets!.dismiss {
			self.hideAddMenuButton(hide: true)
			self.showWiFiTransferView()
		}
	}
	
}
