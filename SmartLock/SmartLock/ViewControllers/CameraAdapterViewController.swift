//
//  CameraAdapterViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import AVFoundation

enum CameraAdapterMode {
	case lock
	case photo
}

class CameraAdapterViewController: BaseViewController, UINavigationControllerDelegate {
	
	@IBOutlet weak var cameraButton: UIButton!
	@IBOutlet weak var lastImagePreview: UIImageView!
	@IBOutlet weak var gridView: CameraGridView!
	
	@IBOutlet weak var photoModeButton: UIButton!
	@IBOutlet weak var videoModeButton: UIButton!
	@IBOutlet weak var splashButton: UIButton!
	@IBOutlet weak var splashView: UIView!
	@IBOutlet weak var topToolBarView: UIView!
	
	@IBOutlet weak var videoTimerView: UIView!
	
	@IBOutlet weak var cameraToolsView: UIView!
	
	@IBOutlet weak var autoSplashButton: UIButton!
	@IBOutlet weak var onSplashButton: UIButton!
	@IBOutlet weak var offSplashButton: UIButton!
	@IBOutlet weak var splashLabel: UILabel!
	
	@IBOutlet weak var focusView: UIView!
	@IBOutlet weak var cameraContainerView: UIView?
	@IBOutlet weak var cancelButton: UIButton!
	@IBOutlet weak var photoButtonCenterPosition: NSLayoutConstraint!
	@IBOutlet weak var videoButtonCenterPosition: NSLayoutConstraint!
	@IBOutlet weak var cameraModeScrollView: UIScrollView!
	
	@IBOutlet weak var timerLabel: UILabel!
	
	var timer: Timer?
	var videoTimer: Timer?
	
	var cameraViewController: CameraViewController?
	
	var splashViewIsShowns = false
	var videoTimerViewIsShowns = false
	
	var videoStartTime: Date?
	
	var cameraManager: CameraManager {
		return cameraViewController!.cameraManager
	}
	
	var mode: CameraAdapterMode = .photo
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setNeedsStatusBarAppearanceUpdate()
		
		navigationController!.delegate = self
		cameraViewController?.resumeAfterBecameActive = true
		
		autoSplashButton.isSelected = true
		gridView.isHidden = true
		
		cameraManager.cameraFocus = .continuousAutoFocus
		cameraManager.moveFocusView = { (_ rect: CGRect) -> () in
			
			DispatchQueue.main.async {
				
				if self.timer != nil {
					
					self.timer?.invalidate()
					
				}
				
				self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.hideFocusView), userInfo: nil, repeats: false)
				
				UIView.animate(withDuration: 0.1, animations: {
					
					self.focusView.isHidden = false
					self.focusView.frame = rect
					
				})
				
			}
			
		}
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		focusView.isHidden = true
	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		
		cameraContainerView?.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY);
	}
	
	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		
		let targetRotation = coordinator.targetTransform
		let inverseRotation = targetRotation.inverted()
		
		coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
			
			self.cameraContainerView!.transform = (self.cameraContainerView?.transform.concatenating(inverseRotation))!
			
		}) { (UIViewControllerTransitionCoordinatorContext) -> Void in
			
		}
	}
	
	override func didEnterBackground(notification: NSNotification) {
		if mode == .lock { onCancel(cancelButton) }		
	}
	
	override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		return .portrait
	}
	
	override var prefersStatusBarHidden: Bool {
		return true
	}
	
	func hideFocusView() {
		
		DispatchQueue.main.async {
			self.focusView.isHidden = true
		}
		
	}
	
	// MARK: - Navigation
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if segue.identifier == "cameraSegue" {
			cameraViewController = segue.destination as? CameraViewController
		}
		
	}
	
	override func didUnlock(notification: Notification) {
		
		DispatchQueue.main.async {
			self.setNeedsStatusBarAppearanceUpdate()
		}
		
		
		DispatchQueue.global().async {
			self.cameraManager.resumeCaptureSession()
		}
	}
	
	// MARK: - @IBAction
	
	@IBAction func onChangeCameraDevice(_ sender: UIButton) {
		cameraManager.cameraDevice = cameraManager.cameraDevice == CameraDevice.front ? CameraDevice.back : CameraDevice.front
	}
	
	@IBAction func onShowGrid(_ sender: UIButton) {
		gridView.isHidden = !gridView.isHidden
	}
	
	@IBAction func onPhotoMode(_ sender: UIButton) {
		
		photoModeButton.isSelected = true
		videoModeButton.isSelected = false
		
		self.photoButtonCenterPosition.isActive = true
		self.videoButtonCenterPosition.isActive = false
		
		DispatchQueue.main.async {
			if self.cameraManager.cameraOutputMode != CameraOutputMode.stillImage {
				self.cameraManager.cameraOutputMode = CameraOutputMode.stillImage
			}
		}
		
	}
	
	@IBAction func onVideoMode(_ sender: UIButton) {
		
		AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeAudio, completionHandler: { (alowedAccess) -> Void in
			
			DispatchQueue.main.async {
				
				if alowedAccess {
					
					self.photoModeButton.isSelected = false
					self.videoModeButton.isSelected = true
					
					self.photoButtonCenterPosition.isActive = false
					self.videoButtonCenterPosition.isActive = true
					
					DispatchQueue.main.async {
						if self.cameraManager.cameraOutputMode != CameraOutputMode.videoWithMic {
							self.cameraManager.cameraOutputMode = CameraOutputMode.videoWithMic
						}
					}
					
				} else {
					
					self.photoModeButton.isSelected = true
					self.videoModeButton.isSelected = false
					
					let alert = UIAlertController(
						title: "IMPORTANT",
						message: "Microphone access required for recordig video!",
						preferredStyle: .alert
					)
					
					alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (alert) in
						
					}))
					alert.addAction(UIAlertAction(title: "Allow Microphone", style: .cancel, handler: { (alert) -> Void in
						UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
					}))
					
					self.present(alert, animated: true, completion: nil)
					
				}
				
			}
			
		})
		
	}
	
	@IBAction func onSplash(_ sender: UIButton) {
		showSplashView()
	}
	
	func showSplashView() {
		
		splashViewIsShowns = !splashViewIsShowns
		
		if splashViewIsShowns {
			splashView.frame = topToolBarView.bounds
			splashView.alpha = 0.0
			topToolBarView.addSubview(splashView)
			UIView.animate(withDuration: 0.3, animations: {
				self.splashView.alpha = 1.0
				self.cameraToolsView.alpha = 0.0
			})
		} else {
			
			UIView.animate(withDuration: 0.3, animations: {
				self.splashView.alpha = 0.0
				self.cameraToolsView.alpha = 1.0
			}, completion: { (success) in
				self.splashView.removeFromSuperview()
			})
			
		}
		
	}
	
	func showVideoTimerView(show: Bool) {
		
		videoTimerViewIsShowns = show
		
		if videoTimerViewIsShowns {
			videoTimerView.frame = topToolBarView.bounds
			videoTimerView.alpha = 0.0
			topToolBarView.addSubview(videoTimerView)
			UIView.animate(withDuration: 0.3, animations: {
				self.videoTimerView.alpha = 1.0
				self.cameraToolsView.alpha = 0.0
			})
		} else {
			
			UIView.animate(withDuration: 0.3, animations: {
				
				self.videoTimerView.alpha = 0.0
				self.cameraToolsView.alpha = 1.0
				
			}, completion: { (success) in
				self.videoTimerView.removeFromSuperview()
			})
			
		}
		
	}
	
	func unselectAllSplashButtons() {
		autoSplashButton.isSelected = false
		onSplashButton.isSelected = false
		offSplashButton.isSelected = false
	}
	
	@IBAction func onSplashAuto(_ sender: UIButton) {
		cameraManager.autoFlashMode()
		splashLabel?.text = "Auto"
		showSplashView()
		unselectAllSplashButtons()
		sender.isSelected = true
	}
	
	@IBAction func onSwitchOnSplash(_ sender: UIButton) {
		cameraManager.onFlashMode()
		splashLabel?.text = "On"
		showSplashView()
		unselectAllSplashButtons()
		sender.isSelected = true
	}
	
	@IBAction func onSwitchOffSplash(_ sender: UIButton) {
		cameraManager.offFlashMode()
		splashLabel?.text = "Off"
		showSplashView()
		unselectAllSplashButtons()
		sender.isSelected = true
	}
	
	func updateTimer() {
		self.timerLabel.text = Date().timeIntervalSince(self.videoStartTime!).toString()
	}
	
	@IBAction func recordButtonTapped(_ sender: UIButton) {
		
		sender.isUserInteractionEnabled = false
		print("sender.isUserInteractionEnabled: \(sender.isUserInteractionEnabled)")
		
		switch (cameraManager.cameraOutputMode) {
			
		case .stillImage: cameraViewController?.takePhoto(compleation: { (image, error) in
			self.lastImagePreview.image = image
			
			sender.isUserInteractionEnabled = true
			print("sender.isUserInteractionEnabled: \(sender.isUserInteractionEnabled)")
		})
			
		case .videoWithMic:
			
			if sender.isSelected == false {
				
				cameraViewController?.startRecording(compleation: {
					
					sender.isSelected = true
					DispatchQueue.main.async {
						
						self.videoStartTime = Date()
						
						self.videoTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
						
						self.showVideoTimerView(show: true)
						self.cancelButton.isHidden = true
						self.cameraModeScrollView.isHidden = true
						self.gridView.isHidden = true
						
						DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
							sender.isUserInteractionEnabled = true
							print("sender.isUserInteractionEnabled: \(sender.isUserInteractionEnabled)")
						})
					}
					
				})
				
			} else {
				
				cameraViewController?.stopRecording(compleation: { (url, error) in
					
					guard url != nil else {
						sender.isUserInteractionEnabled = true
						return
					}
					
					self.lastImagePreview.setImageAsVideoThumbnail(videoURL: url!)
					
					sender.isSelected = false
					DispatchQueue.main.async {
						self.videoTimer?.invalidate()
						self.showVideoTimerView(show: false)
						self.cancelButton.isHidden = false
						self.cameraModeScrollView.isHidden = false
						
						DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
							sender.isUserInteractionEnabled = true
							print("sender.isUserInteractionEnabled: \(sender.isUserInteractionEnabled)")
						})
						
					}
				})
				
			}
			
			sender.isSelected = !sender.isSelected
			
		default:
			break
		}
		
	}
	
	@IBAction func onCancel(_ sender: UIButton) {
				
		switch mode {
		case .photo:
			
			if let navigationController = navigationController {
				navigationController.popViewController(animated: true)
			}
			
		case .lock:
			let transition = CATransition()
			transition.duration = 0.5
			transition.type = kCATransitionFade
			self.view.window!.layer.add(transition, forKey: kCATransition)
			
			willMove(toParentViewController: nil)
			view.removeFromSuperview()
			removeFromParentViewController()
		}
		
	}
	
}
