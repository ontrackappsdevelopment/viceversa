//
//  VideoPreviewViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/19/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import AVFoundation

protocol VideoPreviewViewControllerDelegate {
	
	func startVideo(in view: VideoPreviewViewController, duration: CMTime)
	func playVideo(in view: VideoPreviewViewController)
	func stopVideo(in view: VideoPreviewViewController)
	func finishVideo(in view: VideoPreviewViewController)
	func seek(in view: VideoPreviewViewController, time: CMTime, duration: CMTime)
	
}

class VideoPreviewViewController: PreviewViewController {
	
	@IBOutlet weak var playerView: PlayerView!
	@IBOutlet weak var playButton: UIButton!
	@IBOutlet weak var previewImageView: UIImageView!
	
	var delegate: VideoPreviewViewControllerDelegate?
	
	var player:AVPlayer?
	
	var movieURL: URL?
	
	var finishPlayAction: (() -> ())?
	
	var autoPlay: Bool = false
	
	var videoGravity: String = AVLayerVideoGravityResizeAspect
	
	var timeObserver: Any?
	
	var previewImage: UIImage?
	
	var duration: CMTime {
		
		guard playerItem != nil else { return kCMTimeZero }
		return playerItem!.duration
		
	}
	
	private var playerItem: AVPlayerItem? = nil {
		didSet {
			player!.replaceCurrentItem(with: self.playerItem)
		}
	}
	
	override var prefersStatusBarHidden: Bool {
		if slideShow {
			return true
		} else {
			return super.prefersStatusBarHidden
		}
	}
		
	override func viewDidLoad() {
		super.viewDidLoad()
		
		previewImageView.image = previewImage
		
		player = AVPlayer()
		playerView.playerLayer.player = player
		playerView.playerLayer.videoGravity = videoGravity
		playerItem = AVPlayerItem(url: movieURL!)
		
		playButton.isHidden = slideShow
		
		NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(notification:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
		
		if slideShow {
			setNeedsStatusBarAppearanceUpdate()
		}
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		//    if slideShow {
		//      playerView.playerLayer.player = player
		//      playerView.playerLayer.videoGravity = videoGravity
		//      playerItem = AVPlayerItem(url: movieURL!)
		//    }
		
		timeObserver = player?.addPeriodicTimeObserver(forInterval: CMTime(value: 1, timescale: 10), queue: DispatchQueue.main, using: { (time) in
			
			self.delegate?.seek(in: self, time: time, duration: self.duration)
			
		})
		
		if playerItem != nil {
			self.delegate?.seek(in: self, time: kCMTimeZero, duration: player!.currentItem!.asset.duration)
		}
		
		//    if autoPlay {
		//
		//      player!.play()
		//
		//      playButton.isHidden = true
		//
		//    }
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if slideShow {
			
			play()
			
		}
		
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
		
		if self.timeObserver != nil {
			self.player?.removeTimeObserver(self.timeObserver!);
			self.timeObserver = nil;
		}
		
		stop()
		
		if slideShow {
			
			player?.replaceCurrentItem(with: nil)
			
		} else {
			
			playButton.isHidden = false
			player!.seek(to: kCMTimeZero)
			
		}
	}
	
	func playerDidFinishPlaying(notification: NSNotification) {
		
		if slideShow == true {
			
			let timeInterval = slideShowInterval.hashValue
			self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(timeInterval), target: self, selector: #selector(self.playNextSlide), userInfo: nil, repeats: true)
			
		} else {
			
			NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StopVideoPreview"), object: nil, userInfo: nil)
			player!.seek(to: kCMTimeZero)
			
			//playButton.isHidden = false
			
			self.delegate?.finishVideo(in: self)
			
		}
		
	}
	
	func  play() {
		
		if slideShow == false {
			NotificationCenter.default.post(name: PreviewViewController.playVideoNotification, object: nil, userInfo: nil)
		}
		
		player!.play()
		
		self.delegate?.playVideo(in: self)
		
	}
	
	func stop() {
		
		if slideShow == false {
			NotificationCenter.default.post(name: PreviewViewController.stopVideoNotification, object: nil, userInfo: nil)
		}
		
		player!.pause()
		
		self.delegate?.stopVideo(in: self)
	}
	
	func skipForward() {
		
		let time = CMTimeSubtract(self.player!.currentTime(),CMTimeMake(3, 1));
		
		if isOutOfTime(time: time, duration: duration) == true {
			player?.seek(to: time)
		}
		
	}
	
	func skipBackward() {
		
		let time = CMTimeSubtract(self.player!.currentTime(),CMTimeMake(-3, 1));
		
		if isOutOfTime(time: time, duration: duration) == true {
			player?.seek(to: time)
		}
		
	}
	
	func seek(to mseconds: Float) {
		
		let time = CMTime(seconds: Double(mseconds), preferredTimescale: 1)
		
		player?.seek(to: time)
		
	}
	
	func isOutOfTime(time:CMTime, duration: CMTime) -> Bool {
		
		if CMTimeGetSeconds(time) <= 0 {
			return false
		}
		
		if CMTimeGetSeconds(time) >= CMTimeGetSeconds(duration) {
			return false
		}
		
		return true
		
	}
	
	@IBAction func onTap(_ sender: UITapGestureRecognizer) {
		
		playButton.isHidden = true
		
		onTapAction()
	}
	
	@IBAction func onPlay(_ sender: UIButton) {
		delegate?.startVideo(in: self, duration: duration)
		
		playButton.isHidden = true
		
		play()
	}
	
}
