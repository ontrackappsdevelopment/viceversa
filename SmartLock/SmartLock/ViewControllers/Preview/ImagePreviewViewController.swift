//
//  ImagePreviewViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/11/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import SDWebImage

class ImagePreviewViewController: PreviewViewController, ImageScrollViewDelegate {
	
	var imageURL: String?
	
	var imageContentMode: UIViewContentMode = .scaleAspectFit
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var imageScrollView: ImageScrollView!
		
	override func viewDidLoad() {
		super.viewDidLoad()
		
		NotificationCenter.default.addObserver(self, selector: #selector(orientationChanged(notification:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
		
		imageView.isUserInteractionEnabled = slideShow
		
		if slideShow {
			setNeedsStatusBarAppearanceUpdate()
		}
	}
	
	deinit {
		print("Deinit ImagePreviewViewController")
	}
	
	func orientationChanged (notification: NSNotification) {
		
		imageScrollView.imageScrollViewDelegate = nil
		imageScrollView.refresh()
		imageScrollView.imageScrollViewDelegate = self
  
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if !slideShow {
			imageScrollView.imageScrollViewDelegate = self
		}
		
		imageView.contentMode = imageContentMode
		guard let image = UIImage(contentsOfFile: imageURL!) else { return }
		
		let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(onSwipeDown(_:)))
		swipeGesture.direction = .down
		
		imageView!.addGestureRecognizer(swipeGesture)
		
		if slideShow {
			imageView.image = image
			let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureRecognizer(_:)))
			tapGesture.numberOfTapsRequired = 1
			imageView!.addGestureRecognizer(tapGesture)
		} else {
			imageScrollView.layoutIfNeeded()
			imageScrollView.display(image: image)
			imageScrollView.isScrollEnabled = SlideViewController.isHidden
		}
		
		imageView.isHidden = !slideShow
		imageScrollView.isHidden = slideShow
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if slideShow {
			
			let timeInterval = slideShowInterval.hashValue == 0 ? 3 : slideShowInterval.hashValue
			
			self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(timeInterval), target: self, selector: #selector(self.playNextSlide), userInfo: nil, repeats: true)
			
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		imageScrollView.imageScrollViewDelegate = nil
		
		if slideShow {
			if self.timer != nil {
				self.timer?.invalidate()
			}
		}
	}
	
	override var prefersStatusBarHidden: Bool {
		
		if slideShow {
			return true
		} else {
			return super.prefersStatusBarHidden
		}
		
	}
	
	func onImageScrollViewTap() {
		onTapAction()
	
		imageScrollView.isScrollEnabled = SlideViewController.isHidden
	}
	
	func onImageScrollViewDoubleTap() {
		onDoubleTapAction()
		
		imageScrollView.isScrollEnabled = SlideViewController.isHidden
	}
	
	func onImageScrollViewZoomed() {
		onZoomAction()
		
		imageScrollView.isScrollEnabled = SlideViewController.isHidden
	}
	
	func tapGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
		onTapAction()
	}
}
