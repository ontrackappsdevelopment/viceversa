//
//  PurchasesViewController.swift
//  SmartLock
//
//  Created by Bogachev on 5/31/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyStoreKit
import YandexMobileMetrica
import FacebookCore

let sharedSecret = "6f1e9bc323be4f21a5918f528337c0e7"

enum RegisteredPurchase: String {
	case purchaseApp = "com.smlock.free.proversion"
	case allFeaturesForOneMonth = "com.smlock.free.subscribe_1_app"
	case allFeaturesForTwelveMonth = "com.smlock.free.subscribe_12_app"
	case allFeaturesBackupForOneMonth = "com.smlock.free.subscribe_1_app_backup"
	case allFeaturesBackupForTwelveMonth = "com.smlock.free.subscribe_12_app_backup"
	
	static var allValues: [RegisteredPurchase] {
		return [.purchaseApp, .allFeaturesForOneMonth, .allFeaturesForTwelveMonth, .allFeaturesBackupForOneMonth, .allFeaturesBackupForTwelveMonth]
	}
	
	var period: String {
		switch self {
		case .purchaseApp:
			return ""
		default:
			return "month"
		}
	}

	var order: Int {
		switch self {
		case .allFeaturesForTwelveMonth: return 1
		case .allFeaturesForOneMonth: return 2
		case .allFeaturesBackupForTwelveMonth: return 3
		case .allFeaturesBackupForOneMonth: return 4
		case .purchaseApp: return 5
		}
	}
	
	var price: Float {
//		switch self {
//		case .allFeaturesForTwelveMonth: return 0.99
//		case .allFeaturesForOneMonth: return 1.99
//		case .allFeaturesBackupForTwelveMonth: return 3.99
//		case .allFeaturesBackupForOneMonth: return 4.99
//		case .purchaseApp: return 14.99
//		}
		
		return 0
	}
}

class PurchaseCellViewModel: PurchaseCellDelegate {
	private var product: SKProduct!
	
	init(product: SKProduct) {
		self.product = product
	}
	
	var description: String? {
		
		guard let product = RegisteredPurchase(rawValue: product.productIdentifier) else { return "" }
		
		switch product {
		case .purchaseApp: return "All features included. \nLifetime purchase. No Secure backup."
		case .allFeaturesForOneMonth: return "All features included. \nSubscribe for 1 month. No secure backup"
		case .allFeaturesForTwelveMonth: return "All features included. \nSubscribe for 12 months. No secure backup"
		case .allFeaturesBackupForOneMonth: return "All features included. \nSubscribe for 1 month. 25GB of secure backup"
		case .allFeaturesBackupForTwelveMonth: return "All features included. \nSubscribe for 12 months. 25GB of secure backup"
		}
		
	}
	
	var price: String? {
		
		let price = product.price
		
		print("\(price)")
		
		if let product = RegisteredPurchase(rawValue: product.productIdentifier) {
			switch product {
			case .allFeaturesForTwelveMonth, .allFeaturesBackupForTwelveMonth:
				return String(format: "~$ %.02f", (price.floatValue/12).roundTo(places: 2))
			default: break
			}
		}
		
		return String(format: "~$ %.02f", price.floatValue)
	}
	
	var period: String? {
		return RegisteredPurchase(rawValue: product.productIdentifier)!.period
	}
	
	var disabled: Bool {
		
		if let product = RegisteredPurchase(rawValue: product.productIdentifier) {
			
			let appUsingType = UserService.sharedService.user.appUsingType
			let subscribedProduct = UserService.sharedService.user.subscribedProduct
			
			switch product {
			case .allFeaturesForOneMonth:
				return appUsingType == .purchased || appUsingType == .subscribed
			case .allFeaturesForTwelveMonth:
				return appUsingType == .purchased || (appUsingType == .subscribed && subscribedProduct != .allFeaturesForTwelveMonth)
			case .allFeaturesBackupForOneMonth:
				return subscribedProduct == .allFeaturesBackupForOneMonth || subscribedProduct == .allFeaturesBackupForTwelveMonth
			case .allFeaturesBackupForTwelveMonth:
				return subscribedProduct == .allFeaturesBackupForTwelveMonth
			case .purchaseApp:
				return appUsingType == .purchased
			}
			
		}
		
		return false
		
	}
	
}

class PurchaseHeaderCellViewModel: PurchaseHeaderCellDelegate {
	var message: String? {
		return UserService.sharedService.user.currentAppStatus
	}
}

class PurchasesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

	private var products = [SKProduct]()
	private var backBarButton: UIBarButtonItem?
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var keepFreeButtonHeight: NSLayoutConstraint!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		title = "Premium Version"
		
    }
	
	override func onBack(_ sender: UIButton) {
		pop(transitionFade: UIDevice.current.isIpad())
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		LoaderView.show(in: view)
		SwiftyStoreKit.retrieveProductsInfo(Set(RegisteredPurchase.allValues.map({ $0.rawValue }))) { result in
			LoaderView.close()
			
			self.products = Array(result.retrievedProducts).filter({ !$0.localizedDescription.contains("disabled") }).sorted(by: { RegisteredPurchase(rawValue: $0.productIdentifier)!.order < RegisteredPurchase(rawValue: $1.productIdentifier)!.order })
			
			DispatchQueue.main.async { [weak self] in
				guard let strongSelf = self else { return }
				strongSelf.tableView.reloadData()
			}
		}
		
		refreshUI()
		
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		navigationBarView.topBarLabel.text = "Premium Version"
	}
	
	private func refreshUI() {
		navigationController?.navigationItem.hidesBackButton = true
		if UserService.sharedService.user.appUsingType != .freeTrial && UserService.sharedService.user.appUsingType != .locked {
			keepFreeButtonHeight.constant = 0
			self.navigationItem.hidesBackButton = false
		} else {
			self.navigationItem.hidesBackButton = true
		}
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if indexPath.row == 0 {
			return 45
		} else {
			return 90
		}
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 0.01
	}
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 0.01
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return products.count + 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
	
		if indexPath.row == 0 {
			
			let cell = tableView.dequeueReusableCell(withIdentifier: "PurchaseHeaderCell", for: indexPath)
			return cell
			
		} else {
			
			let cell = tableView.dequeueReusableCell(withIdentifier: "PurchaseCell", for: indexPath)
			return cell
			
		}
		
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
	
		switch cell {
		case is PurchaseHeaderCell:
			(cell as! PurchaseHeaderCell).configurate(with: PurchaseHeaderCellViewModel())
		case is PurchaseCell:
			let product = products[indexPath.row - 1]
			(cell as! PurchaseCell).configurate(with: PurchaseCellViewModel(product: product))
		default:
			break
		}
		
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		guard indexPath.row > 0 else { return }
		
		let product = products[indexPath.row - 1]
		
		LoaderView.show(in: view)
		
		SwiftyStoreKit.purchaseProduct(product) { purchaseResult in
			LoaderView.close()
			
			switch purchaseResult {
			case .success(purchase: let purchase):
				if purchase.needsFinishTransaction {
					SwiftyStoreKit.finishTransaction(purchase.transaction)
				}
				
				AppEventsLogger.log(AppEvent.purchased(amount: product.price.doubleValue, currency: product.priceLocale.currencyCode))

				YMMYandexMetrica.reportEvent("Purchase", parameters: ["event": product.productIdentifier], onFailure: nil)
				
				AppsFlyerTracker.shared().trackEvent(product.productIdentifier, withValues: [
					AFEventParamContentId:product.productIdentifier,
					AFEventParamContentType: "",
					AFEventParamRevenue: product.price,
					AFEventParamCurrency:product.priceLocale.currencyCode ?? ""])
				
				LoaderView.show(in: self.view)
				
				User.verifyNewReceipt(completion: { (result) in
					DispatchQueue.main.async {
						
						switch result {
						case .success:
							
							if let user = MUser.mr_findFirst(), user.mobConfirmed == false, UserService.sharedService.user.canBackup == true {
								
								_ = ActionView.shows(inViewController: self, text: "\(UserService.sharedService.user.currentAppStatus)\nBackup will become available once recovery email is set!", actionTitle: "Set recovery email",action: {
									
									if var viewControllers = self.navigationController?.viewControllers {
										viewControllers[viewControllers.count - 1] = EmailViewController.register()
										self.navigationController?.setViewControllers(viewControllers, animated: true)
									}
									
									
								})
								
							} else {
								_ = ErrorView.shows(inViewController: self, text: UserService.sharedService.user.currentAppStatus)
							}
							
						default: break
						}
						
						self.tableView.reloadData()
						
						self.refreshUI()
						
						LoaderView.close()
					}
					
				})
			case .error(error: let error):
				
				DispatchQueue.main.async {
					_ = ErrorView.shows(inViewController: self, text: error.localizedDescription)
				}
				
			}
			
		}
		
		tableView.deselectRow(at: indexPath, animated: true)
	}
	
	func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
		let appleValidator = AppleReceiptValidator(service: User.verifyReceiptURLType)
		SwiftyStoreKit.verifyReceipt(using: appleValidator, password: sharedSecret, completion: completion)
	}
	
	@IBAction func onKeepFree(_ sender: UIButton) {
		popToViewController(viewController: PhotosViewController.currentPhotosViewController, transitionFade: UIDevice.current.isIpad())
	}
}

fileprivate extension PurchasesViewController {
	
	func alertWithTitle(_ title: String, message: String) -> UIAlertController {
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
		return alert
	}
	
	func showAlert(_ alert: UIAlertController) {
		guard self.presentedViewController != nil else {
			
			if UIDevice.current.isIpad() == true {
				
				if let popoverPresentationController = alert.popoverPresentationController {
					popoverPresentationController.sourceView = self.view
					popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0,
																	  y: self.view.bounds.size.height / 2.0,
																	  width: self.view.bounds.size.width / 2.0,
																	  height: self.view.bounds.size.height / 2.0)
				}
				
			}
			
			self.present(alert, animated: true, completion: nil)
			
			return
		}
	}
	
	func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {
		
		if let product = result.retrievedProducts.first {
			let priceString = product.localizedPrice!
			return alertWithTitle(product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
		} else if let invalidProductId = result.invalidProductIDs.first {
			return alertWithTitle("Could not retrieve product info", message: "Invalid product identifier: \(invalidProductId)")
		} else {
			let errorString = result.error?.localizedDescription ?? "Unknown error. Please contact support"
			return alertWithTitle("Could not retrieve product info", message: errorString)
		}
	}
	
	func alertForPurchaseResult(_ result: PurchaseResult) -> UIAlertController? {
		switch result {
		case .success(let purchase):
			print("Purchase Success: \(purchase.productId)")
			return alertWithTitle("Thank You", message: "Purchase completed")
		case .error(let error):
			print("Purchase Failed: \(error)")
			switch error.code {
			case .unknown: return alertWithTitle("Purchase failed", message: "Unknown error. Please contact support")
			case .clientInvalid: // client is not allowed to issue the request, etc.
				return alertWithTitle("Purchase failed", message: "Not allowed to make the payment")
			case .paymentCancelled: // user cancelled the request, etc.
				return nil
			case .paymentInvalid: // purchase identifier was invalid, etc.
				return alertWithTitle("Purchase failed", message: "The purchase identifier was invalid")
			case .paymentNotAllowed: // this device is not allowed to make the payment
				return alertWithTitle("Purchase failed", message: "The device is not allowed to make the payment")
			case .storeProductNotAvailable: // Product is not available in the current storefront
				return alertWithTitle("Purchase failed", message: "The product is not available in the current storefront")
			case .cloudServicePermissionDenied: // user has not allowed access to cloud service information
				return alertWithTitle("Purchase failed", message: "Access to cloud service information is not allowed")
			case .cloudServiceNetworkConnectionFailed: // the device could not connect to the nework
				return alertWithTitle("Purchase failed", message: "Could not connect to the network")
			case .cloudServiceRevoked: // user has revoked permission to use this cloud service
				return alertWithTitle("Purchase failed", message: "Cloud service was revoked")
			}
		}
	}
	
	func alertForRestorePurchases(_ results: RestoreResults) -> UIAlertController {
		
		if results.restoreFailedPurchases.count > 0 {
			print("Restore Failed: \(results.restoreFailedPurchases)")
			return alertWithTitle("Restore failed", message: "Unknown error. Please contact support")
		} else if results.restoredPurchases.count > 0 {
			print("Restore Success: \(results.restoredPurchases)")
			return alertWithTitle("Purchases Restored", message: "All purchases have been restored")
		} else {
			print("Nothing to Restore")
			return alertWithTitle("Nothing to restore", message: "No previous purchases were found")
		}
	}
	
	func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {
		
		switch result {
		case .success(let receipt):
			print("Verify receipt Success: \(receipt)")
			return alertWithTitle("Receipt verified", message: "Receipt verified remotely")
		case .error(let error):
			print("Verify receipt Failed: \(error)")
			switch error {
			case .noReceiptData:
				return alertWithTitle("Receipt verification", message: "No receipt data. Try again.")
			case .networkError(let error):
				return alertWithTitle("Receipt verification", message: "Network error while verifying receipt: \(error)")
			default:
				return alertWithTitle("Receipt verification", message: "Receipt verification failed: \(error)")
			}
		}
	}
	
	func alertForVerifySubscription(_ result: VerifySubscriptionResult) -> UIAlertController {
		
		switch result {
		case .purchased(let expiryDate):
			print("Product is valid until \(expiryDate)")
			return alertWithTitle("Product is purchased", message: "Product is valid until \(expiryDate)")
		case .expired(let expiryDate):
			print("Product is expired since \(expiryDate)")
			return alertWithTitle("Product expired", message: "Product is expired since \(expiryDate)")
		case .notPurchased:
			print("This product has never been purchased")
			return alertWithTitle("Not purchased", message: "This product has never been purchased")
		}
	}
	
	func alertForVerifyPurchase(_ result: VerifyPurchaseResult) -> UIAlertController {
		
		switch result {
		case .purchased:
			print("Product is purchased")
			return alertWithTitle("Product is purchased", message: "Product will not expire")
		case .notPurchased:
			print("This product has never been purchased")
			return alertWithTitle("Not purchased", message: "This product has never been purchased")
		}
	}
}
