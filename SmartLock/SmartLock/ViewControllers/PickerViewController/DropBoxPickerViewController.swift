//
//  DropBoxPickerViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import SwiftyDropbox

class DropBoxPickerCellDelegate: PickerCellProtocol {
	
	var request: DownloadRequestMemory<Files.FileMetadataSerializer, Files.ThumbnailErrorSerializer>?
	
	var file:Files.Metadata?
	
	init(file: Files.Metadata) {
		self.file = file
	}
	
	var contentType: PickerViewControllerCellType {
		
		switch file {
			
		case is Files.FileMetadata: return .file
			
		case is Files.FolderMetadata: return .folder
			
		default: return .unknown
		}
		
		
	}
	
	var resourceID: String {
		return ""
	}
	
	var thumbnailLink: String {
		return ""
	}
	
	var webViewLink: String {
		return ""
	}
	
	func cancelThumbnailRequest() {
		
		guard request != nil else { return }
		
		request?.cancel()
		request = nil
		
	}
	
	func updateThumbnail(compleation: ((UIImage?, URL?) -> ())?) {
		
		request = DropBoxService.sharedInstance.fetchThambnail(path: file!.pathDisplay ?? "", compleation: { (image) in
			
			compleation!(image, nil)
			
		})
		
	}
	
	var identifier: String {
		return file!.pathDisplay ?? ""
	}
	
	var name: String {
		return file!.name
	}
	
	func pickerController(frame: CGRect) -> UIViewController {
		
		let controller = DropBoxPickerViewController()
		controller.identifier = identifier
		controller.title = name
		controller.view.frame = frame
		return controller
		
	}
	
	var isVideo: Bool {
		return file!.name.mimeType().isVideoMimeType
	}
	
}

class DropBoxPickerViewController: PickerViewController {
	
	override var pickerViewController: PickerViewController {
		return DropBoxPickerViewController()
	}
	
	override var defaultIdentifier: String {
		return ""
	}
	
	override func cellDelegate(indexPath: IndexPath) -> PickerCellProtocol? {
		let item = items[indexPath.row]
		let delegate = DropBoxPickerCellDelegate(file: item as! Files.Metadata)
		return delegate
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		DropBoxService.sharedInstance.fetchFolderContent (pathFolder: identifier) { [weak self] (driveFiles) in
			
			guard let strongSelf = self else { return }
			
			strongSelf.items = driveFiles
			
			strongSelf.collectionView?.delegate = self
			strongSelf.collectionView?.dataSource = self
			strongSelf.collectionView?.reloadData()
			
			DispatchQueue.main.async {
				LoaderView.close()
			}
			
		}
		
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		super.didSetNavigationBarView(navigationBarView: navigationBarView)
		navigationBarView.topBarLabel.text = "Dropbox"
	}
	
	override func onUpload(item: UIBarButtonItem) {
		
		progressView = ProgressView.showUploadProgressView(inView: view, labelType: .size, cancel: {
			GoogleService.sharedInstance.cancelUpload()
		})
		
		DropBoxService.sharedInstance.upload(files: PhotosViewController.filesToUpload, folderIdentifier: identifier, progress: { (written, total) in
			
			DispatchQueue.main.async {
				self.progressView?.setProgressLabel(downloaded: Float(written), total: Float(total))
			}
			
		}) {
			
			guard LockService.sharedService.isDecoyEnabled == false else { return }
			
			DispatchQueue.main.async {
				
				guard AppDelegate.appIsLocked == false else {
					self.progressView?.compleateMode {
						
						self.progressView?.close()
						if let action = self.compleationAction { action() }
						let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
						
					}
					return
				}
				
				self.progressView?.close()
				if let action = self.compleationAction { action() }
				let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			}
			
		}
		
	}
	
	override func onDownload(item: UIBarButtonItem) {
		
		var filesToUpload = Array<Files.Metadata>()
		
		for indexPath in collectionView!.indexPathsForSelectedItems!.sorted(by: { $0.0.row > $0.1.row }) {
			filesToUpload.append(items[indexPath.row] as! Files.Metadata)
		}
		
		progressView = ProgressView.showProgressView(inView: view, labelType: .size, cancel: {
			DropBoxService.sharedInstance.cancelDownload()
		})
		
		downloadObserver = DropBoxServiceDownloadObserver(progress: { (progress, written, total) in
			
			DispatchQueue.main.async {
				self.progressView!.setProgressLabel(progress: progress, written: written, total: total)
			}
			
		}, finish: { [weak self] in
			
			guard let strongSelf = self else { return }
			
			guard LockService.sharedService.isDecoyEnabled == false else { return }
			
			guard AppDelegate.appIsLocked == false else {
				strongSelf.progressView?.compleateMode {
					strongSelf.finishDownload()
				}
				return
			}
			
			strongSelf.finishDownload()
			
		}) {  [weak self] in
			
			guard let strongSelf = self else { return }
			
			let _ = strongSelf.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			
		}
		
		DropBoxService.sharedInstance.download(files: filesToUpload) { }
		
	}
	
	func finishDownload() {
		guard LockService.sharedService.user?.mobDeleteAfterImort == true else {
			
			DispatchQueue.main.async {
				self.progressView?.close()
				let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
				if let action = self.compleationAction { action() }
			}
			
			return
		}
		
		let alert = UIAlertController(title: "DropBox", message: "Allow Smart Lock to delete this photos from DropBox", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
			let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			if let action = self.compleationAction { action() }
		}))
		
		
		alert.addAction(UIAlertAction(title: "Allow", style: .default, handler: { (alert) in
			
			DropBoxService.sharedInstance.deleteFileWithService()
			let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			if let action = self.compleationAction { action() }
			
		}))
		
		DispatchQueue.main.async {
			
			self.progressView?.close()
			
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
				
				if UIDevice.current.isIpad() == true {
					
					if let popoverPresentationController = alert.popoverPresentationController {
						popoverPresentationController.sourceView = self.view
						popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0,
						                                                  y: self.view.bounds.size.height / 2.0,
						                                                  width: self.view.bounds.size.width / 2.0,
						                                                  height: self.view.bounds.size.height / 2.0)
					}
					
				}
				
				alert.presentBy(viewController: self)
				
			})
			
		}
	}
	
	override func onNewFolderButton(sender: UIButton) {
		
		let _ = NewFolderView.showsView(inView: view, ok: { (folderName) in
			
			DropBoxService.sharedInstance.createFolder(pathFolder: self.identifier, name: folderName, compleation: { [weak self] (metadata, error) in
				guard let strongSelf = self else { return }
				
				if error == nil {
					strongSelf.items.append(metadata!)
				}
				
				DispatchQueue.main.async {
					strongSelf.collectionView?.reloadData()
				}
				
			})
			
		}) {
			
		}
		
	}
	
}
