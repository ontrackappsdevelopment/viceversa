//
//  YandexPickerViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import SwiftyDropbox

class YandexPickerCellDelegate: PickerCellProtocol {
	
	private var thambnailTask: URLSessionDataTask?
	
	var item:YDItem?
	
	init(item: YDItem) {
		self.item = item
	}
	
	var contentType: PickerViewControllerCellType {
		
		if item!.type == "file" {
			return .file
		} else if item!.type == "dir" {
			return .folder
		}
  
		return .unknown
	}
	
	var resourceID: String {
		return ""
	}
	
	var thumbnailLink: String {
		return ""
	}
	
	var webViewLink: String {
		return ""
	}
	
	func cancelThumbnailRequest() {
		guard thambnailTask != nil else {
			return
		}
		
		thambnailTask?.cancel()
		thambnailTask = nil
	}
	
	func updateThumbnail(compleation: ((UIImage?, URL?) -> ())?) {
		thambnailTask = YandexService.sharedInstance.thambnailTask(item: item!, compleation: { (image) in
			compleation!(image, nil)
		})
		
		thambnailTask?.resume()
	}
	
	var identifier: String  {
		return item!.path! + "/"
	}
	
	var name: String {
		return item!.name ?? ""
	}
	
	var isVideo: Bool {
		return item?.media_type == "video"
	}
	
}

class YandexPickerViewController: PickerViewController {
	
	override var pickerViewController: PickerViewController {
		return YandexPickerViewController()
	}
	
	override var defaultIdentifier: String {
		return "disk:/"
	}
	
	override func cellDelegate(indexPath: IndexPath) -> PickerCellProtocol? {
		let item = items[indexPath.row]
		let delegate = YandexPickerCellDelegate(item: item as! YDItem)
		return delegate
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		YandexService.sharedInstance.fetchYandexDriveContent(folderIdentifier: identifier) {[weak self] (items) in
			
			guard let strongSelf = self else { return }
			
			strongSelf.items = items
			
			DispatchQueue.main.async {
				strongSelf.collectionView?.delegate = self
				strongSelf.collectionView?.dataSource = self
				strongSelf.collectionView?.reloadData()
				
				LoaderView.close()
			}
			
		}
		
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		super.didSetNavigationBarView(navigationBarView: navigationBarView)
		navigationBarView.topBarLabel.text = "Yandex Disk"
	}
	
	override func onUpload(item: UIBarButtonItem) {
		
		progressView = ProgressView.showUploadProgressView(inView: view, labelType: .size, cancel: {
			
			YandexService.sharedInstance.cancelUpload()
			
		})
		
		YandexService.sharedInstance.upload(files: PhotosViewController.filesToUpload, folderIdentifier: identifier, progress: { (written, total) in
			
			DispatchQueue.main.async {
				self.progressView?.setProgressLabel(downloaded: Float(written), total: Float(total))
			}
			
		}) {
			
			guard LockService.sharedService.isDecoyEnabled == false else { return }
			
			DispatchQueue.main.async {
				
				guard AppDelegate.appIsLocked == false else {
					self.progressView?.compleateMode {
						
						self.progressView?.close()
						if let action = self.compleationAction { action() }
						let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
						
					}
					return
				}
				
				self.progressView?.close()
				if let action = self.compleationAction { action() }
				let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			}
			
			//      DispatchQueue.main.async {
			//        self.progressView?.close()
			//        let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			//        if let action = self.compleationAction { action() }
			//      }
			
		}
		
	}
	
	
	override func onDownload(item: UIBarButtonItem) {
		var filesToUpload = [YDItem]()
		
		for indexPath in collectionView!.indexPathsForSelectedItems!.sorted(by: { $0.0.row > $0.1.row }) {
			filesToUpload.append(items[indexPath.row] as! YDItem)
		}
		
		progressView = ProgressView.showProgressView(inView: view, labelType: .size, cancel: {
			YandexService.sharedInstance.cancelDownload()
		})
		
		downloadObserver = YandexServiceDownloadObserver(progress: { (written, total) in
			
			let writtenMBytes = Float(written) / 1024 / 1024
			let totalMBytes = Float(total) / 1024 / 1024
			
			DispatchQueue.main.async {
				self.progressView!.setProgressLabel(downloaded: writtenMBytes, total: totalMBytes)
			}
			
		}, finish: { [weak self] in
			
			guard let strongSelf = self else { return }
			
			guard LockService.sharedService.isDecoyEnabled == false else { return }
			
			guard AppDelegate.appIsLocked == false else {
				strongSelf.progressView?.compleateMode {
					strongSelf.finishDownload()
				}
				return
			}
			
			strongSelf.finishDownload()
			
		}) {  [weak self] in
			
			guard let strongSelf = self else { return }
			
			let _ = strongSelf.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			
		}
		
		YandexService.sharedInstance.download(driveFiles: filesToUpload) {}
	}
	
	func finishDownload() {
		guard LockService.sharedService.user?.mobDeleteAfterImort == true else {
			
			DispatchQueue.main.async {
				self.progressView?.close()
				let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
				if let action = self.compleationAction { action() }
			}
			
			return
		}
		
		let alert = UIAlertController(title: "Yandex Disk", message: "Allow Smart Lock to delete this photos from Yandex Disk", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
			let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			if let action = self.compleationAction { action() }
		}))
		
		alert.addAction(UIAlertAction(title: "Allow", style: .default, handler: { (alert) in
			
			YandexService.sharedInstance.deleteFileWithService()
			let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			if let action = self.compleationAction { action() }
			
		}))
		
		DispatchQueue.main.async {
			
			self.progressView?.close()
			
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
				
				
				if UIDevice.current.isIpad() == true {
					
					if let popoverPresentationController = alert.popoverPresentationController {
						popoverPresentationController.sourceView = self.view
						popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0,
						                                                  y: self.view.bounds.size.height / 2.0,
						                                                  width: self.view.bounds.size.width / 2.0,
						                                                  height: self.view.bounds.size.height / 2.0)
					}
					
				}
				
				alert.presentBy(viewController: self)
				
			})
			
		}
	}
	
	override func onNewFolderButton(sender: UIButton) {
		
		let _ = NewFolderView.showsView(inView: view, ok: { (folderName) in
			
			YandexService.sharedInstance.createFolder(path: self.identifier, name: folderName, compleation: { [weak self] (item, error) in
				
				guard let strongSelf = self else { return }
				
				if error == nil {
					strongSelf.items.append(item!)
				}
				
				DispatchQueue.main.async {
					strongSelf.collectionView?.reloadData()
				}
				
			})
			
		}) {
			
		}
		
	}
}
