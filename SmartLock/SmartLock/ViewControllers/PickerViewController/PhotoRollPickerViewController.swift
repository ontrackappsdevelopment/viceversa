//
//  PhotoRollPickerViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import Photos

class PhotoRollPickerViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	
	var	topView: UIView?
	var collectionView: UICollectionView?
	var collectionViewLayout: UICollectionViewLayout?
	
	var fetchResult: PHFetchResult<PHAsset>!
	var assetCollection: PHAssetCollection!
	
	fileprivate let imageManager = PHCachingImageManager()
	fileprivate var thumbnailSize: CGSize!
	fileprivate var previousPreheatRect = CGRect.zero
	fileprivate var errorMessage: String?
	fileprivate var itemsToDownload = [PHAsset]()
	
	static var columnCount: CGFloat {
		
		switch UIDevice.current.deviceType! {
		case .iPhone35, .iPhone40:
			return 3
		case .iPad:
			return 7
		default:
			return 4
		}
		
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		resetCachedAssets()
		
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		
		topView = UIView(frame:  CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 64))
		topBarView = topView
		view.addSubview(topBarView)
				
		var frame = view.frame
		
		frame.origin.y = 64
		frame.size.height -= 64
		
		collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
		collectionView?.backgroundColor = UIColor.black
		collectionView?.allowsMultipleSelection = true
		
		view.addSubview(collectionView!)
		
		self.collectionView!.register(UINib(nibName: "PhotoPickerCell", bundle: nil), forCellWithReuseIdentifier: PhotoPickerCellIdentifier)
		self.collectionView!.register(UINib(nibName: "FolderPickerCell", bundle: nil), forCellWithReuseIdentifier: FolderPickerCellIdentifier)
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if fetchResult == nil {
			let allPhotosOptions = PHFetchOptions()
			allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
			fetchResult = PHAsset.fetchAssets(in: assetCollection, options: allPhotosOptions)
		}
		
		self.collectionView?.delegate = self
		self.collectionView?.dataSource = self
		self.collectionView?.reloadData()
		
		let item = self.collectionView!.numberOfItems(inSection: 0) - 1
		
		if item > 0 {
			self.collectionView?.scrollToItem(at: IndexPath(row: item, section: 0), at: .bottom, animated: false)
		}
		
		let scale = UIScreen.main.scale
		let width = ((self.collectionView?.frame.size.width)! - (PhotoRollPickerViewController.columnCount - 1)) / PhotoRollPickerViewController.columnCount
		let cellSize = CGSize(width: width, height: width)
		thumbnailSize = CGSize(width: cellSize.width * scale, height: cellSize.height * scale)
		
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		navigationBarView.topBarLabel.text = assetCollection.localizedTitle
		if let rightTopBarButton = rightTopBarButton {
			navigationBarView.setupDownloadButton(button: rightTopBarButton)
			rightTopBarButton.isEnabled = false
		}
	}
	
	override func onRight(view: NavigationBarView, button: UIButton) {
		onUpload()
	}
	
	// MARK: UICollectionViewDataSource
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return fetchResult.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let asset = fetchResult.object(at: indexPath.item)
		
		let photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoPickerCellIdentifier, for: indexPath) as! PhotoPickerCell
		
		photoCell.videoIcon.isHidden = asset.mediaType != .video
		
		//    photoCell.isSelected = false
		
		let options = PHImageRequestOptions()
		options.resizeMode = .exact
		options.deliveryMode = .highQualityFormat
		options.isSynchronous = true // Set it to false for async callback
		
		imageManager.requestImage(for: asset, targetSize: thumbnailSize, contentMode: .aspectFill, options: options, resultHandler: { image, _ in
			
			photoCell.imageView.image = image
			
		})
		
		return photoCell
		
	}
	
	// MARK: UICollectionViewDelegate
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard let rightTopBarButton = rightTopBarButton else { return }
		rightTopBarButton.isEnabled = collectionView.indexPathsForSelectedItems!.count > 0
	}
	
	func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
		guard let rightTopBarButton = rightTopBarButton else { return }
		rightTopBarButton.isEnabled = collectionView.indexPathsForSelectedItems!.count > 0
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		
		let width = (collectionView.frame.size.width - (PhotoRollPickerViewController.columnCount - 1)) / PhotoRollPickerViewController.columnCount
		
		let height = width
		
		return CGSize(width: width, height: height)
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 1
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		return 1
	}
	
	// MARK: Asset Caching
	
	fileprivate func resetCachedAssets() {
		imageManager.stopCachingImagesForAllAssets()
		previousPreheatRect = .zero
	}
	
	@objc func upoladDidFinish() {
		
		NotificationCenter.default.removeObserver(self, name: applicationDidUnlocked, object: nil)
		
		if let navigationController = navigationController, let currentPhotosViewController = PhotosViewController.currentPhotosViewController {
			
			var viewControllers = navigationController.viewControllers
			
			if viewControllers.contains(currentPhotosViewController) {
				let _ = navigationController.popToViewController(PhotosViewController.currentPhotosViewController, animated: true)
			} else {
				viewControllers.removeLast()
				viewControllers.removeLast()
				navigationController.setViewControllers(viewControllers, animated: true)
			}
			
			if let counter = ReviewService.reviewLaunchCounter {
				ReviewService.reviewLaunchCounter! = counter - 1
			}
		}
		
		if LockService.sharedService.user?.mobDeleteAfterImort == true {
			
			PHPhotoLibrary.shared().performChanges({
				PHAssetChangeRequest.deleteAssets(self.itemsToDownload as NSArray)
			}, completionHandler: { (success, error) in })
			
		}
		
		if let errorMessage = errorMessage, errorMessage.isEmpty == false {
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
				_ = ErrorView.shows(inViewController: PhotosViewController.currentPhotosViewController, text: errorMessage)
			})
		}
	}
	
	func onUpload() {
		
		itemsToDownload = [PHAsset]()
		
		for indexPath in collectionView!.indexPathsForSelectedItems! {
			itemsToDownload.append(fetchResult.object(at: indexPath.row))
		}
		
		FileService.sharedInstance.importFromPhotoRoll(presenter: self, assets: itemsToDownload) { [weak self] (errorMessage) in
			
			guard let strongSelf = self else { return }
			
			guard AppDelegate.appIsLocked == false else {
			
				strongSelf.errorMessage = errorMessage
				
				NotificationCenter.default.addObserver(strongSelf, selector: #selector(PhotoRollPickerViewController.upoladDidFinish), name: applicationDidUnlocked, object: nil)
				
				return
			}
			
			DispatchQueue.main.async {
				
				strongSelf.upoladDidFinish()
				
			}
			
		}
		
	}
}

