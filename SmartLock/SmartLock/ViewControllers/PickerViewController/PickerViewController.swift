//
//  PickerViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import GoogleAPIClient

let PhotoPickerCellIdentifier = "photo"
let FolderPickerCellIdentifier = "folder"

enum TypePicker: String {
	case download = "Download"
	case upload = "Upload"
}

enum PickerViewControllerCellType {
	case unknown
	case file
	case folder
}

protocol PickerCellProtocol {
	var resourceID: String { get }
	var thumbnailLink: String { get }
	var webViewLink: String { get }
	var contentType: PickerViewControllerCellType { get }
	
	func updateThumbnail(compleation: ((UIImage?, URL?) -> ())?)
	func cancelThumbnailRequest()
	
	var identifier: String { get }
	
	var name: String { get }
	
	var isVideo: Bool { get }
}

protocol PickerViewControllerDelegate {
	
	
	
}

class PickerViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	
	var	topView: UIView?
	var collectionView: UICollectionView?
	var collectionViewLayout: UICollectionViewLayout?
	var compleationAction: (() -> ())?
	
	static var parentViewController: UIViewController!
	
	lazy var identifier: String = self.defaultIdentifier
	var defaultIdentifier: String {
		return ""
	}
	
	var items = [Any]()
	var downloadObserver: Any?
	var progressView: ProgressView?
	
	var type: TypePicker = .download
	
	var newFolderButtonContainer: UIView?
	var newFolderButton: UIButton?
	
	var pickerViewController: PickerViewController {
		return PickerViewController()
	}
	
	func setParent(viewController: UIViewController) {
		PickerViewController.parentViewController = viewController
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		topView = UIView(frame:  CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 64))
		topBarView = topView
		view.addSubview(topBarView)
		
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		
		var frame = view.frame
		
		if type == .upload {
			frame.size.height -= 35
		}
		
		frame.origin.y = 64
		frame.size.height -= 64
		
		collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
		collectionView?.backgroundColor = UIColor.black
		collectionView?.allowsMultipleSelection = true
		
		layout.scrollDirection = .vertical
		
		view.addSubview(collectionView!)
		
		if type == .upload {
			
			newFolderButton = UIButton(frame: CGRect(x: 0, y: frame.maxY, width: frame.size.width, height: 35))
			newFolderButton?.setTitle("New Folder", for: .normal)
			newFolderButton?.setTitleColor(UIColor.textColor, for: .normal)
			newFolderButton?.addTarget(self, action: #selector(onNewFolderButton(sender:)), for: .touchUpInside)
			view.addSubview(newFolderButton!)
			
		}
		
		self.collectionView!.register(UINib(nibName: "PhotoPickerCell", bundle: nil), forCellWithReuseIdentifier: PhotoPickerCellIdentifier)
		self.collectionView!.register(UINib(nibName: "FolderPickerCell", bundle: nil), forCellWithReuseIdentifier: FolderPickerCellIdentifier)
		
		LoaderView.show(in: view)
		
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		LoaderView.close()
	}
	
	override func willResignActive(notification: NSNotification) {
		super.willResignActive(notification: notification)
		
		if progressView != nil {
			progressView?.isHidden = true
		}
	}
	
	override func didEnterBackground(notification: NSNotification) {
		super.didEnterBackground(notification: notification)
		
		if progressView != nil {
			progressView?.isHidden = true
		}
	}
	
	override func didBecomeActive(notification: NSNotification) {
		super.didBecomeActive(notification: notification)
		
		if progressView != nil {
			progressView?.isHidden = false
		}
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		if type == .download {
			navigationBarView.setupRightButton(image: nil, title: type.rawValue, target: self, action: #selector(onDownload(item:)))
			navigationBarView.rightTopBarButton.isEnabled = false
		}		
		if type == .upload {
			navigationBarView.setupRightButton(image: nil, title: type.rawValue, target: self, action: #selector(onUpload(item:)))
		}
	}
	
	func onNewFolderButton(sender: UIButton) {
		
	}
	
	func cellDelegate(indexPath: IndexPath) -> PickerCellProtocol? {
		return nil
	}
	
	func addNewFolderButton() {
		
	}
	
	// MARK: UICollectionViewDataSource
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return items.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let cellDelegate = self.cellDelegate(indexPath: indexPath)!
		
		switch cellDelegate.contentType {
		case .file:
			let photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoPickerCellIdentifier, for: indexPath) as! PhotoPickerCell
			photoCell.videoIcon.isHidden = !cellDelegate.isVideo
			photoCell.configurate(viewModel: cellDelegate)
			//      photoCell.isSelected = false
			return photoCell
			
		case .folder:
			let folderCell = collectionView.dequeueReusableCell(withReuseIdentifier: FolderPickerCellIdentifier, for: indexPath) as! FolderPickerCell
			folderCell.nameLabel.text = cellDelegate.name
			return folderCell
			
		default: return UICollectionViewCell()
			
		}
		
	}
	
	// MARK: UICollectionViewDelegate
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let cellDelegate = self.cellDelegate(indexPath: indexPath)!
		if cellDelegate.contentType == .folder {
			collectionView.deselectItem(at: indexPath, animated: false)
			
			let controller = pickerViewController
			controller.identifier = cellDelegate.identifier
			controller.title = cellDelegate.name
			controller.type = self.type
			controller.view.frame = self.view.frame
			controller.compleationAction = compleationAction
			controller.pushFrom(viewController: self)
			
		}
		
		if type == .download {
			if let navigationBarView = navigationBarView {
				navigationBarView.rightTopBarButton.isEnabled = collectionView.indexPathsForSelectedItems!.count > 0
			}
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
		if type == .download {
			if let navigationBarView = navigationBarView {
				navigationBarView.rightTopBarButton.isEnabled = collectionView.indexPathsForSelectedItems!.count > 0
			}
		}
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		
		let width = (collectionView.frame.size.width - PhotosViewController.columnCount) / PhotosViewController.columnCount
		
		let height = width
		
		return CGSize(width: width, height: height)
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 1
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		return 1
	}
	
	//MARK: - Actions
	
	func onDownload(item: UIBarButtonItem) {
		
	}
	
	func onUpload(item: UIBarButtonItem) {
		
	}
	
}
