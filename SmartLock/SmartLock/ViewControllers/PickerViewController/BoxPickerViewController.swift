//
//  BoxPickerViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import BoxContentSDK

class BoxPickerCellDelegate: PickerCellProtocol {
	
	private var request: BOXFileThumbnailRequest?
	var item:BOXItem?
	
	private var client: BOXContentClient {
		return BoxService.sharedInstance.client!
	}
	
	init(item: BOXItem) {
		self.item = item
	}
	
	deinit {
		cancelThumbnailRequest()
	}
	
	var contentType: PickerViewControllerCellType {
		
		if item!.isFolder {
			return .folder
		} else if item!.isFile {
			return .file
		}
  
		return .unknown
  
	}
	
	var resourceID: String {
		return ""
	}
	
	var thumbnailLink: String {
		return ""
	}
	
	var webViewLink: String {
		return ""
	}
	
	func cancelThumbnailRequest() {
		guard request != nil else { return }
		request?.cancel()
		request = nil
	}
	
	func updateThumbnail(compleation: ((UIImage?, URL?) -> ())?) {
		
		if let image = BOXSampleThumbnailsHelper.sharedInstance().thumbnailForItem(withID: item!.modelID, userID: client.user.modelID) {
			compleation!(image, nil)
			return
		}
		
		if BOXSampleThumbnailsHelper.sharedInstance().shouldDownloadThumbnailForItem(withName: item?.name) == true {
			
			
			request = client.fileThumbnailRequest(withID: item!.modelID, size: .size64)
			request?.perform(progress: nil, completion: {[weak self] (image, error) in
				
				guard let strongSelf = self else { return }
				
				if let _ = image {
					
					BOXSampleThumbnailsHelper.sharedInstance().storeThumbnailForItem(withID: strongSelf.item!.modelID, userID: strongSelf.client.user.modelID, thumbnail: image!)
					compleation!(image, nil)
					
				}
				
			})
			
		}
		
	}
	
	var identifier: String  {
		return self.item!.modelID
	}
	
	var name: String {
		return item?.name ?? ""
	}
	
	var isVideo: Bool {
		return item!.name.mimeType().isVideoMimeType
	}
	
}

class BoxPickerViewController: PickerViewController {
	
	override var pickerViewController: PickerViewController {
		return BoxPickerViewController()
	}
	
	override var defaultIdentifier: String {
		return BOXAPIFolderIDRoot
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		BoxService.sharedInstance.fetchFolderContent(folderIdentifier: identifier) { [weak self] (items) in
			
			guard let strongSelf = self else { return }
			
			strongSelf.items = items
			
			DispatchQueue.main.async {
				strongSelf.collectionView?.delegate = self
				strongSelf.collectionView?.dataSource = self
				strongSelf.collectionView?.reloadData()
				
				LoaderView.close()
			}
			
		}
		
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		super.didSetNavigationBarView(navigationBarView: navigationBarView)
		navigationBarView.topBarLabel.text = "Box"
	}
	
	override func onUpload(item: UIBarButtonItem) {
		
		progressView = ProgressView.showUploadProgressView(inView: view, labelType: .size, cancel: {
			GoogleService.sharedInstance.cancelUpload()
		})
		
		BoxService.sharedInstance.upload(files: PhotosViewController.filesToUpload, folderIdentifier: identifier, progress: { (written, total) in
			
			DispatchQueue.main.async {
				self.progressView?.setProgressLabel(downloaded: Float(written), total: Float(total))
			}
			
		}) {
			
			guard LockService.sharedService.isDecoyEnabled == false else { return }
			
			DispatchQueue.main.async {
				
				guard AppDelegate.appIsLocked == false else {
					self.progressView?.compleateMode {
						
						self.progressView?.close()
						if let action = self.compleationAction { action() }
						let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
						
					}
					return
				}
				
				self.progressView?.close()
				if let action = self.compleationAction { action() }
				let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			}
			
			//      DispatchQueue.main.async {
			//        self.progressView?.close()
			//        let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			//        if let action = self.compleationAction { action() }
			//      }
			
		}
		
	}
	
	override func onDownload(item: UIBarButtonItem) {
		
		var filesToDownload = [BOXItem]()
		
		for indexPath in collectionView!.indexPathsForSelectedItems!.sorted(by: { $0.0.row > $0.1.row }) {
			filesToDownload.append(items[indexPath.row] as! BOXItem)
		}
		
		progressView = ProgressView.showProgressView(inView: view, labelType: .size, cancel: {
			BoxService.sharedInstance.cancelDownload()
		})
		
		downloadObserver = BoxServiceDownloadObserver(progress: { (progress, written, total) in
			
			DispatchQueue.main.async {
				self.progressView!.setProgressLabel(progress: progress, written: written, total: total)
			}
			
		}, finish: { [weak self] in
			
			guard let strongSelf = self else { return }
			
			guard LockService.sharedService.isDecoyEnabled == false else { return }
			
			guard AppDelegate.appIsLocked == false else {
				strongSelf.progressView?.compleateMode {
					strongSelf.finishDownload()
				}
				return
			}
			
			strongSelf.finishDownload()
			
			
		}) {  [weak self] in
			
			guard let strongSelf = self else { return }
			
			let _ = strongSelf.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			
		}
		
		BoxService.sharedInstance.download(items: filesToDownload) { }
	}
	
	func finishDownload() {
		guard LockService.sharedService.user?.mobDeleteAfterImort == true else {
			
			DispatchQueue.main.async {
				self.progressView?.close()
				let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
				if let action = self.compleationAction { action() }
			}
			
			return
		}
		
		let alert = UIAlertController(title: "Box", message: "Allow Smart Lock to delete this photos from Box", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
			let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			if let action = self.compleationAction { action() }
		}))
		
		alert.addAction(UIAlertAction(title: "Allow", style: .default, handler: { (alert) in
			
			BoxService.sharedInstance.deleteFileWithService()
			let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			if let action = self.compleationAction { action() }
			
		}))
		
		DispatchQueue.main.async {
			
			self.progressView?.close()
			
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
				
				if UIDevice.current.isIpad() == true {
					
					if let popoverPresentationController = alert.popoverPresentationController {
						popoverPresentationController.sourceView = self.view
						popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0,
						                                                  y: self.view.bounds.size.height / 2.0,
						                                                  width: self.view.bounds.size.width / 2.0,
						                                                  height: self.view.bounds.size.height / 2.0)
					}
					
				}
				
				alert.presentBy(viewController: self)
				
			})
			
		}
	}
	
	override func cellDelegate(indexPath: IndexPath) -> PickerCellProtocol? {
		let item = items[indexPath.row]
		let delegate = BoxPickerCellDelegate(item: item as! BOXItem)
		return delegate
	}
	
	override func onNewFolderButton(sender: UIButton) {
		
		let _ = NewFolderView.showsView(inView: view, ok: { (folderName) in
			
			BoxService.sharedInstance.createFolder(folderID: self.identifier, name: folderName, compleation: { [weak self] (folder, error) in
				
				guard let strongSelf = self else { return }
				
				if error == nil {
					strongSelf.items.append(folder!)
				}
				
				DispatchQueue.main.async {
					strongSelf.collectionView?.reloadData()
				}
				
			})
			
		}) {
			
		}
		
	}
	
}
