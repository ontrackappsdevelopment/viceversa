//
//  PhotoRollAlbumsPickerViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import Photos

class PhotoRollAlbumsPickerViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
 
	var	topView: UIView?
	var collectionView: UICollectionView?
	var collectionViewLayout: UICollectionViewLayout?
	
	var albums = Array<PHAssetCollection>()
	
	var columns: Int {
		return UIDevice.current.isIpad() ? 4 : 2
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		
		topView = UIView(frame:  CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 64))
		topBarView = topView
		view.addSubview(topBarView)
			
		var frame = view.frame
		
		frame.origin.y = 64
		frame.size.height -= 64
		
		collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
		collectionView?.backgroundColor = UIColor.black
		collectionView?.allowsMultipleSelection = true
		
		view.addSubview(collectionView!)
		
		self.collectionView!.register(UINib(nibName: "PhotoPickerCell", bundle: nil), forCellWithReuseIdentifier: PhotoPickerCellIdentifier)
		self.collectionView!.register(UINib(nibName: "FolderPickerCell", bundle: nil), forCellWithReuseIdentifier: FolderPickerCellIdentifier)
		
		LoaderView.show(in: view)
		
		DispatchQueue.global().async {
			
			let smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .albumRegular, options: nil)
			
			smartAlbums.enumerateObjects({ (collection, index, stop) in
				
				if collection.collectionInfo.0 > 0 {
					
					self.albums.append(collection)
					
				}
				
			})
			
			DispatchQueue.main.async {

				self.collectionView?.delegate = self
				self.collectionView?.dataSource = self
				self.collectionView?.reloadData()
				
				LoaderView.close()
			}
		}
		
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		navigationBarView.topBarLabel.text = "Photoroll"
	}
	
	// MARK: UICollectionViewDataSource
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return albums.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let collection = albums[indexPath.row]
		let folderCell = collectionView.dequeueReusableCell(withReuseIdentifier: FolderPickerCellIdentifier, for: indexPath) as! FolderPickerCell
		folderCell.nameLabel.text = collection.localizedTitle! + " (\(collection.collectionInfo.0))"
		folderCell.imageView.contentMode = .scaleAspectFill
		folderCell.imageView.image = collection.collectionInfo.1
		return folderCell
		
	}
	
	// MARK: UICollectionViewDelegate
	
	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		
		
		
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let collection = albums[indexPath.row]
		
		let controller = PhotoRollPickerViewController()
		controller.assetCollection = collection
		controller.view.frame = view.frame
		controller.pushFrom(viewController: self)
		
		collectionView.deselectItem(at: indexPath, animated: false)
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		
		let width = (collectionView.frame.size.width - CGFloat(20 * columns)) / CGFloat(columns)
		
		let height = width
		
		return CGSize(width: width, height: height)
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 20
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		return 20
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return UIEdgeInsets(top: 10 ,left: 10 , bottom: 10, right: 10)
	}
}

extension PHAssetCollection {
	var collectionInfo: (Int, UIImage?) {
		let fetchOptions = PHFetchOptions()
		let descriptor = NSSortDescriptor(key: "creationDate", ascending: true)
		fetchOptions.sortDescriptors = [descriptor]
		fetchOptions.predicate = NSPredicate(format: "mediaType == %d OR mediaType == %d", PHAssetMediaType.image.rawValue, PHAssetMediaType.video.rawValue)
		let result = PHAsset.fetchAssets(in: self, options: fetchOptions)
		
		if result.count == 0 {
			
			return (0, nil)
			
		}
		
		let lastAsset = result.lastObject
		
		let options = PHImageRequestOptions()
		options.resizeMode = .none
		options.isSynchronous = true
		var previewImage: UIImage?
		
		let width = (UIScreen.main.bounds.size.width - 40) / 2
		
		let height = width
		
		let size = CGSize(width: width, height: height)
		
		PHImageManager.default().requestImage(for: lastAsset!, targetSize: size, contentMode: .aspectFill, options: options) { (image, info) in
			previewImage = image
		}
		
		return (result.count, previewImage)
	}
}
