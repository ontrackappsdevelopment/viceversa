//
//  GooglePickerViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import GoogleAPIClient

let folderMimeType = "application/vnd.google-apps.folder"

class GoogleDrivePickerCellDelegate: PickerCellProtocol {
	
	var driveFile:GTLDriveFile?
	
	init(driveFile: GTLDriveFile) {
		self.driveFile = driveFile
	}
	
	var contentType: PickerViewControllerCellType {
		if driveFile!.mimeType == folderMimeType {
			return .folder
		} else {
			return .file
		}
	}
	
	var resourceID: String {
		return driveFile!.identifier ?? ""
	}
	
	var thumbnailLink: String {
		return driveFile!.thumbnailLink ?? ""
	}
	
	var webViewLink: String {
		return driveFile!.webViewLink ?? ""
	}
	
	func cancelThumbnailRequest() {
		
	}
	
	func updateThumbnail(compleation: ((UIImage?, URL?) -> ())?) {
		compleation!(nil, URL(string: thumbnailLink))
	}
	
	var identifier: String  {
		return driveFile!.identifier
	}
	
	var name: String {
		return driveFile!.name
	}
	
	var isVideo: Bool {
		return driveFile!.mimeType.isVideoMimeType
	}
	
}

class GooglePickerViewController: PickerViewController {
	
	override var pickerViewController: PickerViewController {
		return GooglePickerViewController()
	}
	
	override var defaultIdentifier: String {
		return "root"
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		GoogleService.sharedInstance.fetchGoogleDriveContent(folderIdentifier: identifier) {[weak self] (driveFiles) in
			
			guard let strongSelf = self else { return }
			
			if strongSelf.type == .download {
				
				strongSelf.items = driveFiles
				
			} else {
				
				strongSelf.items = driveFiles.filter( { $0.mimeType == folderMimeType } )
				
			}
			
			strongSelf.collectionView?.delegate = self
			strongSelf.collectionView?.dataSource = self
			strongSelf.collectionView?.reloadData()
			
			DispatchQueue.main.async {
				LoaderView.close()
			}
			
		}
		
	}
	
	override func onUpload(item: UIBarButtonItem) {
		
		progressView = ProgressView.showUploadProgressView(inView: view, labelType: .size, cancel: {
			GoogleService.sharedInstance.cancelUpload()
		})
		
		GoogleService.sharedInstance.upload(files: PhotosViewController.filesToUpload, folderIdentifier: identifier, progress: { (written, total) in
			
			DispatchQueue.main.async {
				self.progressView?.setProgressLabel(downloaded: Float(written), total: Float(total))
			}
			
		}) {
			
			DispatchQueue.main.async {
				
				guard LockService.sharedService.isDecoyEnabled == false else { return }
				
				guard AppDelegate.appIsLocked == false else {
					self.progressView?.compleateMode {
						
						self.progressView?.close()
						if let action = self.compleationAction { action() }
						let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
						
					}
					return
				}
				
				self.progressView?.close()
				if let action = self.compleationAction { action() }
				let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			}
			
		}
		
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		super.didSetNavigationBarView(navigationBarView: navigationBarView)
		navigationBarView.topBarLabel.text = "Google Drive"
	}
	
	override func onDownload(item: UIBarButtonItem) {
		
		var driveFilesToDownload = [GTLDriveFile]()
		
		for indexPath in collectionView!.indexPathsForSelectedItems!.sorted(by: { $0.0.row < $0.1.row }) {
			driveFilesToDownload.append(items[indexPath.row] as! GTLDriveFile)
		}
		
		progressView = ProgressView.showProgressView(inView: view, labelType: .size, cancel: {
			GoogleService.sharedInstance.cancelDownload()
		})
		
		downloadObserver = GoogleServiceDownloadObserver(progress: { (written, total) in
			
			let writtenMBytes = Float(written) / 1024 / 1024
			let totalMBytes = Float(total) / 1024 / 1024
			
			DispatchQueue.main.async {
				self.progressView!.setProgressLabel(downloaded: writtenMBytes, total: totalMBytes)
			}
			
		}, finish: { [weak self] in
			
			guard LockService.sharedService.isDecoyEnabled == false else { return }
			
			guard let strongSelf = self else { return }
			
			guard AppDelegate.appIsLocked == false else {
				strongSelf.progressView?.compleateMode {
					strongSelf.finishDownload()
				}
				return
			}
			
			strongSelf.finishDownload()
			
		}) {  [weak self] in
			
			guard let strongSelf = self else { return }
			
			let _ = strongSelf.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			
		}
		
		GoogleService.sharedInstance.download(driveFiles: driveFilesToDownload) { }
	}
	
	func finishDownload() {
		
		guard LockService.sharedService.user?.mobDeleteAfterImort == true else {
			
			DispatchQueue.main.async {
				self.progressView?.close()
				if let action = self.compleationAction { action() }
				let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			}
			
			return
		}
		
		let alert = UIAlertController(title: "Google Drive", message: "Allow Smart Lock to delete this photos from Google Drive", preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
			if let action = self.compleationAction { action() }
			let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
		}))
		
		alert.addAction(UIAlertAction(title: "Allow", style: .default, handler: { (alert) in
			
			GoogleService.sharedInstance.deleteFileWithService()
			if let action = self.compleationAction { action() }
			let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			
		}))
		
		progressView?.close()
		
		//    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
			
			if UIDevice.current.isIpad() == true {
				
				if let popoverPresentationController = alert.popoverPresentationController {
					popoverPresentationController.sourceView = self.view
					popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0,
					                                                  y: self.view.bounds.size.height / 2.0,
					                                                  width: self.view.bounds.size.width / 2.0,
					                                                  height: self.view.bounds.size.height / 2.0)
				}
				
			}
			
			alert.presentBy(viewController: self)
			
		})
		
	}
	
	override func cellDelegate(indexPath: IndexPath) -> PickerCellProtocol? {
		let file = items[indexPath.row]
		let googleDrivePickerCellDelegate = GoogleDrivePickerCellDelegate(driveFile: file as! GTLDriveFile)
		return googleDrivePickerCellDelegate
	}
	
	override func onNewFolderButton(sender: UIButton) {
		
		let _ = NewFolderView.showsView(inView: view, ok: { (folderName) in
			
			GoogleService.sharedInstance.createFolder(identifier: self.identifier, name: folderName, compleation: {[weak self] (folder, error) in
				
				guard let strongSelf = self else { return }
				
				if error == nil {
					strongSelf.items.append(folder!)
				}
				
				DispatchQueue.main.async {
					strongSelf.collectionView?.reloadData()
				}
				
			})
			
		}) {
			
		}
		
	}
	
}
