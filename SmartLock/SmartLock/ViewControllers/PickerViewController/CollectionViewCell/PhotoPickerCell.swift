//
//  PhotoPickerCell.swift
//  SmartLock
//
//  Created by Bogachev on 2/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoPickerCell: UICollectionViewCell {
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var selectedView: UIView!
	@IBOutlet weak var selectedImageView: UIImageView!
	@IBOutlet weak var videoIcon: UIImageView!
	
	override func prepareForReuse() {
		super.prepareForReuse()
		viewModel?.cancelThumbnailRequest()
	}
	
	var viewModel: PickerCellProtocol?
	
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	func configurate(viewModel: PickerCellProtocol) {
		self.viewModel = viewModel
		
		viewModel.updateThumbnail {[weak self] (image, url) in
			
			guard let strongSelf = self else { return }
			
			if let image = image {
				
				DispatchQueue.main.async {
					
					strongSelf.imageView.image = image
					
				}
				
			}
			
			if let url = url {
				
				strongSelf.imageView.sd_cancelCurrentImageLoad()
				strongSelf.imageView.sd_setImage(with: url) //URL(string: viewModel.thumbnailLink)
				
			}
			
		}
		
	}
	
	override var isSelected: Bool {
		didSet {
			
			selectedView.backgroundColor = isSelected ? UIColor(red: 92/255, green: 53/255, blue: 168/255, alpha: 0.3) : UIColor.clear
			selectedImageView.isHidden = !isSelected
			if videoIcon.isHidden == false {
				videoIcon.alpha = isSelected ? 0 : 1
			}
			
		}
	}
	
}
