//
//  FolderPickerCell.swift
//  SmartLock
//
//  Created by Bogachev on 2/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class FolderPickerCell: UICollectionViewCell {
  
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var imageView: UIImageView!
  
  @IBOutlet weak var selectedView: UIView!
  @IBOutlet weak var selectedImageView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  
  override var isSelected: Bool {
    didSet {
      
      selectedView.backgroundColor = isSelected ? UIColor(red: 92/255, green: 53/255, blue: 168/255, alpha: 0.3) : UIColor.clear
      selectedImageView.isHidden = !isSelected
      
    }
  }
  
}
