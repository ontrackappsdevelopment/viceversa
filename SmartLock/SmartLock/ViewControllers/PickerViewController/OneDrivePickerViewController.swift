//
//  OneDrivePickerViewController.swift
//  SmartLock
//
//  Created by Bogachev on 2/8/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import OneDriveSDK

class OneDrivePickerCellDelegate: PickerCellProtocol {
	
	private var downloadTask: ODURLSessionDownloadTask?
	
	var item:ODItem?
	
	private var client: ODClient {
		return OneDriveService.sharedInstance.client!
	}
	
	init(item: ODItem) {
		self.item = item
	}
	
	deinit {
		cancelThumbnailRequest()
	}
	
	var contentType: PickerViewControllerCellType {
		
		if item!.folder != nil {
			return .folder
		} else if item!.file != nil {
			return .file
		}
		
		return .unknown
		
	}
	
	var resourceID: String {
		return ""
	}
	
	var thumbnailLink: String {
		return ""
	}
	
	var webViewLink: String {
		return ""
	}
	
	func cancelThumbnailRequest() {
		guard downloadTask != nil else { return }
		downloadTask?.cancel()
		downloadTask = nil
	}
	
	func updateThumbnail(compleation: ((UIImage?, URL?) -> ())?) {
		
		guard contentType == .file else { return }
		
		//    if let _ = item?.thumbnails(0) {
		
		downloadTask = client.drive().items(item?.id).thumbnails("0").small().contentRequest().download(completion: { (url, response, error) in
			
			if let url = url {
				
				let image = UIImage(contentsOfFile: url.path)
				
				compleation!(image, nil)
				
			}
			
		})
		
		//    }
		
	}
	
	var identifier: String  {
		return self.item!.id
	}
	
	var name: String {
		return self.item!.name
	}
	
	var isVideo: Bool {
		return self.item!.video != nil
	}
	
}

class OneDrivePickerViewController: PickerViewController {
	
	override var pickerViewController: PickerViewController {
		return OneDrivePickerViewController()
	}
	
	override var defaultIdentifier: String {
		return "root"
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		OneDriveService.sharedInstance.fetchOneDriveContent(itemId: identifier) { [weak self] (items) in
			
			guard let strongSelf = self else { return }
			
			strongSelf.items = items
			
			DispatchQueue.main.async {
				strongSelf.collectionView?.delegate = self
				strongSelf.collectionView?.dataSource = self
				strongSelf.collectionView?.reloadData()
				
				LoaderView.close()
			
			}
			
		}
		
	}
	
	override func didSetNavigationBarView(navigationBarView: NavigationBarView) {
		super.didSetNavigationBarView(navigationBarView: navigationBarView)
		navigationBarView.topBarLabel.text = "OneDrive"
	}
	
	override func onUpload(item: UIBarButtonItem) {
		
		progressView = ProgressView.showUploadProgressView(inView: view, labelType: .items, cancel: {
			GoogleService.sharedInstance.cancelUpload()
		})
		
		OneDriveService.sharedInstance.upload(files: PhotosViewController.filesToUpload, itemId: identifier, progress: { (progress, written, total) in
			
			DispatchQueue.main.async {
				self.progressView?.setProgressLabel(progress: progress, written: written, total: total)
			}
			
		}) {
			
			guard LockService.sharedService.isDecoyEnabled == false else { return }
			
			DispatchQueue.main.async {
				
				guard AppDelegate.appIsLocked == false else {
					self.progressView?.compleateMode {
						
						self.progressView?.close()
						if let action = self.compleationAction { action() }
						let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
						
					}
					return
				}
				
				self.progressView?.close()
				if let action = self.compleationAction { action() }
				let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			}
			
			//      DispatchQueue.main.async {
			//        self.progressView?.close()
			//        let _ = self.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			//        if let action = self.compleationAction { action() }
			//      }
			
		}
		
	}
	
	override func onDownload(item: UIBarButtonItem) {
		var filesToDownload = [ODItem]()
		
		for indexPath in collectionView!.indexPathsForSelectedItems!.sorted(by: { $0.0.row > $0.1.row }) {
			filesToDownload.append(items[indexPath.row] as! ODItem)
		}
		
		progressView = ProgressView.showProgressView(inView: view, labelType: .size, cancel: {
			OneDriveService.sharedInstance.cancelDownload()
		})
		
		downloadObserver = OneDriveServiceDownloadObserver(progress: { (progress, written, total) in
			
			DispatchQueue.main.async {
				self.progressView!.setProgressLabel(progress: progress, written: written, total: total)
			}
			
		}, finish: { [weak self] in
			
			guard let strongSelf = self else { return }
			
			guard LockService.sharedService.isDecoyEnabled == false else { return }
			
			guard AppDelegate.appIsLocked == false else {
				strongSelf.progressView?.compleateMode {
					strongSelf.finishDownload()
				}
				return
			}
			
			strongSelf.finishDownload()
			
			
		}) {  [weak self] in
			
			guard let strongSelf = self else { return }
			
			let _ = strongSelf.navigationController?.popToViewController(PickerViewController.parentViewController, animated: true)
			
		}
		
		OneDriveService.sharedInstance.download(items: filesToDownload) { }
		
	}
	
	func finishDownload() {
		
	}
	
	override func cellDelegate(indexPath: IndexPath) -> PickerCellProtocol? {
		let item = items[indexPath.row]
		let delegate = OneDrivePickerCellDelegate(item: item as! ODItem)
		return delegate
	}
	
	override func onNewFolderButton(sender: UIButton) {
		
		let _ = NewFolderView.showsView(inView: view, ok: { (folderName) in
			
			OneDriveService.sharedInstance.createFolder(itemId: self.identifier, name: folderName, compleation: {[weak self] (item, error) in
				
				guard let strongSelf = self else { return }
				
				if error == nil {
					strongSelf.items.append(item!)
				}
				
				DispatchQueue.main.async {
					strongSelf.collectionView?.reloadData()
				}
				
			})
			
		}) {
			
		}
		
	}
	
}
