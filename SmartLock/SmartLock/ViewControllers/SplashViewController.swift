//
//  SplashViewController.swift
//  SmartLock
//
//  Created by Bogachev on 3/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class SplashViewController: BaseViewController, UINavigationControllerDelegate {
	
	private var reachabilityManager: NetworkReachabilityManager?
	private var progressIndicatorView: NVActivityIndicatorView?
	
	@IBOutlet weak var progressContainerView: UIView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.navigationBar.configurateTransparentNavigationBar()
		navigationController?.delegate = self
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		progressIndicatorView = NVActivityIndicatorView(frame: progressContainerView.bounds, type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.white)
		progressContainerView.addSubview(progressIndicatorView!)
		progressIndicatorView?.startAnimating()
		
		if LockService.sharedService.firstLaunch {
			let controller = StoryboardScene.instantiateChoosePasscodeTypeScene()
			self.navigationController?.setViewControllers([controller], animated: false)
		} else {
			
			if let reviewUserType = ReviewService.reviewUserType, reviewUserType == .old {

				let context = NSManagedObjectContext.mr_default()
				context.mr_save(blockAndWait: { (localContext) in
					
					MFile.mr_findAll(with: NSPredicate(format: "mobFilePath == nil"), in: localContext)?.forEach({ (object) in
						
						if let file = object as? MFile {
							file.mobThumbnail = "\(UUID().uuidString).jpg"
							if let mobPath = file.mobPath {
								file.mobFilePath = mobPath.deletingLastPathComponent()
							}
						}
						
					})
					
					MAlbum.mr_findAll(with: NSPredicate(format: "mobAlbumPath == nil"), in: localContext)?.forEach({ (object) in
						
						if let album = object as? MAlbum {
							
							album.mobThumbnail = "\(UUID().uuidString).jpg"
							if let mobPath = album.mobPath {
								album.mobAlbumPath = mobPath.deletingLastPathComponent()
							}
							
						}
						
					})
					
					
				})
				
			}
			
			if let appDelegate = (UIApplication.shared.delegate as? AppDelegate) {
				if let user = LockService.sharedService.user, let lockTypeValue = LockType(rawValue: Int(user.mobLockType)), lockTypeValue != .undefined {
					appDelegate.lockScreen()
				} else {
					appDelegate.pushPhotosViewControler()
				}
			}
			
		}
	}
	
	func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		
		if toVC is SmartLock.PhotosViewController {
			return FadeOutAnimatedTransitioning()
		}
		
		return nil
	}
	
}


