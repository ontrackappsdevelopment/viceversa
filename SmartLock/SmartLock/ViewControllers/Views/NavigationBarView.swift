//
//  NavigationBarView.swift
//  SmartLock
//
//  Created by Bogachev on 8/12/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import LTMorphingLabel

enum NavigationBarViewStyle {
	case colored
	case light
}

protocol NavigationBarViewDelegate {
	func onLeft(view: NavigationBarView, button: UIButton)
	func onRight(view: NavigationBarView, button: UIButton)
	var canShowBackupStatus: Bool { get }
}

class NavigationBarView: UIView {
	
	@IBOutlet weak var backupIcon: UIImageView! {
		didSet {
			backupIcon.alpha = 0
		}
	}
	@IBOutlet weak var backgroundImageView: UIImageView!
	@IBOutlet weak var topBarLabel: LTMorphingLabel! {
		didSet {
			topBarLabel.morphingEnabled = false
		}
	}
	@IBOutlet weak var leftTopBarButton: UIButton! {
		didSet {
			setupBackButton()
			leftTopBarButton.addTarget(self, action: #selector(onLeft(button:)), for: .touchUpInside)
		}
	}
	@IBOutlet weak var rightTopBarButton: UIButton! {
		didSet {
			setupNoButton(button: rightTopBarButton)
			rightTopBarButton.addTarget(self, action: #selector(onRight(button:)), for: .touchUpInside)
		}
	}
	
	var delegate: NavigationBarViewDelegate?
	
	var timer: Timer?

	static func instantiateFromXib(style: NavigationBarViewStyle) -> NavigationBarView? {
		
		let nibName = "NavigationBarColoredView"//style == .light ? "NavigationBarLightView" : "NavigationBarColoredView"
		
		if let view = Bundle.main.loadNibNamed(nibName, owner: nil, options: nil)?.first as? NavigationBarView {
			return view
		}
		
		return nil
	}
	
	static func instantiateAndAddToView(parentView: UIView, style: NavigationBarViewStyle) -> NavigationBarView?  {
		if let view = instantiateFromXib(style: style) {
			view.frame = parentView.bounds
			view.autoresizingMask = [.flexibleWidth]
			view.translatesAutoresizingMaskIntoConstraints = true
			parentView.addSubview(view)
			return view
		}
		return nil
	}
	
	func showBackupActiveStatus() {
		guard let delegate = delegate, delegate.canShowBackupStatus else { return }
		
		if timer == nil {
			timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(animateCloudIcon), userInfo: nil, repeats: true)
		}
	}
	
	func hideBackupActiveStatus() {
		if timer != nil {
			timer?.invalidate()
			timer = nil
		}
		
		UIView.animate(withDuration: 0.1) {
			self.backupIcon.alpha = 0.0
		}
	}
	
	func animateCloudIcon() {
		UIView.animate(withDuration: 0.1) {
			self.backupIcon.alpha = self.backupIcon.alpha == 1 ? 0 : 1
		}
	}
	
	@objc private func onLeft(button: UIButton) {
		if let delegate = delegate {
			delegate.onLeft(view: self, button: button)
		}
	}
	
	@objc private func onRight(button: UIButton) {
		if let delegate = delegate {
			delegate.onRight(view: self, button: button)
		}
	}
	
	func setupLeftButton(image: UIImage?, title: String?, target: Any?, action: Selector?) {
		setupButton(button: leftTopBarButton, image: image, title: title, target: target, action: action)
	}
	
	func setupRightButton(image: UIImage?, title: String?, target: Any?, action: Selector?) {
		setupButton(button: rightTopBarButton, image: image, title: title, target: target, action: action)
	}

	func setupButton(button: UIButton, image: UIImage?, title: String?, enabled: Bool = true, target: Any?, action: Selector?) {
		button.setImage(image, for: .normal)
		button.setTitle(title, for: .normal)
		button.isEnabled = enabled
		
		button.removeTarget(nil, action: nil, for: .touchUpInside)
		
		if let target = target, let action = action {
			button.addTarget(target, action: action, for: .touchUpInside)
		}
	}
	
	func setupEditButton(button: UIButton) {
		button.setImage(#imageLiteral(resourceName: "ico_edit"), for: .normal)
		button.setTitle(nil, for: .normal)
		button.isEnabled = true
	}
	
	func setupSettingsButton(button: UIButton) {
		button.setImage(#imageLiteral(resourceName: "ico_settings"), for: .normal)
		button.setTitle(nil, for: .normal)
		button.isEnabled = true
	}
	
	func setupBackButton() {
		leftTopBarButton.setImage(#imageLiteral(resourceName: "ic_back_white"), for: .normal)
		leftTopBarButton.setTitle(nil, for: .normal)
		leftTopBarButton.isEnabled = true
	}

	func setupNoButton(button: UIButton) {
		button.setImage(nil, for: .normal)
		button.setTitle(nil, for: .normal)
		button.isEnabled = false
	}

	func setupSelectAllButton(button: UIButton) {
		button.setImage(nil, for: .normal)
		button.setTitle("Select All", for: .normal)
		button.isEnabled = true
	}
	
	func setupDeselectAllButton(button: UIButton) {
		button.setImage(nil, for: .normal)
		button.setTitle("Deselect All", for: .normal)
		button.isEnabled = true
	}

	func setupCancelButton(button: UIButton) {
		button.setImage(nil, for: .normal)
		button.setTitle("Cancel", for: .normal)
		button.isEnabled = true
	}
	
	func setupDownloadButton(button: UIButton) {
		button.setImage(nil, for: .normal)
		button.setTitle("Download", for: .normal)
		button.isEnabled = true
	}
	
}
