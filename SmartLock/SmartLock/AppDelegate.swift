	//
//  AppDelegate.swift
//  SmartLock
//
//  Created by Bogachev on 2/3/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import SwiftyDropbox
import MagicalRecord
import Fabric
import Crashlytics
import SwiftyStoreKit
import YandexMobileMetrica
import FacebookCore

let applicationDidUnlocked = NSNotification.Name(rawValue: "ApplicationDidUnlocked")

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CrashlyticsDelegate {
	
	var window: UIWindow?
	var inactiveView: UIView?
	var lockViewController: LockViewController?
	
	static var appIsLocked: Bool = false {
		didSet {
			if appIsLocked == false && oldValue == true {
				NotificationCenter.default.post(name: applicationDidUnlocked, object: nil)
			}
		}
	}
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
				
		UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
		
		AppsFlyerTracker.shared().appsFlyerDevKey = "BPqPd3LAjKyAkEEnJ4tPeZ"
		AppsFlyerTracker.shared().appleAppID = "id1204082913"
		AppsFlyerTracker.shared().trackAppLaunch()
		
		AppEventsLogger.activate(application)
		SDKSettings.enableLoggingBehavior(.appEvents)
		
//		if let receiptData = KeychainService.loadData(service: "SmartLockAppReceipt") {
//			KeychainService.delete(service: "SmartLockAppReceipt", data1: receiptData)
//		}
//		
//		if let receiptData = KeychainService.loadData(service: "firstLaunchDateKey") {
//			KeychainService.delete(service: "firstLaunchDateKey", data1: receiptData)
//		}

		Fabric.with([Crashlytics.self])
		
		YMMYandexMetrica.activate(withApiKey: "059fdba7-2383-4b3f-8b90-bd96b4c85822")

		MagicalRecord.setupAutoMigratingCoreDataStack()
		
		let navigationBarApearance = UINavigationBar.appearance()
		let font = UIFont(name: "SFUIDisplay-Regular", size: 18)
		let color = UIColor.white
		navigationBarApearance.titleTextAttributes = [NSFontAttributeName: font!, NSForegroundColorAttributeName:color]
		
		application.applicationSupportsShakeToEdit = true
		
		completeIAPTransactions()
		
		setup()
		
		return true
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		NetworkManager.sharedInstance.switchToBackground()
		lockScreen()
	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		
		if inactiveView != nil {
			inactiveView?.removeFromSuperview()
			inactiveView = nil
		}
		
		LockService.sharedService.launhes += 1
		
		NetworkManager.sharedInstance.switchToForeground()
	}

	func applicationWillResignActive(_ application: UIApplication) {
		
		guard AppDelegate.appIsLocked == false else { return }
		guard inactiveView == nil else { return }
		
		guard BaseViewController.avoidHidingWhenResignActive == false else {
			BaseViewController.avoidHidingWhenResignActive = true
			return
		}
		
		if let window = window {
			window.endEditing(true)
		}
		
		inactiveView = UIView(frame: UIScreen.main.bounds)
		
		inactiveView?.backgroundColor = UIColor.black
		
		UIApplication.shared.keyWindow!.addSubview(inactiveView!)
	}
	
	func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
		
		if let scheme = url.scheme {
			
			switch scheme {
				
			case "com.smartlock.app":
				
				return YandexService.sharedInstance.handleURL(url)
				
			case "com.googleusercontent.apps.748264322596-p2uhqfntdcd130jgkteha5ensi1dqlkg":
				
				return GoogleService.sharedInstance.resumeAuthorizationFlowWithURL(url: url)
				
			case "db-l9ao07i0t8k3x50":
				
				return DropBoxService.sharedInstance.handleURL(url: url)
				
			default: break
				
			}
			
		}
		
		return true
		
	}

	func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
		return self.application(application, open: url, options: [UIApplicationOpenURLOptionsKey : Any]())
	}
	
	func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
		NetworkManager.sharedInstance.backgroundCompletionHandler = completionHandler
	}

	//MARK: - Internal
	
	private func setup() {
		
		User.verifyReceipt { (result) in }
		
		print("\(NSPersistentStore.mr_url(forStoreName: MagicalRecord.defaultStoreName())!.path)")
		
		if let deviceID = MDevice.mr_findFirst()?.mobID {
			print("deviceID: \(deviceID)")
		}
		
		if let email = MUser.mr_findFirst()?.mobEmail {
			print("deviceID: \(email)")
		}
		
		KeychainService.saveFirstLaunchDate()
		
		_ = FileService.sharedInstance
		_ = DropBoxService.sharedInstance
		_ = OneDriveService.sharedInstance
		_ = LocationManager.sharedInstance
		_ = BackupService.sharedInstance
		
		DataManager.createDeviceIfNeeded()
		
		if let bundleVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
			if let userDefaultsVersion = UserDefaults.standard.string(forKey: "AppVersion") {
				
				if userDefaultsVersion != bundleVersion {
					ReviewService.setupUserSession(type: .update)
					UserDefaults.standard.set(Date(), forKey: "AppVersionDate")
					
					UserDefaults.standard.removeObject(forKey: "canShowSwipeHint")
					UserDefaults.standard.removeObject(forKey: "canShowDrageHint")
					UserDefaults.standard.removeObject(forKey: "canShowBackupException")
				}
				
			} else {
				
				ReviewService.setupUserSession(type: .new)
				UserDefaults.standard.set(Date(), forKey: "AppVersionDate")
			}
			
			UserDefaults.standard.set(bundleVersion, forKey: "AppVersion")
		}
		
		UserDefaults.standard.synchronize()
		
	}
	
	private func completeIAPTransactions() {
		
		SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
			
			for purchase in purchases {
				
				if purchase.transaction.transactionState == .purchased || purchase.transaction.transactionState == .restored {
					
					if purchase.needsFinishTransaction {
						
						SwiftyStoreKit.finishTransaction(purchase.transaction)
					}
					
					print("purchased: \(purchase.productId)")
				}
			}
		}
	}	
	
	func pushPhotosViewControler(animated : Bool = true) {
		if let window = window, let navigationController = window.rootViewController as? UINavigationController {
			let controller = StoryboardScene.Main.instantiatePhotosScene()
			navigationController.setViewControllers([controller], animated: animated)
		}
	}
	
	//MARK: - Public
	
	func lockScreen() {
		
		guard AppDelegate.appIsLocked == false else { return }
		
		guard let user = LockService.sharedService.user, let lockTypeValue = LockType(rawValue: Int(user.mobLockType)) else { return }
		
		guard let lockViewController = LockViewController.getLockControllerBy(lockType: lockTypeValue) else { return }
		
		self.lockViewController = lockViewController
		
		lockViewController.lockMode = .unlock
		lockViewController.passcode = user.mobPasscode
		lockViewController.canCancel = false
		
		lockViewController.success = { (pin: String) in
			
			if LockService.sharedService.isDecoyEnabled == true {
				
				AppDelegate.appIsLocked = false
				LockService.sharedService.isDecoyEnabled = false
				FileService.sharedInstance.isDecoyEnabled = false
				
				self.removeLock()
				
				self.pushPhotosViewControler(animated: false)
				
			} else {
				
				if let window = self.window, let rootViewController = window.rootViewController {					
					if let navigationController = rootViewController as? UINavigationController,
						let _ = navigationController.viewControllers.first as? SplashViewController,
						navigationController.viewControllers.count == 2 {
					
						self.removeLock()
						
						self.pushPhotosViewControler()
						AppDelegate.appIsLocked = false

					} else {
						
						DispatchQueue.main.async {
							UIView.animate(withDuration: 0.7, animations: {
								lockViewController.view.alpha = 0
							}, completion: { (_) in
								
								self.removeLock()
								
								AppDelegate.appIsLocked = false
							})
						}
						
					}
					
				}
			}
			
		}
		
		lockViewController.failure = {
			
			guard let decoyType = DecoyType(rawValue: Int(LockService.sharedService.user!.mobDecoyPasscodeType)) else {
				return
			}
			
			if decoyType != .decoyOff {
				
				AppDelegate.appIsLocked = false
				
				LockService.sharedService.isDecoyEnabled = true
				FileService.sharedInstance.isDecoyEnabled = true
				
				self.removeLock()
				
				self.pushPhotosViewControler(animated: false)
			}
			
		}
		
		if let window = window, let rootViewController = window.rootViewController {
			window.endEditing(true)
			
			AppDelegate.appIsLocked = true
			rootViewController.addChildViewController(lockViewController)
			lockViewController.view.backgroundColor = UIColor.black
			lockViewController.view.frame = UIScreen.main.bounds;
			window.addSubview(lockViewController.view)
			rootViewController.didMove(toParentViewController: lockViewController)
			lockViewController.viewWillAppear()
		}
	}
	
	func removeLock() {
		guard let lockViewController = lockViewController else { return }
		
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidUnlockNotification"), object: nil)
		
		lockViewController.remove()
		self.lockViewController = nil
	}
		
	func removeLockAndPushPhotos() {
		if	let window = self.window,
			let rootViewController = window.rootViewController,
			let _ = rootViewController as? UINavigationController {
			
			self.removeLock()
			self.pushPhotosViewControler()
			AppDelegate.appIsLocked = false
			
		}
	}
}
