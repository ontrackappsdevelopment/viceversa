//
//  MRestoreLog.swift
//  SmartLock
//
//  Created by Bogachev on 4/29/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension MRestoreLog {
	
	var fileURL: URL? {
		
		return FileService.documentsDirectory.appendingPathComponent(mobPath!)
		
	}
	
}
