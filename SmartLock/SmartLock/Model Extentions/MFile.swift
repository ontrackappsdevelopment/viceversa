//
//  MFile.swift
//  SmartLock
//
//  Created by Bogachev on 4/19/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension MFile {
	
	static func create(with path: String, context: NSManagedObjectContext) -> MFile? {
		
		if let file = MFile.mr_createEntity(in: context) {
			
			file.mobThumbnail = "\(UUID().uuidString).jpg"			
			file.mobPath = path
			file.mobFilePath = path.deletingLastPathComponent()
			file.mobDate = Date().timeIntervalSince1970
			_ = MLog.createFile(with: path, context: context)
			
			return file
		}
		
		return nil
	}
	
	static func createFileWithoutLog(with path: String, uuid: String, context: NSManagedObjectContext) {
		
		let file = MFile.mr_createEntity(in: context)!
		
		file.mobThumbnail = "\(UUID().uuidString).jpg"
		file.mobPath = path
		file.mobFilePath = path.deletingLastPathComponent()
		file.mobUUID = uuid
		file.mobDate = Date().timeIntervalSince1970
	}
	
	func moveTo(path: String, context: NSManagedObjectContext) {
		
		let oldPath = mobPath!
		let newPath = path
		
		_ = MLog.moveFile(from: oldPath, to: newPath, context: context)
		
		mobPath = newPath
		mobFilePath = newPath.deletingLastPathComponent()
		mobDate = Date().timeIntervalSince1970		
	}
	
	func moveToWithouLog(path: String, context: NSManagedObjectContext) {
		mobPath = path
		mobFilePath = path.deletingLastPathComponent()
		mobDate = Date().timeIntervalSince1970
	}

	static func deleteWithouLog(with path: String, context: NSManagedObjectContext) {
		if let file = MFile.mr_findFirst(byAttribute: "mobPath", withValue: path, in: context) {
			file.mr_deleteEntity(in: context)
		}
	}
	
	static func delete(with path: String, context: NSManagedObjectContext) {
		if let file = MFile.mr_findFirst(byAttribute: "mobPath", withValue: path, in: context) {
			_ = MLog.deleteFile(with: file.mobPath!, uuid: file.mobUUID, context: context)
			file.mr_deleteEntity(in: context)
		}		
	}
	
}
