//
//  CMTime.swift
//  SmartLock
//
//  Created by Bogachev on 3/27/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import AVFoundation

extension CMTime {
  
  func toString() -> String {
    
    let ti = self.toSeconds()/1000
    
    let seconds = ti % 60
    let minutes = (ti / 60) % 60
    
    return String(format: "%0.2d:%0.2d", minutes, seconds)
    
  }
  
  func toSeconds() -> Int64 {
    
    if timescale == 0 {
      return 0
    }
    
    return (1000 * value) / Int64(timescale)
  }
  
}
