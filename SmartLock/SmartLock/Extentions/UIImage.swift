//
//  UIImage.swift
//  SmartLock
//
//  Created by Bogachev on 2/22/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension UIImage {
  
  func fixOrientationOfImage() -> UIImage? {
    
    let image = self
    
    if image.imageOrientation == .up {
      return image
    }
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    var transform = CGAffineTransform.identity
    
    switch image.imageOrientation {
    case .down, .downMirrored:
      transform = transform.translatedBy(x: image.size.width, y: image.size.height)
      transform = transform.rotated(by: CGFloat(Double.pi))
    case .left, .leftMirrored:
      transform = transform.translatedBy(x: image.size.width, y: 0)
      transform = transform.rotated(by: CGFloat(Double.pi / 2))
    case .right, .rightMirrored:
      transform = transform.translatedBy(x: 0, y: image.size.height)
      transform = transform.rotated(by: -CGFloat(Double.pi / 2))
    default:
      break
    }
    
    switch image.imageOrientation {
    case .upMirrored, .downMirrored:
      transform = transform.translatedBy(x: image.size.width, y: 0)
      transform = transform.scaledBy(x: -1, y: 1)
    case .leftMirrored, .rightMirrored:
      transform = transform.translatedBy(x: image.size.height, y: 0)
      transform = transform.scaledBy(x: -1, y: 1)
    default:
      break
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    guard let context = CGContext(data: nil, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: image.cgImage!.bitsPerComponent, bytesPerRow: 0, space: image.cgImage!.colorSpace!, bitmapInfo: image.cgImage!.bitmapInfo.rawValue) else {
      return nil
    }
    
    context.concatenate(transform)
    
    switch image.imageOrientation {
    case .left, .leftMirrored, .right, .rightMirrored:
      context.draw(image.cgImage!, in: CGRect(x: 0, y: 0, width: image.size.height, height: image.size.width))
    default:
      context.draw(image.cgImage!, in: CGRect(origin: .zero, size: image.size))
    }
    
    // And now we just create a new UIImage from the drawing context
    guard let CGImage = context.makeImage() else {
      return nil
    }
    
    return UIImage(cgImage: CGImage)
  }
  
  func rotateCameraImageToProperOrientation(maxResolution : CGFloat = 320) -> UIImage? {
    
    let imageSource = self
    
    guard let imgRef = imageSource.cgImage else {
      return nil
    }
    
    let width = CGFloat(imgRef.width)
    let height = CGFloat(imgRef.height)
    
    var bounds = CGRect(x: 0, y: 0, width: width, height: height)
    
    var scaleRatio : CGFloat = 1
    if (width > maxResolution || height > maxResolution) {
      
      scaleRatio = min(maxResolution / bounds.size.width, maxResolution / bounds.size.height)
      bounds.size.height = bounds.size.height * scaleRatio
      bounds.size.width = bounds.size.width * scaleRatio
    }
    
    var transform = CGAffineTransform.identity
    let orient = imageSource.imageOrientation
    let imageSize = CGSize(width: CGFloat(imgRef.width), height: CGFloat(imgRef.height))
    
    switch(imageSource.imageOrientation) {
    case .up:
      transform = .identity
    case .upMirrored:
      transform = CGAffineTransform
        .init(translationX: imageSize.width, y: 0)
        .scaledBy(x: -1.0, y: 1.0)
    case .down:
      transform = CGAffineTransform
        .init(translationX: imageSize.width, y: imageSize.height)
        .rotated(by: CGFloat.pi)
    case .downMirrored:
      transform = CGAffineTransform
        .init(translationX: 0, y: imageSize.height)
        .scaledBy(x: 1.0, y: -1.0)
    case .left:
      let storedHeight = bounds.size.height
      bounds.size.height = bounds.size.width;
      bounds.size.width = storedHeight;
      transform = CGAffineTransform
        .init(translationX: 0, y: imageSize.width)
        .rotated(by: 3.0 * CGFloat.pi / 2.0)
    case .leftMirrored:
      let storedHeight = bounds.size.height
      bounds.size.height = bounds.size.width;
      bounds.size.width = storedHeight;
      transform = CGAffineTransform
        .init(translationX: imageSize.height, y: imageSize.width)
        .scaledBy(x: -1.0, y: 1.0)
        .rotated(by: 3.0 * CGFloat.pi / 2.0)
    case .right :
      let storedHeight = bounds.size.height
      bounds.size.height = bounds.size.width;
      bounds.size.width = storedHeight;
      transform = CGAffineTransform
        .init(translationX: imageSize.height, y: 0)
        .rotated(by: CGFloat.pi / 2.0)
    case .rightMirrored:
      let storedHeight = bounds.size.height
      bounds.size.height = bounds.size.width;
      bounds.size.width = storedHeight;
      transform = CGAffineTransform
        .init(scaleX: -1.0, y: 1.0)
        .rotated(by: CGFloat.pi / 2.0)
    }
    
    UIGraphicsBeginImageContext(bounds.size)
    if let context = UIGraphicsGetCurrentContext() {
      if orient == .right || orient == .left {
        context.scaleBy(x: -scaleRatio, y: scaleRatio)
        context.translateBy(x: -height, y: 0)
      } else {
        context.scaleBy(x: scaleRatio, y: -scaleRatio)
        context.translateBy(x: 0, y: -height)
      }
      
      context.concatenate(transform)
      context.draw(imgRef, in: CGRect(x: 0, y: 0, width: width, height: height))
    }
    
    let imageCopy = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return imageCopy
  }
  
}
