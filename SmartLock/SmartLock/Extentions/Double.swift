//
//  Double.swift
//  SmartLock
//
//  Created by Bogachev on 3/11/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension Double {

  func roundTo(places:Int) -> Double {
    let divisor = pow(10.0, Double(places))
    return (self * divisor).rounded() / divisor
  }
  
}
