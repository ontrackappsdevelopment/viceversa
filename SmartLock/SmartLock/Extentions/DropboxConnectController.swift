//
//  DropboxConnectController.swift
//  SmartLock
//
//  Created by Bogachev on 2/20/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation
import SwiftyDropbox

extension DropboxConnectController {
  

  
  override open func viewDidLoad() {
    super.viewDidLoad()
    
    setNeedsStatusBarAppearanceUpdate()
  }
  
  override open var preferredStatusBarStyle: UIStatusBarStyle {
    return .default
  }
  
}
