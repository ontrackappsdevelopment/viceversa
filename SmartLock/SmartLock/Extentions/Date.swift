//
//  Date.swift
//  SmartLock
//
//  Created by Bogachev on 3/9/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension Date {
  
  static public var localDate : Date? {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    formatter.timeZone = NSTimeZone.local
    
    let stringDate = formatter.string(from: Date())
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    
    return formatter.date(from: stringDate)
  }

  public func toString() -> String {
    let formatter = DateFormatter()
//    formatter.dateStyle = .short
//    formatter.dateStyle = .full
    formatter.locale = Locale(identifier: "en_US_POSIX")
	formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.dateFormat = "MMMM dd, yyyy hh:mm a"

    let stringDate = formatter.string(from: self)
    
    return stringDate
  }
  
}
