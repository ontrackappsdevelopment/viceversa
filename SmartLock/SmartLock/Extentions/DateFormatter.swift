//
//  DateFormatter.swift
//  SmartLock
//
//  Created by Bogachev on 6/3/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension DateFormatter {
	
	static var defaultFormatter: DateFormatter {
		let formatter = DateFormatter()
		formatter.dateStyle = .full
		formatter.timeStyle = .full
		return formatter
	}
	
}
