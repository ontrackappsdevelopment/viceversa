//
//  Float.swift
//  SmartLock
//
//  Created by Bogachev on 6/18/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import Foundation

extension Float {
	func roundTo(places: Int) -> Float {
		let divisor = pow(10.0, Float(places))
		return floorf(self * divisor) / divisor
	}
}
