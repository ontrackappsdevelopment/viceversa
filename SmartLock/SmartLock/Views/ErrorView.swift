//
//  ErrorView.swift
//  SmartLock
//
//  Created by Bogachev on 4/2/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class ErrorView: UIView {
  
  @IBOutlet weak var label: UILabel!
  @IBOutlet weak var errorView: UIView!
  
  @IBOutlet weak var errorViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var errorViewLeadingConstraint: NSLayoutConstraint!
  @IBOutlet weak var errorViewTrailingConstraint: NSLayoutConstraint!
  
  static func instantiateFromXib() -> ErrorView {
    
    let view = Bundle.main.loadNibNamed("ErrorView", owner: nil, options: nil)?.first as! ErrorView
    
    return view
    
  }
  
  static func shows(inViewController controller: UIViewController, text: String) -> ErrorView {
    
    let view = instantiateFromXib()
    view.frame = UIScreen.main.bounds
    controller.view.addSubview(view)
    view.label.text = text
    
    if UIDevice.current.isIpad() == true {
      NSLayoutConstraint.activate([view.errorViewWidthConstraint])
      NSLayoutConstraint.deactivate([view.errorViewLeadingConstraint, view.errorViewTrailingConstraint])
    } else {
      NSLayoutConstraint.deactivate([view.errorViewWidthConstraint])
      NSLayoutConstraint.activate([view.errorViewLeadingConstraint, view.errorViewTrailingConstraint])
    }
    
    view.errorView.transform = CGAffineTransform(scaleX: 0, y: 0)
    
    UIView.animate(withDuration: 0.3, animations: {
      
      view.errorView.transform = CGAffineTransform.identity
      
    }) { (_) in
      
      DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5, execute: {
        
        UIView.animate(withDuration: 0.3, animations: {
          view.errorView.alpha = 0
        }, completion: { (_) in
          view.removeFromSuperview()
        })
        
      })
      
    }
    
    return view
    
  }
  
}
