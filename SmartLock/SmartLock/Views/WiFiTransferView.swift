//
//  WiFiTransferView.swift
//  SmartLock
//
//  Created by Bogachev on 2/28/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit

class WiFiTransferView: UIView {
  
  static func showWiFiTransferView(inView view: UIView, port: String, cancel: @escaping () -> ()) -> CustomizableActionSheet {
    
    var items = [CustomizableActionSheetItem]()
    
    if let view = UINib(nibName: "WiFiTransferView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? WiFiTransferView {
      
      view.addressLabel.text = port
      
      let sampleViewItem = CustomizableActionSheetItem()
      sampleViewItem.type = .view
      sampleViewItem.view = view
      sampleViewItem.height = 400
      items.append(sampleViewItem)
    }
    
    let closeItem = CustomizableActionSheetItem()
    closeItem.type = .button
    closeItem.label = "Cancel"
    closeItem.backgroundColor = UIColor.clear
    closeItem.backgroundImage = UIImage(named: "menu_cancel_background")
    closeItem.textColor = UIColor.textColor
    closeItem.font = UIFont(name: "SFUIDisplay-Medium", size: 24.0)
    closeItem.selectAction = { (actionSheet: CustomizableActionSheet) -> Void in
      actionSheet.dismiss(compleation: { 
        cancel()
      })
    }
    
    items.append(closeItem)
    
    let menuSheets = CustomizableActionSheet()
    
    menuSheets.showInView(view, items: items) {
      cancel()
    }
    
    return menuSheets
  }
  
  @IBOutlet weak var addressLabel: UILabel!
  
}
