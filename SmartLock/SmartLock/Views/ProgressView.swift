//
//  ProgressView.swift
//  SmartLock
//
//  Created by Bogachev on 2/17/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import YLProgressBar

enum LabelType {
  
  case items
  case size
  
}

enum ProgressType {
  case download
  case upload
}

class ProgressView: UIView {
  
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var progressLabel: UILabel!
  @IBOutlet weak var progressBar: YLProgressBar!
  @IBOutlet weak var progressView: UIView!
  @IBOutlet weak var cancelButton: UIButton!
  
  @IBOutlet weak var errorViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var errorViewLeadingConstraint: NSLayoutConstraint!
  @IBOutlet weak var errorViewTrailingConstraint: NSLayoutConstraint!
  
  var cancelAction: (() -> ())!
  
  var labelType: LabelType = .size
  
  var progressType: ProgressType = .download
  
  static func instantiateFromXib() -> ProgressView {
    let view = Bundle.main.loadNibNamed("ProgressView", owner: nil, options: nil)?.first as! ProgressView
    view.frame = UIScreen.main.bounds
    view.configurateProgressBar()
    view.progressBar.progress = 0.0
    
    return view
  }
  
  static func showDownloadProgressView(inView parentView: UIView, labelType: LabelType, cancel: @escaping () -> ()) -> ProgressView {
    let view = showProgressView(inView: parentView, labelType: labelType, cancel: cancel)
    view.iconImageView.image = UIImage(named: "ico_download")
    view.progressType = .download
    return view
  }

  static func showUploadProgressView(inView parentView: UIView, labelType: LabelType, cancel: @escaping () -> ()) -> ProgressView {
    let view = showProgressView(inView: parentView, labelType: labelType, cancel: cancel)
    view.iconImageView.image = UIImage(named: "ico_upload")
    view.progressType = .upload
    return view
  }
  
  static func showProgressView(inView view: UIView, labelType: LabelType, cancel: @escaping () -> ()) -> ProgressView {
    
    let progressView = instantiateFromXib()
    progressView.labelType = labelType
    progressView.frame = UIScreen.main.bounds
    
    if UIDevice.current.isIpad() == true {
      NSLayoutConstraint.activate([progressView.errorViewWidthConstraint])
      NSLayoutConstraint.deactivate([progressView.errorViewLeadingConstraint, progressView.errorViewTrailingConstraint])
    } else {
      NSLayoutConstraint.deactivate([progressView.errorViewWidthConstraint])
      NSLayoutConstraint.activate([progressView.errorViewLeadingConstraint, progressView.errorViewTrailingConstraint])
    }
    
    progressView.cancelAction = cancel
    
    view.addSubview(progressView)
    
    progressView.progressView.transform = CGAffineTransform(scaleX: 0, y: 0)
    
    UIView.animate(withDuration: 0.3) {

      progressView.progressView.transform = CGAffineTransform.identity
    
    }
    
    return progressView
  }
  
  func configurateProgressBar() {
    
    progressBar.setProgress(0, animated: true)
    
    progressBar.progressTintColors = [UIColor(colorLiteralRed: 66/255, green: 18/255, blue: 138/255, alpha: 1),
                                      UIColor(colorLiteralRed: 70/255, green: 112/255, blue: 221/255, alpha: 1)]
    
    progressBar.progressBarInset = 0
    progressBar.trackTintColor = UIColor.white
    progressBar.type = .flat
    progressBar.behavior = .indeterminate
    progressBar.stripesOrientation = .vertical
  }
  
  func setProgressLabel(progress: Float, written: Int, total: Int) {
    
    print("downloaded: \(written), total: \(total), progress: \(progress)")
    
    let downloadText = progressType == .download ? "Download " : "Upload "
    var downloadedText = ""
    var totalText = ""
    
    downloadedText = "\(written) "
    totalText = "of \(total) items "

    let attributedString = NSMutableAttributedString(string: "\(downloadText) \(downloadedText) \(totalText)")
    
    attributedString.addAttributes([
      NSForegroundColorAttributeName:UIColor.textColor,
      NSFontAttributeName:UIFont(name: "SFUIDisplay-Medium", size: 18.0)!], range: NSRange(location: 0, length: downloadText.characters.count))
    
    attributedString.addAttributes([
      NSForegroundColorAttributeName:UIColor.blueBackgroundColor,
      NSFontAttributeName:UIFont(name: "SFUIDisplay-Medium", size: 18.0)!], range: NSRange(location: downloadText.characters.count + 1, length: downloadedText.characters.count))
    
    attributedString.addAttributes([
      NSForegroundColorAttributeName:UIColor.textColor,
      NSFontAttributeName:UIFont(name: "SFUIDisplay-Medium", size: 18.0)!], range: NSRange(location: downloadText.characters.count + downloadedText.characters.count + 1, length: totalText.characters.count))
    
    progressLabel.attributedText = attributedString
    
    self.progressBar.setProgress(CGFloat(progress), animated: false)
    
  }
  
  func setProgressLabel(downloaded: Float, total: Float) {
    
    print("downloaded: \(downloaded), total: \(total), progress: \(CGFloat(downloaded / total))")
    
    let downloadText = progressType == .download ? "Download " : "Upload "
    var downloadedText = ""
    var totalText = ""
    
    switch labelType {
    case .items:
      downloadedText = "\(Int(downloaded)) "
      totalText = "of \(Int(total)) items "
    case .size:
      downloadedText = "\(String(format: "%.1f", downloaded)) MB "
      totalText = "of \(String(format: "%.1f", total)) MB "
    }
    
    let attributedString = NSMutableAttributedString(string: "\(downloadText) \(downloadedText) \(totalText)")
    
    attributedString.addAttributes([
      NSForegroundColorAttributeName:UIColor.textColor,
      NSFontAttributeName:UIFont(name: "SFUIDisplay-Medium", size: 18.0)!], range: NSRange(location: 0, length: downloadText.characters.count))
    
    attributedString.addAttributes([
      NSForegroundColorAttributeName:UIColor.blueBackgroundColor,
      NSFontAttributeName:UIFont(name: "SFUIDisplay-Medium", size: 18.0)!], range: NSRange(location: downloadText.characters.count + 1, length: downloadedText.characters.count))
    
    attributedString.addAttributes([
      NSForegroundColorAttributeName:UIColor.textColor,
      NSFontAttributeName:UIFont(name: "SFUIDisplay-Medium", size: 18.0)!], range: NSRange(location: downloadText.characters.count + downloadedText.characters.count + 1, length: totalText.characters.count))
    
    progressLabel.attributedText = attributedString
    
    self.progressBar.setProgress(CGFloat(downloaded / total), animated: false)
    
  }
  
  func close() {
    
    UIView.animate(withDuration: 0.3, animations: {
      
      self.progressView.frame = CGRect(x: self.bounds.width, y: 0, width: 0, height: 0)
      
    }) { (compleat) in
      
      self.removeFromSuperview()
      
    }

  }
  
  func compleateMode(finish: @escaping () -> ()) {
    cancelButton.setTitle("Complete", for: .normal)
    cancelAction = finish
  }
  
  @IBAction func onCancel(_ sender: UIButton) {
    cancelAction()
    close()
  }
  
}
