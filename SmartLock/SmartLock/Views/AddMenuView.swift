//
//  AddMenuView.swift
//  SmartLock
//
//  Created by Bogachev on 2/5/17.
//  Copyright © 2017 AAA. All rights reserved.
//

import UIKit
import Alamofire
import YandexMobileMetrica

@objc protocol AddMenuViewDelegate {
  func openCamera(view: AddMenuView)
  func openCameraRoll(view: AddMenuView)
  func createAlbum(view: AddMenuView)
  func chooseCloud(view: AddMenuView)
  func wifiTransfer(view: AddMenuView)
}

class AddMenuView: UIView {
  
  static func showAddMenu(inView view: UIView, delegate: AddMenuViewDelegate, cancel: @escaping () -> ()) -> CustomizableActionSheet {
    
    var items = [CustomizableActionSheetItem]()
    
    if let addMenuView = UINib(nibName: "AddMenuView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? AddMenuView {
      let sampleViewItem = CustomizableActionSheetItem()
      sampleViewItem.type = .view
      sampleViewItem.view = addMenuView
      sampleViewItem.height = 260
      items.append(sampleViewItem)
      addMenuView.delegate = delegate
      
      addMenuView.startNetworkReachabilityObserver()
      
    }
    
    let closeItem = CustomizableActionSheetItem()
    closeItem.type = .button
    closeItem.label = "Cancel"
    closeItem.backgroundColor = UIColor.clear
    closeItem.backgroundImage = UIImage(named: "menu_cancel_background")
    closeItem.textColor = UIColor.textColor
    closeItem.font = UIFont(name: "SFUIDisplay-Medium", size: 24.0)
    closeItem.selectAction = { (actionSheet: CustomizableActionSheet) -> Void in
      actionSheet.dismiss {
        cancel()
      }
    }
    
    items.append(closeItem)
    
    let addMenuSheets = CustomizableActionSheet()
    
    addMenuSheets.showInView(view, items: items) {
      cancel()
    }
        
    return addMenuSheets
  }
  
  static func showDecoyAddMenu(inView view: UIView, delegate: AddMenuViewDelegate, cancel: @escaping () -> ()) -> CustomizableActionSheet {
    
    var items = [CustomizableActionSheetItem]()
    
    if let addMenuView = UINib(nibName: "AddMenuViewDecoy", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? AddMenuView {
      let sampleViewItem = CustomizableActionSheetItem()
      sampleViewItem.type = .view
      sampleViewItem.view = addMenuView
      sampleViewItem.height = 220
      items.append(sampleViewItem)
      addMenuView.delegate = delegate
      
    }
    
    let closeItem = CustomizableActionSheetItem()
    closeItem.type = .button
    closeItem.label = "Cancel"
    closeItem.backgroundColor = UIColor.clear
    closeItem.backgroundImage = UIImage(named: "menu_cancel_background")
    closeItem.textColor = UIColor.textColor
    closeItem.font = UIFont(name: "SFUIDisplay-Medium", size: 24.0)
    closeItem.selectAction = { (actionSheet: CustomizableActionSheet) -> Void in
      actionSheet.dismiss {
        cancel()
      }
    }
    
    items.append(closeItem)
    
    let addMenuSheets = CustomizableActionSheet()
    
    addMenuSheets.showInView(view, items: items) {
      cancel()
    }
    
    return addMenuSheets
  }
  
  weak var delegate: AddMenuViewDelegate?
  @IBOutlet weak var photosBotton: UIButton!
  
  @IBOutlet weak var topButtonLeftPosition: NSLayoutConstraint!
  @IBOutlet weak var topButtonRightPosition: NSLayoutConstraint!
  @IBOutlet weak var phtotosButtonContent: UIView!
  @IBOutlet weak var wifiStatusImageView: UIImageView!
  @IBOutlet weak var wifiStatusLabel: UILabel!
  @IBOutlet weak var wifiButton: UIButton!
  
  func startNetworkReachabilityObserver() {
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")!
    
    if reachabilityManager.networkReachabilityStatus != .reachable(.ethernetOrWiFi) {
      wifiStatusImageView.image = UIImage(named: "ico_wifi_disabled")
      wifiStatusLabel.text = "Wi-Fi Transfer \n(no network)"
      wifiButton.isEnabled = false
    }
    
    reachabilityManager.listener = {[weak self] status in
      
      guard let strongSelf = self else { return }
      
      switch status {
      case .reachable(.ethernetOrWiFi):
        strongSelf.wifiStatusImageView.image = UIImage(named: "ico_wifi_disabled")
        strongSelf.wifiStatusLabel.text = "Wi-Fi Transfer \n(network detected)"
        strongSelf.wifiButton.isEnabled = true
        
      default:
        strongSelf.wifiStatusImageView.image = UIImage(named: "ico_wifi_disabled")
        strongSelf.wifiStatusLabel.text = "Wi-Fi Transfer \n(no network)"
        strongSelf.wifiButton.isEnabled = false
      }
      
      reachabilityManager.startListening()
      
    }
  }
  
  @IBAction func onPhotos(_ sender: UIButton) {
	YMMYandexMetrica.reportEvent("Import", parameters: ["event": "Photoroll"], onFailure: nil)

    if let delegate = delegate {
      delegate.openCameraRoll(view: self)
    }
  }
  
  @IBAction func onCamera(_ sender: UIButton) {
	YMMYandexMetrica.reportEvent("Import", parameters: ["event": "Camera"], onFailure: nil)
    if let delegate = delegate {
      delegate.openCamera(view: self)
    }
  }
  
  @IBAction func onNewAlbum(_ sender: UIButton) {
    if let delegate = delegate {
      delegate.createAlbum(view: self)
    }
  }
  
  @IBAction func onClouds(_ sender: UIButton) {
    if let delegate = delegate {
      delegate.chooseCloud(view: self)
    }
  }
  
  @IBAction func onWiFi(_ sender: UIButton) {
	YMMYandexMetrica.reportEvent("Import", parameters: ["event": "Wi-fi"], onFailure: nil)
    if let delegate = delegate {
      delegate.wifiTransfer(view: self)
    }
  }
  
  @IBAction func onAirDrop(_ sender: UIButton) {
    
  }
  
}
