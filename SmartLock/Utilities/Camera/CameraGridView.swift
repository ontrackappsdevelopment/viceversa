//
//  CameraGridView.swift
//  CameraTest
//
//  Created by Bogachev on 1/24/17.
//  Copyright © 2017 SBogachev. All rights reserved.
//

import UIKit

class CameraGridView: UIView {
  
  let numberOfRows = 2
  let numberOfColumns = 2
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.backgroundColor = UIColor.clear
  }
  
  override func draw(_ rect: CGRect) {

    guard let context = UIGraphicsGetCurrentContext() else { return }
    
    context.clear(bounds)
    
    context.setLineWidth(0.5)
    context.setStrokeColor(UIColor.white.cgColor)
    
    let columnWidth = frame.size.width / CGFloat(numberOfColumns + 1)
    
    for column in 1...numberOfColumns {
      
      let startPoint = CGPoint(x: columnWidth * CGFloat(column), y: 0)
      let endPoint = CGPoint(x: startPoint.x, y: frame.size.height)
      
      context.move(to: startPoint)
      context.addLine(to: endPoint)
      context.strokePath()
      
    }
    
    let rowHeight = frame.size.height / CGFloat(numberOfRows + 1)
    
    for row in 1...numberOfRows {
      
      let startPoint = CGPoint(x: 0, y: rowHeight * CGFloat(row))
      let endPoint = CGPoint(x: frame.size.width, y: startPoint.y)

      context.move(to: startPoint)
      context.addLine(to: endPoint)
      context.strokePath()
      
    }
    
  }
  
}
